module.exports = {


    friendlyName: 'Create project',
  
  
    description: 'Create project for the logged in user.',
  
 

    inputs: {
  
      projectId: {
        required: true,
        type: 'string',
        description: 'The project id.',
      },
  
  
    },
  
  
    exits: {
  
      success: {
        description: 'Project was deleted successfully.'
      },
  
    },
  
  
    fn: async function ({projectId}) {
  
      // Build up data for the new user record and save it to the database.
      // (Also use `fetch` to retrieve the new ID so that we can use it below.)

      await User.removeFromCollection(this.req.session.userId, 'projects')
      .members([projectId]);

    }
  
  };
  