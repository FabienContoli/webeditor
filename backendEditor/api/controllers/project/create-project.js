module.exports = {


    friendlyName: 'Create project',
  
  
    description: 'Create project for the logged in user.',
  
 

    inputs: {
  
      name: {
        required: true,
        type: 'string',
        description: 'The project name.',
        extendedDescription: 'Must be a unique name.',
      },

      description: {
        required: true,
        type: 'string',
        description: 'The project description.',
      },

      status: {
        defaultsTo:'active',
        type: 'string',
        isIn: ['active', 'deleted'],
        description: 'The project status.',
      },
  
  
    },
  
  
    exits: {
  
      success: {
        description: 'New project was created successfully.'
      },
  
      projectNameAlreadyInUse: {
        statusCode: 409,
        description: 'The provided project name is already used.',
      },
  
    },
  
  
    fn: async function ({name,description}) {
  
      var projectName = name.toLowerCase();
  
      // Build up data for the new user record and save it to the database.
      // (Also use `fetch` to retrieve the new ID so that we can use it below.)
      var newProjectRecord = await Project.create({
        name:projectName,
        description,
        owner: this.req.session.userId
      })
      .intercept('E_UNIQUE', 'projectNameAlreadyInUse')
      .fetch();
  
      
      if (sails.hooks.sockets) {
        await sails.helpers.broadcastSessionChange(this.req);
      }
      
      this.res.json(newProjectRecord);  
    }
  
  };
  