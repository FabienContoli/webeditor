module.exports = {


    friendlyName: 'get projects with pagination',
  
  
    description: 'retrieve project list with pagination.',
  
 
    inputs: {
  
      skip: {
        default:0,
        type: 'number',
        description: 'The project name.',
        extendedDescription: 'Must be a unique name.',
      }
  
    },
  
    exits: {
  
      success: {
        description: 'projects was retrieved successfully.'
      },
  
    },
  
  
    fn: async function () {
      // Build up data for the new user record and save it to the database.
      // (Also use `fetch` to retrieve the new ID so that we can use it below.)
      const projects = await Project.find({sort: 'name'}).paginate( this.req.param('skip'),10);
    
      this.res.json(projects);  
    }
  
  };
  