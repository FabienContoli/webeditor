module.exports = {


    friendlyName: 'retrieve a project model',
  
  
    description: 'retrieve a project detail with all files associated.',
  
 
    inputs: {
  
      id: {
        required:true,
        type: 'string',
        description: 'The project id.'
      }
  
    },
  
  
    exits: {
  
      success: {
        description: 'project detail was retrieved successfully.'
      },
      
      
        userUnauthorized: {
            description: `The user have not access to this project.`,
            responseType: 'unauthorized'
            // ^This uses the custom `unauthorized` response located in `api/responses/unauthorized.js`.
            // To customize the generic "unauthorized" response across this entire app, change that file
            // (see api/responses/unauthorized).
            //
            // To customize the response for _only this_ action, replace `responseType` with
            // something else.  For example, you might set `statusCode: 498` and change the
            // implementation below accordingly (see http://sailsjs.com/docs/concepts/controllers).
        },
    },
  
  
    fn: async function () {
      // Build up data for the new user record and save it to the database.
      // (Also use `fetch` to retrieve the new ID so that we can use it below.)
      
      const project = await Project.findOne({id:this.req.param('id')}).populate('files')

      if(project && project.owner === this.req.session.userId){
        this.res.json(project);
      }else{
          throw "userUnauthorized"
      }
        
    }
  
  };
  