module.exports = {

    friendlyName: 'Download file',
  
  
    description: 'Download a project file.',
  
  
    inputs: {
      
      projectId: {
        description: 'The id of the file.',
        type: 'string',
        required: true
      },
      id: {
        description: 'The id of the file.',
        type: 'string',
        required: true
      }
  
    },
  
  
    exits: {
  
      success: {
        outputDescription: 'The streaming bytes of the specified thing\'s photo.',
        outputType: 'ref'
      },
  
      forbidden: { responseType: 'forbidden' },
  
      notFound: { responseType: 'notFound' }
  
    },
  
    /**TODO: secure access with temporary token gived in session for users allowed **/

   fn: async function ({}) {
      const fileProject = await FileProject.findOne(this.req.param('id'))
      if (!fileProject) throw "notFound"
  
      // User has no avatar image uploaded.
      // (should have never have hit this endpoint and used the default image)
      if (!fileProject.fd && fileProject.project !==this.req.param('projectId') ) {
        throw "notFound"
      }
      this.res.type(fileProject.mimeType);
      this.res.attachment(fileProject.name);

      const downloading=await sails.startDownload(fileProject.fd)
      return downloading;
  
    }
  }