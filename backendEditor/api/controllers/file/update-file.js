module.exports = {


    friendlyName: 'update a file',
  
  
    description: 'update a project\'s file.',

    files: ['file'],

    inputs: {
        file:{
            description: 'Upstream for an incoming file upload.',
            type: 'ref'
        },
        projectId: {
          require:true,
          type: 'string',
          description: 'The project\'s id.'
        },
        id:{
          require:true,
          type: 'string',
          description: 'The file\'s id.'
        },
    },
    
  
  
    exits: {
  
        success: {
            description: 'a file was updated successfully.'
        },
        
        
        notFound: {
            responseType: 'notFound'
        },
      
        userUnauthorized: {
            description: `The user have not access to this project.`,
            responseType: 'unauthorized'
            // ^This uses the custom `unauthorized` response located in `api/responses/unauthorized.js`.
            // To customize the generic "unauthorized" response across this entire app, change that file
            // (see api/responses/unauthorized).
            //
            // To customize the response for _only this_ action, replace `responseType` with
            // something else.  For example, you might set `statusCode: 498` and change the
            // implementation below accordingly (see http://sailsjs.com/docs/concepts/controllers).
        },
    },
  
  
    fn: async function ({file}) {
        
        var path = require('path');
        var util = require('util');
        
        sails.log.warn("test",{ id: this.req.param('id'), project: this.req.param('projectId')})
        const project = await Project.findOne({ "owner" : this.req.session.userId,id: this.req.param('projectId')})

        if(! project ) throw "userUnauthorized";

        const fileFound=await FileProject.findOne({ id: this.req.param('id'), project: this.req.param('projectId')})
   
        if(!fileFound){
            throw "notFound";
        }

        // Upload the image.
        var info = await sails.uploadOne(file, {
            maxBytes: 8000000,
            dirname: path.resolve(sails.config.appPath, 'upload/projects/'+this.req.param('projectId')+'/')
        })
        // Note: E_EXCEEDS_UPLOAD_LIMIT is the error code for exceeding
        // `maxBytes` for both skipper-disk and skipper-s3.
        .intercept('E_EXCEEDS_UPLOAD_LIMIT', 'tooBig')
        .intercept((err)=>new Error('The file upload failed: '+util.inspect(err)));

        if(!info) {
            throw 'noFileAttached';
        }
        
        
        sails.log.warn("info",info)
      
        const fileInfoUpdate={
            fd:info.fd,
            size:info.size,
            mimeType:info.type,
        }
        
        const updatedFile=await FileProject.updateOne({ id: this.req.param('id'), project: this.req.param('projectId')})
        .set(fileInfoUpdate)

        return {
            file:updatedFile
        };
    }
        
  
  };
  