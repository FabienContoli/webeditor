module.exports = {


    friendlyName: 'create a file',
  
  
    description: 'Create a project\'s file.',
  
    files: ['file'],
 
    inputs: {
  
      file:{
        description: 'Upstream for an incoming file upload.',
        type: 'ref'
      },
      projectId: {
        require:true,
        type: 'string',
        description: 'The project id.'
      },
      path:{
        type: 'string',
        required:true,
        description: 'The folder path.'
      },
    },
  
  
    exits: {
  
      success: {
        description: 'the file was created successfully.'
      },
      
      
        userUnauthorized: {
            description: `The user have not access to this project.`,
            responseType: 'unauthorized'
            // ^This uses the custom `unauthorized` response located in `api/responses/unauthorized.js`.
            // To customize the generic "unauthorized" response across this entire app, change that file
            // (see api/responses/unauthorized).
            //
            // To customize the response for _only this_ action, replace `responseType` with
            // something else.  For example, you might set `statusCode: 498` and change the
            // implementation below accordingly (see http://sailsjs.com/docs/concepts/controllers).
        },
        fileAlreadyExisting:{
            statusCode: 409,
            description: 'The provided project name is already used.',
        }
    },
  
  
    fn: async function ({file,path:pathFile}) {
        
      var path = require('path');
      var util = require('util');
      const project = await Project.findOne({ "owner" : [this.req.session.userId],id: this.req.param('projectId')})

      if( !project ) throw "userUnauthorized"

      const fileFound=await FileProject.findOne({ path: pathFile, project: this.req.param('projectId')})
      
      if(fileFound){
          throw "fileAlreadyExisting";
      }

      var info = await sails.uploadOne(file, {
        maxBytes: 8000000,
        dirname: path.resolve(sails.config.appPath, 'upload/projects/'+this.req.param('projectId')+'/')
      })
      // Note: E_EXCEEDS_UPLOAD_LIMIT is the error code for exceeding
      // `maxBytes` for both skipper-disk and skipper-s3.
      .intercept('E_EXCEEDS_UPLOAD_LIMIT', 'tooBig')
      .intercept((err)=>new Error('The file upload failed: '+util.inspect(err)));

      if(!info) {
        throw 'noFileAttached';
      }
    
      sails.log.warn("info",info)
      
      sails.log.warn("info",pathFile)
      
      const fileModel ={
        name:info.name,
        fd:info.fd,
        size:info.size,
        mimeType:info.type,
        project: this.req.param('projectId'),
        path:pathFile
      }

      const newFileRecord = await FileProject.create(
          fileModel
        )
      .fetch();
    
      return {
        file:newFileRecord
      };
    }
  
  };
  