module.exports = {


    friendlyName: 'update a file',
  
  
    description: 'update a project\'s file.',

    inputs: {
        projectId: {
          require:true,
          type: 'string',
          description: 'The project\'s id.'
        },
        id:{
          require:true,
          type: 'string',
          description: 'The file\'s id.'
        },
        folder:{
            type: 'string',
            description: 'The folder path.'
        },
        name:{
            type: 'string',
            description: 'The file\'s name.'
        },
    },
    
  
  
    exits: {
  
        success: {
            description: 'a file info was updated successfully.'
        },
        
        
        notFound: {
            responseType: 'notFound'
        },
      
        badRequest:{
            responseType: 'badRequest'
        },
        
        userUnauthorized: {
            description: `The user have not access to this project.`,
            responseType: 'unauthorized'
            // ^This uses the custom `unauthorized` response located in `api/responses/unauthorized.js`.
            // To customize the generic "unauthorized" response across this entire app, change that file
            // (see api/responses/unauthorized).
            //
            // To customize the response for _only this_ action, replace `responseType` with
            // something else.  For example, you might set `statusCode: 498` and change the
            // implementation below accordingly (see http://sailsjs.com/docs/concepts/controllers).
        },
    },
  
  
  
    fn: async function ({name,folder}) {

        if(!folder && !name){
            throw "badRequest"
        }
        
        const project = await Project.findOne({ "owner" : this.req.session.userId,id: this.req.param('projectId')})

        if(! project ) throw "userUnauthorized";

        const fileFound=await FileProject.findOne({ id: this.req.param('id'), project: this.req.param('projectId')})
        
        if(!fileFound){
            throw "notFound";
        }

        const fileInfoUpdate={
        }
        
        if(name) fileInfoUpdate.name=name
        if(folder) fileInfoUpdate.folder=folder


        const updatedFile=await FileProject.updateOne({ id: this.req.param('id'), project: this.req.param('projectId')})
        .set(fileInfoUpdate)

  
        this.res.json(updatedFile);
    }
  
  };
  