module.exports = {


    friendlyName: 'delete a file',
  
  
    description: 'delete a project\'s file.',
  
 
    inputs: {
  
      projectId: {
        require:true,
        type: 'string',
        description: 'The project\'s id.'
      },

      id:{
        require:true,
        type: 'string',
        description: 'The file\'s id.'
      }


  
    },
  
  
    exits: {
  
      success: {
        description: 'a file was deleted successfully.'
      },
      
      
      notFound: {
        responseType: 'notFound'
      },

      userUnauthorized: {
          description: `The user have not access to this project.`,
          responseType: 'unauthorized'
          // ^This uses the custom `unauthorized` response located in `api/responses/unauthorized.js`.
          // To customize the generic "unauthorized" response across this entire app, change that file
          // (see api/responses/unauthorized).
          //
          // To customize the response for _only this_ action, replace `responseType` with
          // something else.  For example, you might set `statusCode: 498` and change the
          // implementation below accordingly (see http://sailsjs.com/docs/concepts/controllers).
        },
    },
  
  
    fn: async function () {
      // Build up data for the new user record and save it to the database.
      // (Also use `fetch` to retrieve the new ID so that we can use it below.)
      
      const project = await Project.findOne({ "owner" : this.req.session.userId,id: this.req.param('projectId')})
      
     
      if(! project ) throw "userUnauthorized";

      const fileFound=await FileProject.findOne({ id: this.req.param('id'), project: this.req.param('projectId')})
 
      if(!fileFound) throw "notFound";
        
      await sails.rm(fileFound.fd)
      .intercept((err)=>new Error('The file delete failed: '+util.inspect(err)));

      await Project.removeFromCollection(this.req.param('projectId'), 'files').members([this.req.param('id')]);
        
        
    }
  
  };
  