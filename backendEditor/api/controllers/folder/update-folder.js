module.exports = {


    friendlyName: 'update a folder',
  
  
    description: 'update a project\'s folder.',


    inputs: {
        name:{
            description: 'Upstream for an incoming file upload.',
            type: 'ref'
        },
        projectId: {
          require:true,
          type: 'string',
          description: 'The project\'s id.'
        },
        id:{
          require:true,
          type: 'string',
          description: 'The folder\'s id.'
        },
        folder:{
            type: 'string',
            description: 'The folder\'s parent id.'
        },
    },
    
  
  
    exits: {
  
        success: {
            description: 'a folder was updated successfully.'
        },
        
        badRequest:{
            responseType: 'badRequest'
        },

        notFound: {
            responseType: 'notFound'
        },
      
        userUnauthorized: {
            description: `The user have not access to this project.`,
            responseType: 'unauthorized'
            // ^This uses the custom `unauthorized` response located in `api/responses/unauthorized.js`.
            // To customize the generic "unauthorized" response across this entire app, change that file
            // (see api/responses/unauthorized).
            //
            // To customize the response for _only this_ action, replace `responseType` with
            // something else.  For example, you might set `statusCode: 498` and change the
            // implementation below accordingly (see http://sailsjs.com/docs/concepts/controllers).
        },
    },
  
  
    fn: async function ({name,folder}) {

        if(!folder && !name){
            throw "badRequest"
        }
        
        const project = await Project.findOne({ "owner" : this.req.session.userId,id: this.req.param('projectId')})

        if(! project ) throw "userUnauthorized";

        const folderFound=await FolderProject.findOne({ id: this.req.param('id'), project: this.req.param('projectId')})
        
        if(!folderFound){
            throw "notFound";
        }

        const folderInfoUpdate={
        }
        
        if(name) fileInfoUpdate.name=name
        if(folder) fileInfoUpdate.folder=folder


        const updatedFolder=await FolderProject.updateOne({ id: this.req.param('id'), project: this.req.param('projectId')})
        .set(folderInfoUpdate)

  
        this.res.json(updatedFolder);
    }
        
  
  };
  