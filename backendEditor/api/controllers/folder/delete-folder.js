module.exports = {


    friendlyName: 'delete a folder',
  
  
    description: 'delete a project\'s folder.',
  
 
    inputs: {
  
      projectId: {
        require:true,
        type: 'string',
        description: 'The project\'s id.'
      },

      id:{
        require:true,
        type: 'string',
        description: 'The folder\'s id.'
      }

    },
  
  
    exits: {
  
        success: {
          description: 'a file was deleted successfully.'
        },
      
        userUnauthorized: {
            description: `The user have not access to this project.`,
            responseType: 'unauthorized'
            // ^This uses the custom `unauthorized` response located in `api/responses/unauthorized.js`.
            // To customize the generic "unauthorized" response across this entire app, change that file
            // (see api/responses/unauthorized).
            //
            // To customize the response for _only this_ action, replace `responseType` with
            // something else.  For example, you might set `statusCode: 498` and change the
            // implementation below accordingly (see http://sailsjs.com/docs/concepts/controllers).
        },
    },
  
    fn: async function () {
      // Build up data for the new user record and save it to the database.
      // (Also use `fetch` to retrieve the new ID so that we can use it below.)
      
      const project = await Project.findOne({ "owner" : this.req.session.userId,id: this.req.param('projectId')})

      if( project ){
        FolderProject.destroy({project:this.req.param('projectId'), id:this.req.param('id')});
        
      }else{
        throw "userUnauthorized"
      } 
        
    }
  
  };
  