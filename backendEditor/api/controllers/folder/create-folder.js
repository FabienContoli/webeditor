module.exports = {


    friendlyName: 'update a folder',
  
  
    description: 'update a project\'s folder.',


    inputs: {
        name:{
            description: 'Upstream for an incoming file upload.',
            type: 'ref'
        },
        projectId: {
          require:true,
          type: 'string',
          description: 'The project\'s id.'
        },
        id:{
          require:true,
          type: 'string',
          description: 'The folder\'s id.'
        },
        folder:{
            type: 'string',
            description: 'The folder\'s parent id.'
        },
    },
    
  
  
    exits: {
  
        success: {
            description: 'a file was updated successfully.'
        },
        
        
        notFound: {
            responseType: 'notFound'
        },
      
        userUnauthorized: {
            description: `The user have not access to this project.`,
            responseType: 'unauthorized'
            // ^This uses the custom `unauthorized` response located in `api/responses/unauthorized.js`.
            // To customize the generic "unauthorized" response across this entire app, change that file
            // (see api/responses/unauthorized).
            //
            // To customize the response for _only this_ action, replace `responseType` with
            // something else.  For example, you might set `statusCode: 498` and change the
            // implementation below accordingly (see http://sailsjs.com/docs/concepts/controllers).
        },
    },
  
  
    fn: async function ({name,folder}) {
        
        var path = require('path');
        var util = require('util');
        
        const project = await Project.findOne({ "owner" : this.req.session.userId,id: this.req.param('projectId')})

        if(! project ) throw "userUnauthorized";

        const folderModel={
            name,
            project:this.req.param('projectId'),
        }
        
        if(folder) folderModel.folder=folder

        const createdFolder=await FolderProject.create(folderModel)
        .fetch()

        this.res.json(createdFolder);
    }
        
  
  };
  