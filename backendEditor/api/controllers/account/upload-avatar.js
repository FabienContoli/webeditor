
/**
 * https://sailsjs.com/documentation/concepts/file-uploads
 * Upload avatar for currently logged-in user
 *
 * (POST /user/avatar)
 */



 module.exports = {


  friendlyName: 'Upload avatar',


  description: 'Upload an avatar user.',


  files: ['avatar'],


  inputs: {

    avatar: {
      description: 'Upstream for an incoming file upload.',
      type: 'ref'
    },

  },


  exits: {

    success: {
      outputDescription: 'The newly created uploaded avatar.',
      outputExample: {}
    },

    noFileAttached: {
      description: 'No file was attached.',
      responseType: 'badRequest'
    },

    tooBig: {
      description: 'The file is too big.',
      responseType: 'badRequest'
    },

  },



  fn: async function ({avatar}) {
    
    //const url = require('url');
    const util = require('util');
    
    const info = await sails.uploadOne(avatar, {
      maxBytes: 3000000,
      dirname: require('path').resolve(sails.config.appPath, 'upload/user/avatar')
    })
    .intercept('E_EXCEEDS_UPLOAD_LIMIT', 'tooBig')
    .intercept((err)=>new Error('The photo upload failed: '+util.inspect(err)));

    if(!info) {
      throw 'noFileAttached';
    }

    const avatarUrl= util.format('%s/api/v1/account/avatar/%s', sails.config.custom.baseUrl, this.req.session.userId)
    await User.update(this.req.session.userId, {
      // Generate a unique URL where the avatar can be downloaded.
      avatarUrl,

      // Grab the first file and use it's `fd` (file descriptor)
      avatarFd: info.fd
    })

    return {
      avatarUrl,
      avatarFd: info.fd
    };
/*
    avatar.upload({
      // don't allow the total upload size to exceed ~10MB
      maxBytes: 10000000,
      dirname: require('path').resolve(sails.config.appPath, 'assets/user/avatar')
    },function whenDone(err, uploadedFiles) {
      if (err) {
        return res.serverError(err);
      }
  
      // If no files were uploaded, respond with an error.
      if (uploadedFiles.length === 0){
        return res.badRequest('No file was uploaded');
      }
  
      // Get the base URL for our deployed application from our custom config
      // (e.g. this might be "http://foobar.example.com:1339" or "https://example.com")
      var baseUrl = sails.config.custom.baseUrl;
  
      // Save the "fd" and the url where the avatar for a user can be accessed
      
      User.update(req.session.userId, {
        // Generate a unique URL where the avatar can be downloaded.
        avatarUrl: require('util').format('%s/api/v1/account/avatar/%s', baseUrl, req.session.userId),
  
        // Grab the first file and use it's `fd` (file descriptor)
        avatarFd: uploadedFiles[0].fd
      })
      .exec(function (err){
        
        if (err) return res.serverError(err);
        return res.ok();
      });
    });*/
  }
}
  