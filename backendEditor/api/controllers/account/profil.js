module.exports = {


    friendlyName: 'Profil',
  
  
    description: 'Retrieve user info for the logged-in user.',

    exits: {
  
      success: {
        description: 'The user model has been retrieved for the logged-in user.',
      },
      
    },
  
  
    fn: async function ({}) {
        
     var userWithProjects = await User.findOne({ id: this.req.session.userId }).populate('projects');
     this.res.json(userWithProjects);  
      
    }
  
  };
  