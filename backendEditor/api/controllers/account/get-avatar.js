module.exports = {

  friendlyName: 'Download avatar',


  description: 'Download a user avatar.',


  inputs: {

    id: {
      description: 'The id of the user whose photo we\'re downloading.',
      type: 'string',
      required: true
    }

  },


  exits: {

    success: {
      outputDescription: 'The streaming bytes of the specified thing\'s photo.',
      outputType: 'ref'
    },

    forbidden: { responseType: 'forbidden' },

    notFound: { responseType: 'notFound' }

  },

 fn: async function ({}) {
    const user = await User.findOne(this.req.param('id'))
    if (!user) throw "notFound"

    // User has no avatar image uploaded.
    // (should have never have hit this endpoint and used the default image)
    if (!user.avatarFd) {
      throw "notFound"
    }

    const downloading=await sails.startDownload(user.avatarFd)
    return downloading;

  }
}