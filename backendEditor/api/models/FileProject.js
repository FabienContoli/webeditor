// A project File may only belong to a single project
module.exports = {
    attributes: {
      name: {
        type: 'string',
        required: true,
        description: 'file name.',
        maxLength: 120,
        example: 'test project'
      },

      fd:{
        type: 'string',
        required: true,
        unique: true,
        description: 'file description.',
        maxLength: 120
      },

      mimeType:{
        type: 'string',
        required: true,
        description: 'file mime type.',
        maxLength: 120
      },
      
      size:{
        type: 'number',
        description: 'file size in octet.'
      },

      path:{
        required: true,
        type: 'string',
        description: 'folder path.',
        maxLength: 120
      },

      
      // Add a reference to Project
      project: {
        model: 'project'
      }
    }
  };