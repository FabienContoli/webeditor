// A project File may only belong to a single project
module.exports = {
    attributes: {
      name: {
        type: 'string',
        required: true,
        description: 'folder name.',
        maxLength: 120,
        example: 'test project'
      },

      folder:{
        type: 'string',
        description: 'id parent folder.',
        maxLength: 120
      },

      root:{
        type: 'string',
        description: 'root folder name.',
        maxLength: 120
      },
      
      // Add a reference to Project
      project: {
        model: 'project'
      }
    }
  };