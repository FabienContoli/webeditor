// A project may only belong to a single user
module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true,
      unique: true,
      description: 'Project\'s name.',
      maxLength: 120,
      example: 'test project'
    },

    avatarUrl:{
      type: 'string',
      description: 'the project avatar url',
      maxLength: 120,
      example: '/project/avatar/135344313513'
    },

    private:{
      type: 'boolean',
      description: 'A boolean to define if the project is private',
    },
    
    description: {
      type: 'string',
      required: true,
      description: 'Project\'s description.',
      maxLength: 120,
      example: 'test project'
    },

    files: {
      collection: 'fileProject', 
      description: 'file\'s projects.',
      via: 'project'
    },

    // folders: {
    //   collection: 'folderProject', 
    //   description: 'folder\'s projects.',
    //   via: 'project'
    // },

    // Add a reference to User
    owner: {
      model: 'user'
    }
  }
};