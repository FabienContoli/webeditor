import Camera from "./../blueprints/camera/Camera";
import Level from "./../blueprints/Level";
import Light from "./../blueprints/Light";
import Mesh from "./../blueprints/mesh/Mesh";
import Scene from "./../blueprints/Scene";
import Node from "./../blueprints/Node";

export function createDefaultLevel(name){
    return new Level({
        name:name,
        scene:0,
        scenes:[new Scene({name:"default scene",nodes:[0]})],
        camera:0,
        cameras:[new Camera()],
        lights:[new Light()],
        meshes:[new Mesh()],
        nodes:[new Node({name:"root Node"})] 
    });
}