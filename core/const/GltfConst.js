// https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#accessors
//https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Constants
// Variable :



/** 
*   @readonly
*   @enum {TEXTURE_ENUM_TYPE}
*   @property {TEXTURE_ENUM_TYPE}
*/

const TEXTURE_ENUM = Object.freeze({
  NEAREST :	0x2600,	
  LINEAR :	0x2601,	
  NEAREST_MIPMAP_NEAREST :	0x2700,	
  LINEAR_MIPMAP_NEAREST :	0x2701,	
  NEAREST_MIPMAP_LINEAR :	0x2702,	
  LINEAR_MIPMAP_LINEAR :	0x2703,	
  TEXTURE_MAG_FILTER :	0x2800,	
  TEXTURE_MIN_FILTER :	0x2801,	
  TEXTURE_WRAP_S :	0x2802,	
  TEXTURE_WRAP_T :	0x2803,	
  TEXTURE_2D :	0x0DE1,	
  TEXTURE :	0x1702,	
  TEXTURE_CUBE_MAP :	0x8513,	
  TEXTURE_BINDING_CUBE_MAP :	0x8514,	
  TEXTURE_CUBE_MAP_POSITIVE_X :	0x8515,	
  TEXTURE_CUBE_MAP_NEGATIVE_X :	0x8516,	
  TEXTURE_CUBE_MAP_POSITIVE_Y :	0x8517,	
  TEXTURE_CUBE_MAP_NEGATIVE_Y :	0x8518,	
  TEXTURE_CUBE_MAP_POSITIVE_Z :	0x8519,	
  TEXTURE_CUBE_MAP_NEGATIVE_Z :	0x851A,	
  MAX_CUBE_MAP_TEXTURE_SIZE :	0x851C,	
  ACTIVE_TEXTURE :	0x84E0,
  REPEAT :	0x2901,	
  CLAMP_TO_EDGE :	0x812F,	
  MIRRORED_REPEAT :	0x8370
})

const COMPONENT_TYPES =  Object.freeze({
    5120: Int8Array,
    5121: Uint8Array,
    5122: Int16Array,
    5123: Uint16Array,
    5125: Uint32Array,
    5126: Float32Array,
  });
  
  // Size in BYTES
  const COMPONENT_TYPES_SIZE = Object.freeze({
    5120: 1,
    5121: 1,
    5122: 2,
    5123: 2,
    5125: 4,
    5126: 4,
  });
  
  const WEBGL_TYPE_SIZES = Object.freeze({
    SCALAR: 1,
    VEC2: 2,
    VEC3: 3,
    VEC4: 4,
    MAT2: 4,
    MAT3: 9,
    MAT4: 16,
  });
  
  const ATTRIBUTES = Object.freeze({
    POSITION: 'position',
    NORMAL: 'normal',
    TANGENT: 'tangent',
    TEXCOORD_0: 'texCoord0',
    TEXCOORD_1: 'texCoord1',
    COLOR_0: 'color',
    WEIGHTS_0: 'weight',
    JOINTS_0: 'joint',
  });
  
  const WEBGL_CONSTANTS = Object.freeze({
    FLOAT: 5126,
    // FLOAT_MAT2: 35674,
    FLOAT_MAT3: 35675,
    FLOAT_MAT4: 35676,
    FLOAT_VEC2: 35664,
    FLOAT_VEC3: 35665,
    FLOAT_VEC4: 35666,
    LINEAR: 9729,
    REPEAT: 10497,
    SAMPLER_2D: 35678,
    POINTS: 0,
    LINES: 1,
    LINE_LOOP: 2,
    LINE_STRIP: 3,
    TRIANGLES: 4,
    TRIANGLE_STRIP: 5,
    TRIANGLE_FAN: 6,
    UNSIGNED_BYTE: 5121,
    UNSIGNED_SHORT: 5123,
  });
  
  export {
    TEXTURE_ENUM ,COMPONENT_TYPES, COMPONENT_TYPES_SIZE, WEBGL_TYPE_SIZES, ATTRIBUTES, WEBGL_CONSTANTS,
  };
  