import Logger from "./../logger/Logger";

/**
 *  Game Loop Class
 *  @class
 */
export default class Loop {

  /**
   * Loop update function callback
   * @type {function}
   * @private
   */
  update;

  /**
  * Loop render function callback
  * @type {function}
  * @private
  */
  render;

  /**
  * Logger lib to use
  * @type {Logger}
  * @private
  */
  logger;

  /**
  * boolean to disable the loop
  * @type {boolean}
  * @private
  */
  paused;

  /**
  * boolean to disable the loop
  * @type {number}
  * @private
  */
  idRequestAnimationFrame;
  
  /**
  * boolean to disable the loop
  * @type {number}
  * @private
  */
   currentUpdate;

   
  /**
  * framerate of the loop
  * @type {number}
  * @private
  */
   framerate;

  /**
  * speed multiplicator for the loop
  * @type {number}
  * @private
  */
  speed;
   
  /**
  * @constructor
  * @param {Object} [param] 
  * @param {function} [param.update]
  * @param {function} [param.render] 
  * @param {Logger} [param.logger] 
  * @param {number} [param.framerate] 
  * @param {number} [param.speed] 
  */
    
  constructor({update,render, logger, framerate = 60, speed = 1}={}){
    this.update=update;
    this.render=render;
    this.logger = logger;
    this.elapsedTime = 0;
    this.lastUpdate=null;
    this.paused = false;
    this.idRequestAnimationFrame=null;
    this.currentUpdate=0
    this.framerate = framerate;
    this.speed = speed;
    this.time = 1 / this.framerate / this.speed;
    this.timeMs = this.time*1000;
  }
  
  mainLoop(currentUpdate) {
    if (this.lastUpdate
        && this.paused === false) {
      this.currentUpdate = currentUpdate;
      // define elapsed time since last update
      this.elapsedTime += this.currentUpdate - this.lastUpdate;

      // call user's update handler matching timeframe, speed and fixing browser time handling
      while (this.elapsedTime >= this.timeMs
            && this.paused === false) {
        // define elapsed time since last user's update handler matching timeframe and speed
        if (this.elapsedTime >= this.timeMs*5) {
          this.logger.log('droped'+ this.elapsedTime);

          // define elapsed time since last user's update handler matching timeframe and speed
          this.elapsedTime -= this.elapsedTime;

          this.update(this.elapsedTime);

          continue;
        }
        this.elapsedTime -= this.timeMs;
        this.update(this.time)
        this.render(this.time);
      }
    
    }  
    
    // call user's update handler on each available frame
    if(!this.paused)
    this.idRequestAnimationFrame=window.requestAnimationFrame(this.mainLoop.bind(this));
    this.lastUpdate = currentUpdate;
    
  }

  pause() {
    this.paused = true;
  }

  play() {
    this.paused = false;
    this.mainLoop()
  }
  
  /**
  * set update function for the mainLoop
  * @function
  * @param {function} updateHandler
  * @returns {void} 
  * @public
  */
  setUpdate(updateHandler){
    this.update=updateHandler
  }

  /**
  * set render function for the mainLoop
  * @function
  * @param {function} renderHandler
  * @returns {void} 
  * @public
  */
  setRender(renderHandler){
    this.render=renderHandler
  }

  tick(times = 1) {
    // call user's update handler on each available frame
    this.idRequestAnimationFrame=window.requestAnimationFrame(() => {
      while (times--) {
        let time =1000 / this.framerate;
        this.update(time)
        this.render(time);
      }
    });
  }

  destroy() {
    console.log("Loop cancelAnimationFrame")
    window.cancelAnimationFrame(this.idRequestAnimationFrame)
    this.paused=true
  }  
}