import Input from "./../input/Input";
import Logger from "./../logger/Logger";
import GamepadInput from "./../input/GamePadInput";

export default class GamepadsController{
    
     /**
     * Logger lib to use
     * @type {Logger}
     * @private
     */
    logger;

    /**
     * add input to controller
     * @type {function} 
     * @private
     */
     onInput

    /**
     * pull of gamepads
     * @type {Array<Gamepad>}
     * @private
     */
     gamepads
     
    /**
     * @type {array} 
     * @private
     */
     gamepadTimestamps

    /**
     * @constructor
     * @param {function} onInput
     * @param {Logger} logger
    */
    constructor(onInput,logger){
        this.inputs=[]
        this.onInput=onInput;
        this.logger=logger;
        this.gamepads=[];
        this.gamepadTimestamps=[]
        this.logger.debug("new Gamepad controler");
        this.init()
    }

    fetchInputs(){
        this.pollGamepad()
        for (let i = 0; i < this.gamepads.length; i++) {
            let gamepad =this.gamepads[i];
            if(this.gamepadTimestamps[i] !== undefined && gamepad.timestamp !==this.gamepadTimestamps[i]){
                this.gamepadTimestamps[i] = gamepad.timestamp;
                this.pollAxis(gamepad,i)
                this.pollButtonsPressed(gamepad,i)
            }
        }
       
    }

    pollGamepad(){
        var gamepads = navigator.getGamepads()
        for (var i = 0; i < gamepads.length; i++) {
            let gamepad =gamepads[i]     
            if(gamepad){
                this.gamepads[i]=gamepad
            }
        }
    }


    /**
     * @function
     * @param {Gamepad} gamepad 
     * @param {number} index
     */
    pollAxis(gamepad,index){
        let axes =gamepad.axes
        for (let axesIndex = 0; axesIndex < axes.length; axesIndex++) {
            let input =new GamepadInput(index,"axes",axesIndex,"move", axes[axesIndex]);
            this.sendInput(input)
        }
    }

    /**
     * @function
     * @param {Gamepad} gamepad 
     * @param {number} index 
     */
    pollButtonsPressed(gamepad,index){
        let buttons=gamepad.buttons 
        for (let buttonIndex = 0; buttonIndex < buttons.length; buttonIndex++) {
            const button = buttons[buttonIndex];
            if(button.pressed){
                let input =new GamepadInput(index,"buttons",buttonIndex,"pressed", button.value);
                this.sendInput(input)
            }
        }
    }


    init(){
        window.addEventListener('gamepadconnected', this.onGamepadConnect.bind(this), false);
        window.addEventListener('gamepaddisconnected', this.onGamepadDisconnect.bind(this), false);
    }

    destroy() {
        window.removeEventListener('gamepadconnected', this.onGamepadConnect.bind(this), false);
        window.removeEventListener('gamepaddisconnected', this.onGamepadDisconnect.bind(this), false);
    }
    
    /**
     * @function
     * @param {Input} input 
     */
    sendInput(input){
        this.onInput(input)
    }

    /**
     * Adds a gamepad when connected
     * @param {GamepadEvent} event A 'gamepadconnected' event object
     */
     onGamepadConnect(event) {
        var gamepad = event.gamepad;
        this.gamepads[event.gamepad.id] = gamepad;
    }

    /**
     * Sets a disconnected gamepad to 'null'
     * @param {GamepadEvent} event 'gamepaddisconnected' event object
     */
    onGamepadDisconnect(event) {
        this.gamepads[event.gamepad.id] = null;
    }

}