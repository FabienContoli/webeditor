import Logger from "./../logger/Logger";
import Input from "./../input/Input";
import MouseInput from "./../input/MouseInput";

export default class MouseController{
    
     /**
     * Logger lib to use
     * @type {Logger}
     * @private
     */
    logger;

    /**
     * add
     * @param {function} onInput
     * @private
     */
     onInput

    /**
     * @constructor
     * @param {function} onInput
     * @param {Logger} logger
    */
    constructor(container=document.body,onInput,logger){
        this.container=container
        this.onInput=onInput;
        this.logger=logger;
        this.listenEvent()
        this.logger.debug("new  mouse controler");
    }

    /**
     * @function
     * @private
     */
    listenEvent(){
        this.container.addEventListener("contextmenu", this.handleContextMenu.bind(this));
        this.container.addEventListener("mousedown", this.handleMouseDown.bind(this));
        this.container.addEventListener("mouseenter", this.handleMouseEnter.bind(this));
        this.container.addEventListener("mouseleave", this.handleMouseLeave.bind(this));
        this.container.addEventListener("mousemove", this.handleMouseMove.bind(this));
        this.container.addEventListener("mouseup", this.handleMouseUp.bind(this));
        this.container.addEventListener("wheel", this.handleMouseWheel.bind(this));
    }

    /**
     * @function
     */
     destroy(){    
        this.container.removeEventListener('contextmenu', this.handleContextMenu.bind(this));
        this.container.removeEventListener('mousedown', this.handleMouseDown.bind(this));
        this.container.removeEventListener('mouseenter', this.handleMouseEnter.bind(this));
        this.container.removeEventListener('mouseleave', this.handleMouseLeave.bind(this));
        this.container.removeEventListener('mousemove', this.handleMouseMove.bind(this));
        this.container.removeEventListener('mouseup', this.handleMouseUp.bind(this));
        this.container.removeEventListener('wheel', this.handleMouseWheel.bind(this));
    }

    /**
     * @function
     * @param {MouseEvent} event 
     */
     handleContextMenu(event) {
         
        const rectangle = event.target.getBoundingClientRect();
        let value ={
            x:event.clientX - rectangle.left,
            y:event.clientY - rectangle.top
        }
        let input=new MouseInput("contextMenu", null, value, event)
        this.sendInput(input)
    }

    /**
     * @function
     * @param {MouseEvent} event 
     */
     handleMouseDown(event) {
         
        const rectangle = event.target.getBoundingClientRect();
        let value ={
            x:event.clientX - rectangle.left,
            y:event.clientY - rectangle.top
        }
        let input=new MouseInput("down", event.button ,value ,event)
        this.sendInput(input)
    }

    /**
     * @function
     * @param {MouseEvent} event 
     */
    handleMouseEnter(event) {
        
        const rectangle = event.target.getBoundingClientRect();
        let value ={
            x:event.clientX - rectangle.left,
            y:event.clientY - rectangle.top
        }
        let input=new MouseInput("enter", null ,value ,event)
        this.sendInput(input)
    }

    /**
     * @function
     * @param {MouseEvent} event 
     */
     handleMouseLeave(event) {
        let input=new MouseInput("leave", null ,null ,event)
        this.sendInput(input)
    }

    /**
     * @function
     * @param {MouseEvent} event 
     */
     handleMouseMove(event) {
        const rectangle = event.target.getBoundingClientRect();
        let value ={
            x:event.clientX - rectangle.left,
            y:event.clientY - rectangle.top
        }
        let input=new MouseInput("move", event.button ,value ,event)
        this.sendInput(input)
    }
   /**
     * @function
     * @param {MouseEvent} event 
     */
    handleMouseUp(event) {
        const rectangle = event.target.getBoundingClientRect();
        const x = event.clientX - rectangle.left;
        const y = event.clientY - rectangle.top;
  

        let input=new MouseInput("up", event.button ,null ,event)
        this.sendInput(input)
    }

   /**
     * @function
     * @param {WheelEvent} event 
     */
    handleMouseWheel(event) {
    
        let value ={
            x:event.deltaX,
            y:event.deltaY
        }
        let input=new MouseInput("wheel", event.button ,value ,event)
        this.sendInput(input)
    }
    /**
     * @function
     * @param {Input} input 
     */
     sendInput(input){
        //  console.log("mouseInput",input);
        this.onInput(input)
    }

    
}