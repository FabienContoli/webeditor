import Logger from "core/logger/Logger";
import Input from "core/input/Input";
import TouchInput from "core/input/TouchInput";

export default class TouchController{
    
     /**
     * Logger lib to use
     * @type {Logger}
     * @private
     */
    logger;

    /**
     * add
     * @param {function} onInput
     * @private
     */
     onInput

    /**
     * @constructor
     * @param {function} onInput
     * @param {Logger} logger
    */
    constructor(container=document.body,onInput,logger){
        this.container=container
        this.onInput=onInput;
        this.logger=logger;
        this.listenEvent()
        this.logger.debug("new  mouse controler");
    }

    /**
     * @function
     * @private
     */
    listenEvent(){
        this.container.addEventListener("touchstart", this.handleStart);
        this.container.addEventListener("touchmove", this.handleMove);
        this.container.addEventListener("touchend", this.handleEnd);
        this.container.addEventListener("touchcancel", this.handleCancel);
    }

    /**
     * @function
     */
    destroy(){
        this.container.removeEventListener('touchstart', this.handleStart)
        this.container.removeEventListener('touchmove', this.handleMove)
        this.container.removeEventListener('touchend', this.handleEnd)
        this.container.removeEventListener('touchcancel', this.handleCancel)
    }
    

    /**
     * @function
     * @param {TouchEvent} event 
     */
     handleStart(event) {
         
        const rectangle = this.container.getBoundingClientRect();
        let value ={
            x:event.touches[0].pageX - rectangle.left,
            y:event.touches[0].pageY - rectangle.top
        }
        let input=new TouchInput("start", value, event)
        
        this.sendInput(input)
    }

    /**
     * @function
     * @param {TouchEvent} event 
     */
     handleMove(event) {
        
        const rectangle = this.container.getBoundingClientRect();
        
        let value ={
            x:event.touches[0].pageX - rectangle.left,
            y:event.touches[0].pageY - rectangle.top
        }
        let input=new TouchInput("move", value, event )
        this.sendInput(input)
    }

    /**
     * @function
     * @param {TouchEvent} event 
     */
     handleEnd(event) {
        let input=new TouchInput("end", null,event)
        this.sendInput(input)
    }

    /**
     * @function
     * @param {TouchEvent} event 
     */
     handleCancel(event) {
        let input=new TouchInput("cancel",null,event)
        this.sendInput(input)
    }

    /**
     * @function
     * @param {Input} input 
     */
     sendInput(input){
        this.onInput(input)
    }

    
}