import Logger from "./../logger/Logger";
import Controls from "./../models/Controls";
import Input from "./../input/Input";
import GamepadsController from "./GamepadsController"
import KeyboardController from "./KeyboardController";
import MouseController from "./MouseController";

export default class Controllers{
    
     /**
     * Logger lib to use
     * @type {Logger}
     * @private
     */
    logger;
 
    /**
     * bind container
     * @type {HTMLElement}
     * @private
     */
    container
        
    /**
     * Controls to use
     * @type {Controls}
     * @private
     */
    controls;

    /**
     * inputs activate by the user
     * @type {Array<Input>}
     * @private
    */
     inputs;

    /**
     * @constructor
     * @param {Controls} controls
     * @param {HTMLElement} container
     * @param {Logger} logger
    */
    constructor(controls,container,logger){
        this.inputs=[]
        this.logger=logger;
        controls=controls??{gamepad:true,keyboard:true,mouse:true};
        
        this.container=container;
        if(this.container.tabIndex===-1){
            this.container.tabIndex=0
        }
        this.gamepads=controls.gamepad?new GamepadsController(this.onInput.bind(this),this.logger):null;
        this.keyboard=controls.keyboard?new KeyboardController(this.container,this.onInput.bind(this),this.logger):null;
        this.mouse=controls.mouse?new MouseController(this.container,this.onInput.bind(this),this.logger):null;
        this.logger.debug("new  controls "+controls);
    }
    
    /**
     * @function
     * @param {Input} input
     */
    onInput(input){
        this.inputs.push(input)
    }

    getInputs(){
        if(this.gamepads){
            this.gamepads.fetchInputs();
        }
        return this.inputs;
    }

    clearInputs(){
        this.inputs=[]
    }

    destroy(){
        if(this.gamepads) this.gamepads.destroy()
        if(this.keyboard) this.keyboard.destroy()
        if(this.mouse) this.mouse.destroy()
    }
}