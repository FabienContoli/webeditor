import Logger from "./../logger/Logger";
import Input from "./../input/Input";
import KeyboardInput from "./../input/KeyboardInput";

export default class KeyboardController{
    
     /**
     * Logger lib to use
     * @type {Logger}
     * @private
     */
    logger;

    /**
     * function to receive input
     * @type {function}
     * @private
     */
     onInput
     
    /**
     * bind container
     * @type {HTMLElement}
     * @private
     */
    container

    /**
     * @constructor
     * @param {HTMLElement} container
     * @param {function} onInput
     * @param {Logger} logger
    */
    constructor(container=document.body,onInput,logger){
        this.inputs=[]
        this.container=container
        this.onInput=onInput;
        this.logger=logger;
        this.listenEvent()
        this.logger.debug("new keyboard controler");
    }

    /**
     * @function
     * @private
     */
    listenEvent(){
        this.container.addEventListener('keydown', this.keyDownHandler.bind(this), false);
        this.container.addEventListener('keyup', this.keyUpHandler.bind(this), false);
    }

    /**
     * @function
     */
    destroy(){
        this.container.removeEventListener('keydown', this.keyDownHandler.bind(this),false)
        this.container.removeEventListener('keyup', this.keyUpHandler.bind(this),false)
    }

    /**
     * @function
     * @param {KeyboardEvent} event 
     */
    keyDownHandler(event) {
        let input=new KeyboardInput("down", event.key, event)
        this.sendInput(input)
    }

    /**
     * @function
     * @param {KeyboardEvent} event 
     */
    keyUpHandler(event) {
        let input=new KeyboardInput("up", event.key,event)
        this.sendInput(input)
    }

    /**
     * @function
     * @param {Input} input 
     */
    sendInput(input){
        // console.log("keyboardInput",input);
        this.onInput(input)
    }
    
}