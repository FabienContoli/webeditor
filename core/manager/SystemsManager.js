import System from "../models/System"
import mapInputToAction from "../common/systems/mapInputToAction.js"
import transformStatesWithActions from "../common/systems/transformStatesWithActions.js"
import triggerActions from "../common/systems/triggerActions.js"
import animationSystem from "../common/systems/animationSystem"

export default class SystemsManager{
    
    /** 
     * @type Map<String,System>
     */
    systems;

    constructor(){
        this.systems=new Map();
    }

    set(system){
        this.systems.set(system.uuid,system)
    }

    update(serviceLocator,level,gameState){

        //mapInputToAction(level,gameState)
        //transformStatesWithActions(level,gameState)
        triggerActions(serviceLocator,level,gameState)
        animationSystem(serviceLocator,level,gameState)
        // level.events:{eventEchap:[callbackChangeState]}
        
        // action echap -> [listeners] -> pause machine -> script post changement d etat 
        // action entrée -> [] ->
        // action echap -> 

        this.systems.forEach((system)=>{
            system.script(serviceLocator,level,gameState)    
        })
    }

}