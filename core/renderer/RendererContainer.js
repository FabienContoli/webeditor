import Layer from "./Layer";

export default class RendererContainer{

    /**
     * @type {HTMLElement}
     */
    element;

    /**
     * @type {Array<Layer>}
     */
    layers;
    
    /**
     * @constructor
     * @param {HTMLElement} element
     */
    constructor(element){
        this.element=element
        this.layers=[]
    }

    /**
     * @function
     * @return {WebGLRenderingContext}
     */
    addWebglLayer(){
        const layer= new Layer(this.element,"webgl");
        this.layers.push(layer);
        return layer.context
    }

    /**
     * @function
     * @return {CanvasRenderingContext2D}
     */
    addCanvas2DLayer(){
        const layer= new Layer(this.element,"2d");
        this.layers.push(layer);
        return layer.context
    }
    
   /**
     * @function
     * @param {number} index
     */
    removeLayerByIndex(index){
        let layer =this.layers.splice(index, 1)[0]
        layer.detach()
    }

    destroy(){
        this.layers.forEach((layer)=>{
            if(layer.context instanceof WebGLRenderingContext ){
                layer.context.getExtension('WEBGL_lose_context').loseContext();
            }
            layer.detach()
        })
    }


}