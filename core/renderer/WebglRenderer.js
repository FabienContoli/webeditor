//import Scene from "./../models/Scene";
import Level from "../models/Level.js";
import RendererContainer from "./RendererContainer";
import WebglApiHelper from './webgl/WebglApiUtils';
import * as m4 from "./../maths/m4" 

export default class WebglRenderer{
    
    /**
     * @type {RendererContainer}
     */
    container;


    /**
     * @type {WebGLRenderingContext}
     */
    gl;
    

    /**
     * @constructor
     * @param {RendererContainer} container
     * @param {WebGLRenderingContext} gl
     */
    constructor(container,gl){
        this.container=container,
        this.gl=gl
        this.renderStack={opaque:[],transmissive:[],transparent:[]}
        this.webglApiHelper = new WebglApiHelper(this.gl)
    }


    /**
     * @funtion
     * @param {Level} level
     */
    render(level){
        let camera = level.camera
        let scene= level.scene
        let nodes =scene.nodes

        this.resizeCanvasToDisplaySize(this.gl.canvas);
    
        this.fillRenderStack(nodes);
        
       // this.renderForPicking(scene, camera/*, frameBuffer, targetTexture*/);
        
        this.renderCubeMap(scene, camera);
        this.renderOpaqueStack(this.renderStack.opaque, camera)

        //this.renderTransmissiveObjects(this.renderStack.transmissive, camera);
        //this.renderMeshTransparentNodes(this.renderStack.transparent, camera);
        
        this.cleanRenderStack()
        
    }

    resizeCanvasToDisplaySize(canvas) {
        const width = canvas.clientWidth;
        const height = canvas.clientHeight;
        if (canvas.width !== width || canvas.height !== height) {
          canvas.setAttribute('width',""+Math.floor(width));
          canvas.setAttribute('height',""+Math.floor(height));
          return true;
        }
        return false;
    }
      
    resize(){

    }

    clear(){
        // Définir la couleur d'effacement comme étant le noir, complètement opaque
        this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
        // Effacer le tampon de couleur avec la couleur d'effacement spécifiée
        this.gl.clear(this.gl.COLOR_BUFFER_BIT|this.gl.DEPTH_BUFFER_BIT);
    }

    fillRenderStack(nodes){
        nodes.forEach(node => {
            if (node.mesh) {
                node.mesh.primitives.forEach((primitive)=>{
                    const { material } = primitive;
                        
                    if (material.alphaMode && material.alphaMode === 'BLEND') {
                        this.renderStack.transparent.push({node,primitive});
                    } else {
                        this.renderStack.opaque.push({node,primitive});
                    }
                })
            }
            if (node.children) {
                this.fillRenderStack(node.children);
            }
        });
    }

    cleanRenderStack(){
        this.renderStack={opaque:[],transmissive:[],transparent:[]}
    }

    renderForPicking(){

    }

    renderCubeMap = function renderCubeMap( scene, camera) {
        const webglHelper = this.webglApiHelper;    
        const gl= this.gl;
      
        gl.viewport(0, 0, gl.canvas.clientWidth, gl.canvas.clientHeight);
      
        webglHelper.disableProperty(gl.BLEND);
        webglHelper.enableProperty(gl.CULL_FACE);
        webglHelper.enableProperty(gl.DEPTH_TEST);
        gl.depthFunc(gl.LEQUAL);
        const programInfo = webglHelper.updateProgramCubeMap();
      
        const quadBufferInfo = webglHelper.setCubeMapBuffer(programInfo, camera);
        
        webglHelper.drawPrimitive(quadBufferInfo);
    };
      

    renderOpaqueStack(stack,camera){
            const gl=this.gl
            gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

            this.webglApiHelper.enableProperty(gl.DEPTH_TEST);
            this.webglApiHelper.disableProperty(gl.CULL_FACE);
            this.webglApiHelper.disableProperty(gl.BLEND);
            gl.depthFunc(gl.LESS);
            stack.forEach(({node,primitive}) => {
                this.renderPrimitive(node, primitive, camera);
        });
    }

    renderMeshTransparentNodes(){

    }

    renderPrimitive(node,primitive, camera){

        node.viewMatrix = m4.multiply(node.matrixWorld, camera.viewProjectionMatrix);
      
        const webglHelper = this.webglApiHelper;
        const programInfoRef = webglHelper.getProgramRef(primitive);
      
        if (programInfoRef.needUpdate) {
          const geometryProgramNeed = webglHelper.getGeometryProgramNeed(node, primitive);
          const materialProgramNeed = webglHelper.getMaterialProgramNeed(node, primitive);
          webglHelper.updateProgram(programInfoRef, geometryProgramNeed, materialProgramNeed);
        }
        const programInfo = webglHelper.loadProgram(programInfoRef,(program)=>{        
            webglHelper.setCommonUniforms(program, node);
        });
        
        webglHelper.setNodeUniforms(programInfo, node,camera);
        webglHelper.setPrimitiveBuffer(programInfo, primitive);
        webglHelper.setMorphTargetBuffer(programInfo, primitive);
        webglHelper.setMaterialBuffer(programInfo, primitive);
        
        // POINTS and LINES should have widths of 1px in viewport space.
        // For LINES with NORMAL and TANGENT properties, render with standard lighting including normal maps.
        // For POINTS or LINES with no TANGENT property, render with standard lighting but ignore any normal maps on the material.
        // For POINTS or LINES with no NORMAL property, don't calculate lighting and instead output the COLOR value for each pixel drawn.
        webglHelper.drawPrimitive(primitive);
    }
}