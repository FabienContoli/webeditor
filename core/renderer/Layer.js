import { generateUUID } from "./../helpers/uuidv4";

export default class Layer{

    /**
     * @type {HTMLCanvasElement}
     */
    canvas;

    /**
     * @type {(CanvasRenderingContext2D |WebGLRenderingContext)}
     */
    context;

    /**
     * @type {number}
     */
     width;

    /**
     * @type {number}
     */
    height;

    /**
     * @type {number}
     */
    offsetLeft

    /**
     * @type {number}
     */
    offsetTop
    
    /**
     * @type {('2d'|'webgl'|'webgl2')}
     */
    contextType;
    
    /**
     * 
     * @param {string}
     */
     uuid


    /**
     * @constructor
     * @param {HTMLElement} container
     * @param {('2d'|'webgl'|'webgl2')} contextType
     */
    constructor(container,contextType){
        this.container=container
        this.contextType=contextType
        this.uuid=generateUUID()
        this.canvas = document.createElement('canvas');
        this.container.appendChild(this.canvas);
        this.canvas.style.width="100%"
        this.canvas.style.height="100%"
        this.canvas.style.display="block"
        this.canvas.setAttribute('id', `${this.contextType}_${this.uuid}`);
        if(this.contextType==='2d'){
            this.getContext2D()
        }else {
            this.getContextWebgl()
        }
    }

    getContextWebgl(){
        this.context = this.canvas.getContext("webgl");
    }

    getContext2D(){
        this.context = this.canvas.getContext("2d");
    }

    detach(){
        this.canvas.remove()
    }


}