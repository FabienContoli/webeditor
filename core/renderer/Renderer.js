import WebglRenderer from "./WebglRenderer"
import RendererContainer from "./RendererContainer";

export default class Renderer{


    /**
     * @type {RendererContainer}
     */
     rendererContainer;
    
    /**
     * @type {HTMLElement}
     */
    containerHtml;
    
    /**
     * @type {WebglRenderer}
     */
    webglRenderer;
    
    /**
     * @type {WebGLRenderingContext}
     */
    gl;

    /**
     * @constructor
     * @param {HTMLElement} containerHtml
     */
    constructor(containerHtml){
        this.rendererContainer = new RendererContainer(containerHtml);
        this.gl = this.rendererContainer.addWebglLayer()
        this.webglRenderer= new WebglRenderer(this.rendererContainer,this.gl)
    }
    
    // picking(scene){

    // }

    render(scene){
        this.webglRenderer.render(scene)
    }

    resize(){
        this.webglRenderer.resize()
    }

    clear(){
        this.webglRenderer.clear()
    }

    destroy(){
        this.rendererContainer.destroy()
    }
}