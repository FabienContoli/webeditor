export default class GeometryProgramNeed {
  constructor() {
    this.flatNormal = false;
    this.useVextexColor = false;
    this.nbTexture = 0;
    this.useIndices = false;
    this.useTangent = false;
    this.isMorphed = false;
    this.nbMorphTargetPosition = 0;
    this.nbMorphTargetNormal = 0;
    this.nbMorphTargetTangent = 0;
    this.nbMorphWeight = 0;
    this.isSkinned = false;
    this.nbJoint = 0;
    this.nbUv = 0;
    this.envMap = false;
  }
}
