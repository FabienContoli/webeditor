import * as glUtils from './homemade-webgl-utils';
import * as m4 from './../../maths/m4';
import { generateVertexShader, generateFragmentShader } from './shaderUtils';
import { drawObjectFragmentShader as drawCubeMapFragmentShader, drawObjectVertexShader as drawCubeMapVertexShader } from './shaders/drawCubeMap';

import { fragmentShader as pickingFragmentShader, vertexShader as pickingVertexShader } from './shaders/pickingShader';
import GeometryProgramNeed from './GeometryProgramNeed';
import MaterialProgramNeed from './MaterialProgramNeed';
// import posXCubeMap from './defaultCubeMap/pos-x.jpg';
// import posYCubeMap from './defaultCubeMap/pos-y.jpg';
// import posZCubeMap from './defaultCubeMap/pos-z.jpg';
// import negXCubeMap from './defaultCubeMap/neg-x.jpg';
// import negYCubeMap from './defaultCubeMap/neg-y.jpg';
// import negZCubeMap from './defaultCubeMap/neg-z.jpg';

import posXCubeMap from './defaultCubeMap/bluecloud_lf.jpg';
import posYCubeMap from './defaultCubeMap/bluecloud_up.jpg';
import posZCubeMap from './defaultCubeMap/bluecloud_ft.jpg';
import negXCubeMap from './defaultCubeMap/bluecloud_rt.jpg';
import negYCubeMap from './defaultCubeMap/bluecloud_dn.jpg';
import negZCubeMap from './defaultCubeMap/bluecloud_bk.jpg';


import skybox from './default/skybox/circle.png';
import Primitive from '../../models/mesh/Primitive';
import { createProgram } from './webgl-utils';

const defaultCubeMap = {
  posX: posXCubeMap,
  negX: negXCubeMap,
  posY: posYCubeMap,
  negY: negYCubeMap,
  posZ: posZCubeMap,
  negZ: negZCubeMap,
};
const ligthUniforms = {
  u_lightWorldPos: [0, -10, 0],
  u_viewInverse: m4.identity(),
  u_lightColor: [1, 1, 1, 1],
};

export default class WebglApiHelper {
  constructor(gl) {
    this.gl = gl;
    this.viewport = [0, 0, gl.canvas.width, gl.canvas.heigh];
    this.frontFace = gl.CCW;
    this.glStatus = {
      [gl.DEPTH_TEST]: true,
      [gl.CULL_FACE]: true,
      [gl.BLEND]: false,
      [gl.blendFunc]: null,
    };
    this.bufferPrimitiveDico = {};
    this.bufferMorphTargetDico = {}
    this.currentState = {
      primitiveUuid: null,
      morphTargetUuid: null,
      materialUuid: null
    }
    this.quadBufferInfo = this.createXYQuadBufferInfo();
    this.currentProgram = null;
    this.programsInfo = {};
    this.primitiveProgramInfo = {};
    this.currentBufferInfo = null;
    this.texturesDico = {};
    this.defaulTexture = null;
    this.defaultMaterialPBR = null;
    this.extensions = {

    };
  }

  enableProperty(glProperty) {
    this.gl.enable(glProperty);
    this.glStatus[glProperty] = true;
  }

  disableProperty(glProperty) {
    this.gl.disable(glProperty);
    this.glStatus[glProperty] = false;
  }

  getProgramRef(primitive) {
    if (!this.primitiveProgramInfo[primitive.uuid]) {
      this.primitiveProgramInfo[primitive.uuid] = {
        programId: null,
        needUpdate: true,
      };
    }
    return this.primitiveProgramInfo[primitive.uuid];
  }

  generateStringFromObject(obj) {
    let str = '';
    Object.values(
      obj,
    ).forEach((value) => {
      if (typeof value === 'number') {
        str += value.toString();
      } else if (typeof value === 'boolean') {
        str += value ? '1' : '0';
      }
    });
    return str;
  }

  generateProgramID(geometryProgramNeed, materialProgramNeed) {
    let id = '';
    const idGeometry = this.generateStringFromObject(geometryProgramNeed);
    const idMaterial = this.generateStringFromObject(materialProgramNeed);
    id = idGeometry + idMaterial;
    return id;
  }

  generatePickingProgram() {
    const vertexShader = pickingVertexShader;
    const fragmentShader = pickingFragmentShader;

    console.log(vertexShader);
    console.log(fragmentShader);

    const ext = this.gl.getExtension('OES_element_index_uint');
    const ext2 = this.gl.getExtension('OES_standard_derivatives');
    const ext3 = this.gl.getExtension('WEBGL_draw_buffers');
    this.gl.getExtension('WEBGL_color_buffer_float');
    return glUtils.createProgramInfo(this.gl, [vertexShader, fragmentShader]);
  }

  updateProgramPicking() {
    if (!this.programsInfo.picking) {
      const programInfo = this.generatePickingProgram.call(this);
      this.programsInfo.picking = programInfo;
    }

    if (this.currentProgram !== this.programsInfo.picking) {
      this.gl.useProgram(this.programsInfo.picking.program);
      this.currentProgram = this.programsInfo.picking;
    }
    return this.programsInfo.picking;
  }

  generateCubeMapProgram() {
    const vertexShader = drawCubeMapVertexShader;
    const fragmentShader = drawCubeMapFragmentShader;

    console.log(vertexShader);
    console.log(fragmentShader);

    return glUtils.createProgramInfo(this.gl, [vertexShader, fragmentShader]);
  }

  updateProgramCubeMap() {
    if (!this.programsInfo.cubeMap) {
      const programInfo = this.generateCubeMapProgram();
      this.programsInfo.cubeMap = programInfo;
    }

    if (this.currentProgram !== this.programsInfo.cubeMap) {
      this.gl.useProgram(this.programsInfo.cubeMap.program);
      this.currentProgram = this.programsInfo.cubeMap;
    }
    return this.programsInfo.cubeMap;
  }

  generateProgram(programGeometryNeed, programMaterialNeed) {
    const vertexShader = generateVertexShader(programGeometryNeed, programMaterialNeed);
    const fragmentShader = generateFragmentShader(programGeometryNeed, programMaterialNeed);

    console.log(vertexShader);
    console.log(fragmentShader);

    const ext = this.gl.getExtension('OES_element_index_uint');
    const ext2 = this.gl.getExtension('OES_standard_derivatives');
    return glUtils.createProgramInfo(this.gl, [vertexShader, fragmentShader]);
  }

  updateProgram(programInfoRef, geometryProgramNeed, materialProgramNeed) {
    const programId = this.generateProgramID(geometryProgramNeed, materialProgramNeed);
    programInfoRef.programId = programId;
    if (!this.programsInfo[programId]) {
      const programInfo = this.generateProgram(geometryProgramNeed, materialProgramNeed);
      this.programsInfo[programId] = programInfo;
    }
    programInfoRef.needUpdate = false;
    return this.programsInfo[programId];
  }

  loadProgram(programInfoRef, postLoadCallBack) {
    if (programInfoRef.programId && this.currentProgram !== this.programsInfo[programInfoRef.programId]) {
      this.gl.useProgram(this.programsInfo[programInfoRef.programId].program);
      this.currentProgram = this.programsInfo[programInfoRef.programId];
      if (postLoadCallBack) postLoadCallBack(this.currentProgram)
    }
    return this.currentProgram;
  }

  
  createCropImageCanvas (img, sx, sy, sLargeur, sHauteur,dLargeur,dHauteur) {
    const temp = {
      canvas: document.createElement('canvas'),
      // canvas: document.querySelector('.canvas-debug'),

    };

    temp.canvas.setAttribute('height', sHauteur);
    temp.canvas.setAttribute('width', sLargeur);

    temp.canvas.style.width = `${sLargeur}+px`;
    temp.canvas.style.height = `${sHauteur}+px`;

    temp.canvas.style.imageRendering = '-moz-crisp-edges';
    temp.canvas.style.imageRendering = '-webkit-crisp-edges';
    temp.canvas.style.imageRendering = 'crisp-edges';
    temp.canvas.style.imageRendering = 'pixelated';
    temp.context = temp.canvas.getContext('2d');
    temp.context.imageSmoothingEnabled = false;
    temp.context.setTransform(1, 0, 0, 1, 0, 0);

    const dL = (dLargeur);
    const dH = (dHauteur);
    if (temp.canvas.width > 0 && temp.canvas.height > 0) {
      temp.context.drawImage(img, sx, sy, sLargeur, sHauteur, 0, 0, dL, dH);
      return temp.canvas
    }
    return null;
  };

  createCubeMapTextureFromOneFile() {
    let texture = this.texturesDico.cubeMap;
    if (!texture) {
      texture = this.gl.createTexture();
      this.gl.bindTexture(this.gl.TEXTURE_CUBE_MAP, texture);
      
      const faceInfos = [
        {
          target: this.gl.TEXTURE_CUBE_MAP_POSITIVE_X,
          sx:2,
          sy:1
        },
        {
          target: this.gl.TEXTURE_CUBE_MAP_NEGATIVE_X,
          sx:0,
          sy:1
        },
        {
          target: this.gl.TEXTURE_CUBE_MAP_POSITIVE_Y,
          sx:1,
          sy:0
        },
        {
          target: this.gl.TEXTURE_CUBE_MAP_NEGATIVE_Y,
          sx:1,
          sy:2
        },
        {
          target: this.gl.TEXTURE_CUBE_MAP_POSITIVE_Z,
          sx:1,
          sy:1
        },
        {
          target: this.gl.TEXTURE_CUBE_MAP_NEGATIVE_Z,
          sx:3,
          sy:1
        },
      ];

      
      faceInfos.forEach((faceInfo) => {
          const { target } = faceInfo;

          // Upload the canvas to the cubemap face.
          const level = 0;
          const internalFormat = this.gl.RGBA;
          const width = 512;
          const height = 512;
          const format = this.gl.RGBA;
          const type = this.gl.UNSIGNED_BYTE;

          // setup each face so it's immediately renderable
          this.gl.texImage2D(target, level, internalFormat, width, height, 0, format, type, null);
      })

      const image = new Image();
      image.src = skybox;
    
      image.addEventListener('load', () => {
        const size =image.width/4;
        // const size = (256>imageWidth && this.isPowerOf2(imageWidth)) ?imageWidth:256;
        faceInfos.forEach((faceInfo) => {
            
            const canvas = this.createCropImageCanvas(image, faceInfo.sx*size, faceInfo.sy*size, size, size, 512, 512)
            const { target } = faceInfo;

            // Upload the canvas to the cubemap face.
            const level = 0;
            const internalFormat = this.gl.RGBA;
            const format = this.gl.RGBA;
            const type = this.gl.UNSIGNED_BYTE;

            this.gl.bindTexture(this.gl.TEXTURE_CUBE_MAP, texture);
            
            this.gl.texImage2D(target, level, internalFormat, format, type, canvas);
      
            this.gl.generateMipmap(this.gl.TEXTURE_CUBE_MAP);
            
          });
        });
      this.gl.generateMipmap(this.gl.TEXTURE_CUBE_MAP);
      this.gl.texParameteri(this.gl.TEXTURE_CUBE_MAP, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR_MIPMAP_LINEAR);
      this.texturesDico.cubeMap = texture;
    }
    return texture;
  }

  createCubeMapTexture() {
    // Create a texture.
    let texture = this.texturesDico.cubeMap;
    if (!texture) {
      texture = this.gl.createTexture();
      this.gl.bindTexture(this.gl.TEXTURE_CUBE_MAP, texture);

      const faceInfos = [
        {
          target: this.gl.TEXTURE_CUBE_MAP_POSITIVE_X,
          url: defaultCubeMap.posX,
        },
        {
          target: this.gl.TEXTURE_CUBE_MAP_NEGATIVE_X,
          url: defaultCubeMap.negX,
        },
        {
          target: this.gl.TEXTURE_CUBE_MAP_POSITIVE_Y,
          url: defaultCubeMap.posY,
        },
        {
          target: this.gl.TEXTURE_CUBE_MAP_NEGATIVE_Y,
          url: defaultCubeMap.negY,
        },
        {
          target: this.gl.TEXTURE_CUBE_MAP_POSITIVE_Z,
          url: defaultCubeMap.posZ,
        },
        {
          target: this.gl.TEXTURE_CUBE_MAP_NEGATIVE_Z,
          url: defaultCubeMap.negZ,
        },
      ];
      faceInfos.forEach((faceInfo) => {
        const { target, url } = faceInfo;

        // Upload the canvas to the cubemap face.
        const level = 0;
        const internalFormat = this.gl.RGBA;
        const width = 512;
        const height = 512;
        const format = this.gl.RGBA;
        const type = this.gl.UNSIGNED_BYTE;

        // setup each face so it's immediately renderable
        this.gl.texImage2D(target, level, internalFormat, width, height, 0, format, type, null);

        // Asynchronously load an image
        const image = new Image();
        image.src = url;
        image.addEventListener('load', () => {
          // Now that the image has loaded make copy it to the texture.
          this.gl.bindTexture(this.gl.TEXTURE_CUBE_MAP, texture);
          this.gl.texImage2D(target, level, internalFormat, format, type, image);
          this.gl.generateMipmap(this.gl.TEXTURE_CUBE_MAP);
        });
      });
      this.gl.generateMipmap(this.gl.TEXTURE_CUBE_MAP);
      this.gl.texParameteri(this.gl.TEXTURE_CUBE_MAP, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR_MIPMAP_LINEAR);
      this.texturesDico.cubeMap = texture;
    }
    return texture;
  }

  createTexture(textureObject) {
    if (this.texturesDico[textureObject.uuid]) return this.texturesDico[textureObject.uuid];
    const { gl } = this;
    const texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);

    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, textureObject.image);
    // Check if the image is a power of 2 in both dimensions.
    if (this.isPowerOf2(textureObject.image.width) && this.isPowerOf2(textureObject.image.height)) {
      // Yes, it's a power of 2. Generate mips.

      // Set the parameters so we can render any size image.
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, textureObject.wrapS);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, textureObject.wrapT);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, textureObject.minFilter);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, textureObject.magFilter);

      /*  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
       gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
       gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
       gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST); */
      gl.generateMipmap(gl.TEXTURE_2D);
    } else {
      // No, it's not a power of 2. Turn off mips and set wrapping to clamp to edge
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    }

    this.texturesDico[textureObject.uuid] = texture;
    return texture;
  }

  getDefaultMrTexture(num = 1) {
    // at init time.
    const { gl } = this;
    if (!this.defaultMaterialPBR) {
      this.defaultMaterialPBR = gl.createTexture();

      gl.activeTexture(gl.TEXTURE0 + num);
      gl.bindTexture(gl.TEXTURE_2D, this.defaultMaterialPBR);
      gl.texParameterf(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
        new Uint8Array([255, 255, 255, 255]));
    }
    return this.defaultMaterialPBR;
  }

  createDefaultTexture(num = 0) {
    // at init time.
    const { gl } = this;
    if (!this.defaulTexture) {
      this.defaulTexture = gl.createTexture();

      gl.activeTexture(gl.TEXTURE0 + num);
      gl.bindTexture(gl.TEXTURE_2D, this.defaulTexture);
      gl.texParameterf(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
        new Uint8Array([255, 255, 255, 255]));
    }
    return this.defaulTexture;
  }

  getDefaultTexture(gl) {
    if (!this.defaulTexture) this.defaulTexture = this.createDefaultTexture(gl);
    return this.defaulTexture;
  }

  isPowerOf2(value) {
    return (value & (value - 1)) == 0;
  }

  getGeometryProgramNeed(node, primitive) {
    const geometryProgramNeed = new GeometryProgramNeed();

    if (primitive.flatNormal) {
      geometryProgramNeed.flatNormal = true;
    } else {
      geometryProgramNeed.flatNormal = false;
    }

    if (primitive.attributes.tangent) {
      geometryProgramNeed.useTangent = true;
    } else {
      geometryProgramNeed.useTangent = false;
    }

    if (primitive.attributes.color) {
      geometryProgramNeed.useVextexColor = true;
    } else {
      geometryProgramNeed.useVextexColor = false;
    }

    if (primitive.indices) {
      geometryProgramNeed.useIndices = true;
    } else {
      geometryProgramNeed.useIndices = false;
    }

    if (primitive.targets) {
      const { weights } = node;
      let nbMorphTargetPosition = 0;
      let nbMorphTargetNormal = 0;
      const nbMorphTargetTangent = 0;
      geometryProgramNeed.isMorphed = true;
      geometryProgramNeed.morphWeight = weights.length;
      primitive.targets.forEach((target) => {
        if (target.position) {
          nbMorphTargetPosition++;
        }
        if (target.normale) {
          nbMorphTargetNormal++;
        }
        if (target.tangent) {
          // nbMorphTargetTangent++;
        }
      });
      geometryProgramNeed.nbMorphTargetPosition = nbMorphTargetPosition;
      geometryProgramNeed.nbMorphTargetNormal = nbMorphTargetNormal;
      geometryProgramNeed.nbMorphTargetTangent = nbMorphTargetTangent;
    } else {
      geometryProgramNeed.isMorphed = false;
    }

    if (node.skin) {
      geometryProgramNeed.isSkinned = true;
      geometryProgramNeed.nbJoint = node.skin.jointMatrices.length;
    }

    if (primitive.attributes.texCoord0) {
      geometryProgramNeed.nbUv += 1;
    }
    if (primitive.attributes.texCoord1) {
      geometryProgramNeed.nbUv += 1;
    }
    return geometryProgramNeed;
  }

  getMaterialProgramNeed(node, primitive) {
    const materialProgramNeed = new MaterialProgramNeed();
    const { material } = primitive;
    const {
      normalTexture, occlusionTexture, emissiveTexture, alphaMode, alphaCutoff,
    } = material;

    if (material.pbrMetallicRoughness) {
      const {
        baseColorTexture, metallicRoughnessTexture,
      } = material.pbrMetallicRoughness;

      if (baseColorTexture) {
        materialProgramNeed.getBaseColorTexture = true;
        materialProgramNeed.baseColorTextureCoord = baseColorTexture.texCoord;
      }

      if (metallicRoughnessTexture) {
        materialProgramNeed.getMetallicRoughnessTexture = true;
        materialProgramNeed.metallicRoughnessTextureCoord = metallicRoughnessTexture.texCoord;
      }
    }

    if (emissiveTexture) {
      materialProgramNeed.getEmissiveTexture = true;
      materialProgramNeed.emissiveTextureCoord = emissiveTexture.texCoord;
    }

    if (normalTexture) {
      materialProgramNeed.getNormalTexture = true;
      materialProgramNeed.normalTextureCoord = normalTexture.texCoord;
    }

    if (occlusionTexture) {
      materialProgramNeed.getOcclusionTexture = true;
      materialProgramNeed.occlusionTextureCoord = occlusionTexture.texCoord;
    }

    if (material.doubleSided) {
      materialProgramNeed.doubleSided = true;
    }

    return materialProgramNeed;
  }

  setPrimitiveBuffer(programInfo, primitive) {

    if (this.currentState.primitiveUuid === primitive.uuid) {
      return;
    }

    if (!this.bufferPrimitiveDico[primitive.uuid]) {
      this.bufferPrimitiveDico[primitive.uuid] = glUtils.createBuffersFromPrimitive(this.gl, primitive);
    }

    glUtils.setBuffersAndAttributes(this.gl, programInfo, this.bufferPrimitiveDico[primitive.uuid]);

    this.currentState.primitiveUuid = primitive.uuid
  }

  setMorphTargetBuffer(programInfo, primitive) {

    if (primitive.targets) {
      if (this.currentState.morphTargetUuid === primitive.uuid) {
        return
      }

      let positionTarget = 0;
      let normalTarget = 0;
      let tangentTarget = 0;
      let morphTargetAttributes = {}

      primitive.targets.forEach((target) => {
        if (target.position) {
          morphTargetAttributes[`morphTargetsPosition_${positionTarget}`] = target.position
          positionTarget += 1;
        }
        if (target.normal) {
          morphTargetAttributes[`morphTargetsNormal_${normalTarget}`] = target.normal
          normalTarget += 1;
        }
        if (target.tangent) {
          morphTargetAttributes[`morphTargetsTangent_${tangentTarget}`] = target.tangent
          tangentTarget += 1;
        }
      });
      if (!this.bufferMorphTargetDico[primitive.uuid]) {
        this.bufferMorphTargetDico[primitive.uuid] = glUtils.createBuffersFromPrimitive(this.gl, { attributes: morphTargetAttributes });
      }

      glUtils.setBuffersAndAttributes(this.gl, programInfo, this.bufferMorphTargetDico[primitive.uuid]);

      this.currentState.morphTargetUuid = primitive.uuid
    }
  }

  setNodeUniforms(programInfo, node, camera, index) {

    const frontFaceCW = m4.determinate(node.viewMatrix) < 0;

    // const flipSided = (material.side === BackSide);
    if (!frontFaceCW) {
      this.gl.frontFace(2304);
    } else {
      this.gl.frontFace(2305);
    }

    const geometryUniforms =
    {
      u_worldViewProjection: camera.viewProjectionMatrix,
      u_worldCameraPosition: camera.worldPosition,
    }

    let u_world = node.matrixWorld;
    if (node.skin) {
      if (node.skin.skeleton) {
        geometryUniforms.u_world = node.skin.skeleton.matrixWorld;
      }
      geometryUniforms.u_jointMatrix = node.skin.boneArray;
    }

    const worldInverseMatrix = m4.inverse(u_world);

    const worldInverseTransposeMatrix = m4.transpose(worldInverseMatrix);


    geometryUniforms.u_world = u_world;
    geometryUniforms.u_worldInverseTranspose = worldInverseTransposeMatrix;


    if (index) {
      geometryUniforms.u_id = [
        ((index >> 0) & 0xFF) / 0xFF,
        ((index >> 8) & 0xFF) / 0xFF,
        ((index >> 16) & 0xFF) / 0xFF,
        ((index >> 24) & 0xFF) / 0xFF,
      ];
    }

    const { weights } = node;
    if (weights) {
      geometryUniforms.u_morphWeights = weights;
    }

    glUtils.setUniforms(programInfo.uniformSetters, geometryUniforms);
  }

  setCommonUniforms(programInfo, camera) {

    const commonUniforms = { ...ligthUniforms }
    if (true || camera.cubeMap) {
      const texture = this.createCubeMapTexture();
      commonUniforms.cubeMap = texture;
    }

    commonUniforms.u_lightWorldPos = [camera.matrixWorld[12], camera.matrixWorld[13], camera.matrixWorld[14]]
    commonUniforms.u_viewInverse = camera.viewMatrix
    glUtils.setUniforms(programInfo.uniformSetters, commonUniforms);

  }

  setMaterialBuffer(programInfo, primitive) {
    const { gl } = this;
    const { material } = primitive;

    if (this.currentState.materialUuid === material.uuid) {
      return;
    }

    const {
      normalTexture, occlusionTexture, emissiveFactor, emissiveTexture, alphaMode, alphaCutoff,
    } = material;


    const materialUniforms = {

    }

    materialUniforms.u_emissiveFactor = emissiveFactor
    materialUniforms.alphaMode = (alphaMode === 'OPAQUE') ? 0 : alphaCutoff
    if (material.pbrMetallicRoughness) {

      const {
        baseColorFactor, baseColorTexture, metallicRoughnessTexture, metallicFactor, roughnessFactor,
      } = material.pbrMetallicRoughness;

      materialUniforms.u_baseColorFactor = baseColorFactor;
      materialUniforms.u_metalness = metallicFactor;
      materialUniforms.u_roughness = roughnessFactor


      if (baseColorTexture) {
        materialUniforms.u_baseColorTexture = this.createTexture(baseColorTexture.texture);
      }

      if (metallicRoughnessTexture) {
        materialUniforms.u_metallicRoughnessTexture = this.createTexture(metallicRoughnessTexture.texture);
      }
    }
    if (emissiveTexture) {
      materialUniforms.u_emissiveTexture = this.createTexture(emissiveTexture.texture);
    }

    if (normalTexture) {
      materialUniforms.u_normalTexture = this.createTexture(normalTexture.texture);
    }
    if (occlusionTexture) {
      materialUniforms.occlusionTexture = this.createTexture(occlusionTexture.texture);
    }

    if (material.doubleSided && this.glStatus[this.gl.CULL_FACE]) {
      this.disableProperty(this.gl.CULL_FACE);
    } else if (!material.doubleSided && !this.glStatus[this.gl.CULL_FACE]) {
      this.enableProperty(this.gl.CULL_FACE);
    }

    glUtils.setUniforms(programInfo.uniformSetters, materialUniforms);

    this.currentState.materialUuid = material.uuid
    /* if no color
          gl.disableVertexAttribArray(colorLoc);
        gl.vertexAttrib4f(colorLoc, 1, 1, 1, 1);
      */
    // Set the uniforms.
  }

  createBufferInfoFunc(fn) {
    return () => {
      const primitive = fn.apply(null, Array.prototype.slice.call(arguments, 1));
      this.bufferPrimitiveDico["cubeMap"] = glUtils.createBuffersFromPrimitive(this.gl, primitive);
      return primitive
    };
  }

  createXYQuadVertices(size, xOffset, yOffset) {
    size = size || 2;
    xOffset = xOffset || 0;
    yOffset = yOffset || 0;
    size *= 0.5;
    return new Primitive({
      attributes: {
        position: {
          normalized: false,
          componentType: 5126,
          itemSize: 2,
          array: new Float32Array([
            xOffset + -1 * size, yOffset + -1 * size,
            xOffset + 1 * size, yOffset + -1 * size,
            xOffset + -1 * size, yOffset + 1 * size,
            xOffset + 1 * size, yOffset + 1 * size,
          ]),
          offset: 0,
          stride: 0
        }
      },
      indices: {
        normalize: false, itemSize: 3, componentType: 5123, array: new Uint16Array([0, 1, 2, 2, 1, 3]), offset: 0, stride: 0, count: 6
      },
      mode: 4
    })
  }

  createXYQuadBufferInfo() {
    return this.createBufferInfoFunc(this.createXYQuadVertices)();
  }

  drawPrimitive(bufferInfo) {
    glUtils.drawPrimitive(this.gl, bufferInfo);
  }

  setCubeMapBuffer(programInfo, camera) {
    const texture = this.createCubeMapTextureFromOneFile();
    // We only care about direciton so remove the translation
    const viewDirectionMatrix = m4.copy(camera.viewMatrix);
    viewDirectionMatrix[12] = 0;
    viewDirectionMatrix[13] = 0;
    viewDirectionMatrix[14] = 0;

    const viewDirectionProjectionMatrix = m4.multiply(
      camera.projectionMatrix, viewDirectionMatrix,
    );

    const viewDirectionProjectionInverseMatrix = m4.inverse(viewDirectionProjectionMatrix);
    glUtils.setBuffersAndAttributes(this.gl, programInfo, this.bufferPrimitiveDico["cubeMap"]);
    glUtils.setUniforms(programInfo.uniformSetters, {
      u_viewDirectionProjectionInverse: viewDirectionProjectionInverseMatrix,
      u_cubeMap: texture,
    });
    return this.quadBufferInfo;
  }
}
