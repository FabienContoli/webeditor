// https://www.khronos.org/registry/glTF/specs/2.0/glTF-2.0.html#complete-model
// https://google.github.io/filament/Filament.md.html#materialsystem/standardmodelsummary

const D_GGX_optimize = `
    #define MEDIUMP_FLT_MAX    65504.0
    #define saturateMediump(x) min(x, MEDIUMP_FLT_MAX)
    float D_GGX_optimize(float roughness, float NoH, const vec3 n, const vec3 h) {
    vec3 NxH = cross(n, h);
    float a = NoH * roughness;
    float k = roughness / (dot(NxH, NxH) + a * a);
    float d = k * k * (1.0 / PI);
    return saturateMediump(d);
}`;

const D_GGX = `float D_GGX(float NoH, float roughness ) {
    float a2 = roughness  * roughness ;
    float f = (NoH * a2 - NoH) * NoH + 1.0;
    return a2 / (PI * f * f);
}`;

const F_Schlick = `vec3 F_Schlick(float u, vec3 f0) {
    return f0 + (vec3(1.0) - f0) * pow(1.0 - u, 5.0);
}`;

const V_SmithGGXCorrelated = `float V_SmithGGXCorrelated(float NoV, float NoL, float a) {
    float a2 = a * a;
    float GGXL = NoV * sqrt((-NoL * a2 + NoL) * NoL + a2);
    float GGXV = NoL * sqrt((-NoV * a2 + NoV) * NoV + a2);
    return 0.5 / (GGXV + GGXL);
}`;

const V_SmithGGXCorrelatedFast = `float V_SmithGGXCorrelatedFast(float NoV, float NoL, float roughness) {
    float a = roughness;
    float GGXV = NoL * (NoV * (1.0 - a) + a);
    float GGXL = NoV * (NoL * (1.0 - a) + a);
    return 0.5 / (GGXV + GGXL);
}`;

const specularBrdf = `vec3 specular_brdf(float D_GGX_value,float V_SmithGGXCorrelated_value,vec3 F_Schlick_value) {
  return (D_GGX_value * V_SmithGGXCorrelated_value) * F_Schlick_value;
}`;

const diffuseBrdf = `vec3 diffuse_brdf(vec3 diffuseColor_value) {
  return (1.0/PI)* diffuseColor_value;
}`;// diffuseBrdf =diffuseColor * F_lambert

const fresnelMix = `vec4 fresnel_mix(float ior, vec4 base, float layer) {
  vec4 f0 = ((1-ior)/(1+ior))^2;
  vec4 fr = f0 + (1 - f0)*(1 - abs(VdotH))^5;
  return mix(base, layer, fr);
}`;

const conductorFresnel = `vec4 conductor_fresnel(vec4 f0, float bsdf) {
  return bsdf * (f0 + (1 - f0) * (1 - abs(VdotH))^5);
}`;

export default [D_GGX, F_Schlick, V_SmithGGXCorrelated, specularBrdf, diffuseBrdf];// [specularBrdf, diffuseBrdf, conductorFresnel, fresnelMix];
