//  declareUniform(Type.vec4, 'u_specular'),
//  declareUniform(Type.float, 'u_shininess'),
//  declareUniform(Type.float, 'u_specularFactor'),
export default `vec4 lit(float l ,float h, float m) {
  return vec4(1.0,
              abs(l),
              (l > 0.0) ? pow(max(0.0, h), m) : 0.0,
              1.0);
}`;

/*
let mainStartContents = [
  'vec4 diffuseColor = texture2D(u_diffuse, v_texCoord);',
  'if( diffuseColor.a < u_alpha )discard ;',
  'vec3 a_normal = normalize(v_normal);',,
  'vec3 surfaceToLight = normalize(v_surfaceToLight);',
  'vec3 surfaceToView = normalize(v_surfaceToView);',
  'vec3 halfVector = normalize(surfaceToLight + surfaceToView);',
];

*/
/*
const mainContents = [
  'vec4 litR = lit(dot(a_normal, surfaceToLight),dot(a_normal, halfVector), u_shininess);',
  `vec4 outColor = vec4((u_lightColor * (diffuseColor * litR.y * u_colorMult
  + u_specular * litR.z * u_specularFactor)).rgb,
    diffuseColor.a);`];
  */
