const vertexShader = `attribute vec4 a_position;

uniform mat4 u_world;
uniform mat4 u_worldViewProjection;

void main() {
  // Multiply the position by the matrix.

  gl_Position = u_worldViewProjection* u_world * a_position;
}`;

const fragmentShader = `precision mediump float;

  uniform vec4 u_id;

  void main() {
     gl_FragColor = u_id;
  }`;

export { vertexShader, fragmentShader };
