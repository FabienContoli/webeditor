const drawObjectVertexShader = `
attribute vec4 position;
varying vec4 v_position;
void main() {
  v_position = position;
  gl_Position = vec4(position.xy, 1, 1);
}`;

const drawObjectFragmentShader = `
precision mediump float;

uniform samplerCube u_cubeMap;
uniform mat4 u_viewDirectionProjectionInverse;

varying vec4 v_position;
void main() {
  
  vec4 t = u_viewDirectionProjectionInverse * v_position;
  gl_FragColor = textureCube(u_cubeMap, normalize(t.xyz / t.w));
  
}`;

export { drawObjectFragmentShader, drawObjectVertexShader };
