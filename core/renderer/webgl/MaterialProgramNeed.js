export default class MaterialProgramNeed {
  constructor() {
    this.getMetallicRoughnessTexture = false;
    this.metallicRoughnessTextureCoord = 0;
    this.getBaseColorTexture = false;
    this.baseColorTextureCoord = 0;
    this.getOcclusionTexture = false;
    this.occlusionTextureCoord = 0;
    this.getNormalTexture = false;
    this.normalTextureCoord = 0;
    this.getEmissiveTexture = false;
    this.emissiveTextureCoord = 0;
    this.doubleSided = false;
    this.useAlphaTest = false;
  }
}
