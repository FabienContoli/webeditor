import pbrLit from './shaders/functions/pbrLit';

const Type = {
  float: 'float',
  mat3: 'mat3',
  mat4: 'mat4',
  vec2: 'vec2',
  vec3: 'vec3',
  vec4: 'vec4',
  sampler2D: 'sampler2D',
};

const generateOutPosition = function generateOutPosition(positionValue) {
  return `gl_Position=${positionValue};`;
};

const generateOutColor = function generateOutColor(valueColor) {
  return `gl_FragColor=${valueColor};`;
};

const declareConst = function declareConst(type, name) {
  return `#define ${type} ${name};`;
};

const declareAttribute = function declareAttribute(type, name) {
  return `attribute ${type} ${name};`;
};

const declareUniform = function declareUniform(type, name) {
  return `uniform ${type} ${name};`;
};

const declareVarying = function declareVarying(type, name) {
  return `varying ${type} ${name};`;
};

const generateMainFunction = function generateMainFunction(mainContents) {
  return `\nvoid main() {\n${
    mainContents.join('\n')
  }\n}`;
};

const createForLoop = function createForLoop(count, innerCodeArray) {
  const startForLoop = `for(int i=0;i<int(${count});++i){`;
  const end = '}';
  return [startForLoop, ...innerCodeArray, end];
};

const generateShader = function generateShader(constVariables, uniformVariables, attributeVariables, varyingVariables, helperFunctions, mainStartContents, mainContents, mainEndContents) {
  const variableDeclaration = [...constVariables, ...uniformVariables, ...attributeVariables, ...varyingVariables, ...helperFunctions].join('\n');
  const mainFunction = generateMainFunction([...mainStartContents, ...mainContents, ...mainEndContents]);
  return variableDeclaration + mainFunction;
};

/*
programGeometryNeed
  this.useVextexColor = false;
  this.useIndices = false;
  this.useTangent = false;
  this.isMorphed = false;
  this.nbMorphTargetPosition = 0;
  this.nbMorphTargetNormal = 0;
  this.nbMorphTargetTangent = 0;
  this.isSkinned = false;
  this.nbJoint=0
*/

const generateVertexShader = function generateVertexShader(programGeometryNeed, programMaterialNeed) {
  const constVariables = [];

  const uniformVariables = [declareUniform(Type.mat4, 'u_viewInverse'),
    declareUniform(Type.mat4, 'u_worldViewProjection'),
    declareUniform(Type.mat4, 'u_world'),
    declareUniform(Type.mat4, 'u_matrix'),
    declareUniform(Type.vec3, 'u_lightWorldPos'),
    declareUniform(Type.mat4, 'u_worldInverseTranspose'),
    declareUniform(Type.vec3, 'u_worldCameraPosition')];

  const attributeVariables = [declareAttribute(Type.vec4, 'position'),
    declareAttribute(Type.vec3, 'normal')];

  const varyingVariables = [declareVarying(Type.vec4, 'v_position'),
    declareVarying(Type.vec3, 'v_normal'),
    declareVarying(Type.vec3, 'v_surfaceToLight'),
    declareVarying(Type.vec3, 'v_surfaceToView')];

  const helperFunctions = [];

  let mainStartContents = [
    'v_position = position;',
    'v_normal =normal;',
    'mat4 world = u_world ;'];
  const mainContents = ['v_position = u_worldViewProjection * world *v_position;'];
  const mainEndContents = [
    'v_normal = (u_worldInverseTranspose * vec4(v_normal, 0)).xyz;',
    'vec3 surfaceWorldPosition = (world * v_position).xyz;',
    'v_surfaceToLight = u_lightWorldPos - surfaceWorldPosition;',
    // 'v_surfaceToView = (u_viewInverse[3] - surfaceWorldPosition;',
    'v_surfaceToView = vec3(u_worldCameraPosition)  - surfaceWorldPosition;',
    generateOutPosition('v_position')];

  for (let i = 0; i < programGeometryNeed.nbUv; i += 1) {
    const aTexCoord = declareAttribute(Type.vec2, `texCoord${i}`);
    attributeVariables.push(aTexCoord);
    const vTexCoord = declareVarying(Type.vec2, `v_texCoord${i}`);
    varyingVariables.push(vTexCoord);
    const code = [`v_texCoord${i}  = texCoord${i};`];
    mainStartContents = [...mainStartContents, ...code];
  }

  if (programGeometryNeed.useVextexColor) {
    const aColor = declareAttribute(Type.vec4, 'color');
    attributeVariables.push(aColor);
    const vColor = declareVarying(Type.vec4, 'v_color');
    varyingVariables.push(vColor);
    const code = ['v_color = color;'];
    mainStartContents = [...mainStartContents, ...code];
  }

  if (programGeometryNeed.useTangent) {
    const aTangent = declareAttribute(Type.vec4, 'tangent');
    attributeVariables.push(aTangent);
    const vTangent = declareVarying(Type.vec3, 'v_tangent');
    varyingVariables.push(vTangent);
    const vBitangent = declareVarying(Type.vec4, 'v_bitangent');
    varyingVariables.push(vBitangent);
    const tangentStartCode = [
      'v_tangent = vec3( tangent.xyz );',
      'v_bitangent = vec4(cross(v_normal.xyz, tangent.xyz),tangent.w);'];
    mainStartContents = [...mainStartContents, ...tangentStartCode];
  }

  if (programGeometryNeed.isMorphed) {
    // TODO: create texture from morph buffer
    const morphWeight = declareUniform(Type.float, `u_morphWeights[${programGeometryNeed.morphWeight}]`);
    uniformVariables.push(morphWeight);

    if (programGeometryNeed.nbMorphTargetPosition > 0) {
      const code = [];
      for (let i = 0; i < programGeometryNeed.nbMorphTargetPosition; i += 1) {
        const morphTarget = declareAttribute(Type.vec3, `morphTargetsPosition_${i}`);
        attributeVariables.push(morphTarget);
        code.push(`v_position += u_morphWeights[${i}] * vec4(morphTargetsPosition_${i}.xyz,0.0 );`);
      }
      mainStartContents = [...mainStartContents, ...code];
    }
    if (programGeometryNeed.nbMorphTargetNormal > 0) {
      const code = [];
      for (let i = 0; i < programGeometryNeed.nbMorphTargetNormal; i += 1) {
        const morphTarget = declareAttribute(Type.vec3, `morphTargetsNormal_${i}`);
        attributeVariables.push(morphTarget);
        code.push(`v_normal += vec4((u_morphWeights[${i}] * vec3(morphTargetsNormal_${i}.xyz)),0.0);`);
      }
      mainStartContents = [...mainStartContents, ...code];
    }
    if (programGeometryNeed.nbMorphTargetTangent > 0 && programGeometryNeed.useTangent) {
      const code = [];
      for (let i = 0; i < programGeometryNeed.nbMorphTargetTangent; i += 1) {
        const morphTarget = declareAttribute(Type.vec3, `morphTargetsTangent_${i}`);
        attributeVariables.push(morphTarget);
        code.push(`v_tangent +=  u_morphWeights[${i}] * vec4(morphTargetsTangent_${i}.xyz,0.0 );`);
      }
      mainStartContents = [...mainStartContents, ...code];
    }
  }

  if (programGeometryNeed.isSkinned) {
    const jointCount = declareConst('JOINT_COUNT', programGeometryNeed.nbJoint);
    constVariables.push(jointCount);
    const jointMatrix = declareUniform(Type.mat4, `u_jointMatrix[${programGeometryNeed.nbJoint}]`);
    uniformVariables.push(jointMatrix);
    const joint = declareAttribute(Type.vec4, 'joint');
    attributeVariables.push(joint);
    const weight = declareAttribute(Type.vec4, 'weight');
    attributeVariables.push(weight);
    const code = [
      'mat4 skinMatrix = mat4(0.0);',
      'skinMatrix += weight.x * u_jointMatrix[int(joint.x)];',
      'skinMatrix += weight.y * u_jointMatrix[int(joint.y)];',
      'skinMatrix += weight.z * u_jointMatrix[int(joint.z)];',
      'skinMatrix += weight.w * u_jointMatrix[int(joint.w)];',
      'world = world * skinMatrix;',
      'v_normal = vec4( skinMatrix * vec4( v_normal, 0.0 ) ).xyz;',
    ];
    mainStartContents = [...mainStartContents, ...code];
  }

  return generateShader(constVariables, uniformVariables, attributeVariables, varyingVariables, helperFunctions, mainStartContents, mainContents, mainEndContents);
};

const generateFragmentShader = function generateFragmentShader(programGeometryNeed, programMaterialNeed) {
  const constVariables = ['#define PI 3.1415926535897932384626433832795'];

  const uniformVariables = ['precision mediump float;',
    declareUniform(Type.vec4, 'u_lightColor'),
    declareUniform(Type.vec4, 'u_baseColorFactor'),
    declareUniform(Type.float, 'u_alpha'),
    declareUniform(Type.float, 'u_metallic'),
    declareUniform(Type.float, 'u_roughness')];

  const attributeVariables = [];

  const varyingVariables = [declareVarying(Type.vec4, 'v_position'),
    declareVarying(Type.vec3, 'v_normal'),
    declareVarying(Type.vec3, 'v_surfaceToLight'),
    declareVarying(Type.vec3, 'v_surfaceToView')];

  const helperFunctions = [...pbrLit];

  let mainStartContents = [
    'float fogAmount = smoothstep(0.9999, 1.0, gl_FragCoord.z);',
    'baseColor = mix(baseColor, vec4(0.0), fogAmount);',
    'float alpha = baseColor.a;',
  //  'if( baseColor.a < u_alpha )discard ;',
    'vec3 normal = normalize(v_normal);',
    'vec3 surfaceToLight = normalize(v_surfaceToLight);',
    'vec3 surfaceToView = normalize(v_surfaceToView);',
    'vec3 halfVector = normalize(surfaceToView + surfaceToLight);',
    'float metallic = u_metallic;',
    'float perceptualRoughness = u_roughness;',
  ];

  let mainContents = [
    'vec3 f0 = (1.0 - metallic) * baseColor.rgb;',
    'perceptualRoughness += 0.01;',
    'float reflectance = 0.04;',
    // 'vec3 f0 = 0.16 * reflectance * reflectance * (1.0 - metallic) + baseColor.rgb * metallic;',
    //  'vec3 f0 = 0.16 * reflectance * reflectance * (1.0 - metallic) + baseColor.rgb * metallic;',
    'float NoV = abs(dot(normal, surfaceToView)) + 1e-5;',
    'float NoL = clamp(dot(normal, surfaceToLight), 0.1, 1.0);',
    'float NoH = clamp(dot(normal, halfVector), 0.0, 1.0);',
    'float LoH = clamp(dot(surfaceToLight, halfVector), 0.0, 1.0);',
    'float roughness = perceptualRoughness * perceptualRoughness;',
    'float D = D_GGX(NoH, roughness);',
    'vec3  F = F_Schlick(LoH, f0);',
    'float V = V_SmithGGXCorrelated(NoV, NoL, roughness);',
    'vec3 specularBrdfColor = specular_brdf(D ,V, F);',
    'vec3 diffuseBrdfColor = diffuse_brdf( baseColor.rgb);',
    'float energyCompensation = 1.0;',
    // 'vec3 color = (diffuseBrdfColor + specularBrdfColor * energyCompensation) * NoL;',
    //  'vec4 outColor = (color * lightColor) * (light.color.a * light.attenuation * illuminated);',

    // 'vec4 outColor =vec4(color * vec3(u_lightColor), alpha);',
    //  'vec4 outColor =vec4( (NoL * vec3(u_lightColor) * (diffuseBrdfColor + specularBrdfColor)), alpha);',
    'vec4 outColor =vec4( (vec3(u_lightColor) * (diffuseBrdfColor + specularBrdfColor)), 1.0);', // pb specular  or f0
    // 'outColor = vec4(normalize(normal)* .5 + .5, 1);',
  ];

 // const mainEndContents = [ generateOutColor('vec4(1.0,0.0,0.0,1.0)')]
 const mainEndContents = [generateOutColor('outColor')];

  for (let i = 0; i < programGeometryNeed.nbUv; i += 1) {
    const vTexCoord = declareVarying(Type.vec2, `v_texCoord${i}`);
    varyingVariables.push(vTexCoord);
  }

  if (programGeometryNeed.useVextexColor) {
    const vColor = declareVarying(Type.vec4, 'v_color');
    varyingVariables.push(vColor);
    const code = ['baseColor = baseColor * vec4(v_color.rgb,1);'];
    mainStartContents = [...mainStartContents, ...code];
  }

  if (programMaterialNeed.getBaseColorTexture) {
    const uniform = declareUniform(Type.sampler2D, 'u_baseColorTexture');
    uniformVariables.push(uniform);
    const varyingTextCoord = `v_texCoord${programMaterialNeed.baseColorTextureCoord}`;
    const code = [
      `vec4 baseColor = texture2D(u_baseColorTexture,${varyingTextCoord}) * u_baseColorFactor;`,
    ];
    mainStartContents = [...code, ...mainStartContents];
  } else {
    const code = [
      'vec4 baseColor = u_baseColorFactor;',
    ];
    mainStartContents = [...code, ...mainStartContents];
  }


  //TODO: to fix

  // if (programMaterialNeed.doubleSided) {
  //   const code = [
  //     'float faceDirection = gl_FrontFacing ? 1.0 : - 1.0;',
  //     'normal = normalize(normal * faceDirection);',
  //   ];

  //   mainStartContents = [...mainStartContents, ...code];
  // }

  if (programGeometryNeed.useTangent) {
    const vTangent = declareVarying(Type.vec3, 'v_tangent');
    varyingVariables.push(vTangent);
    const vBitangent = declareVarying(Type.vec4, 'v_bitangent');
    varyingVariables.push(vBitangent);
    let tangentStartCode = [];
    if (programMaterialNeed.doubleSided) {
      tangentStartCode = [
        'vec3 tangent = v_tangent *faceDirection;',
        'vec4 bitangent = v_bitangent *faceDirection;',
      ];
    } else {
      tangentStartCode = [
        'vec3 tangent = v_tangent;',
        'vec4 bitangent = v_bitangent;',
      ];
    }
    const tbnCode = ['mat3 tbn = mat3(tangent, bitangent, normal);',
      'normal = normalize(tbn * normal);'];
    mainStartContents = [...mainStartContents, ...tangentStartCode, ...tbnCode];
  }

  if (programMaterialNeed.getEmissiveTexture) {
    const uniform = declareUniform(Type.sampler2D, 'u_emissiveTexture');
    uniformVariables.push(uniform);
    const emissiveFactor = declareUniform(Type.vec3, 'u_emissiveFactor');
    uniformVariables.push(emissiveFactor);
    const varyingTextCoord = `v_texCoord${programMaterialNeed.emissiveTextureCoord}`;

    const code = [
      `vec3 emissive = texture2D(u_emissiveTexture, ${varyingTextCoord}).rgb * u_emissiveFactor;`,
      'outColor.rgb += emissive;',
    ];
    mainContents = [...mainContents, ...code];
  } else {
    const emissiveFactor = declareUniform(Type.vec3, 'u_emissiveFactor');
    uniformVariables.push(emissiveFactor);

    const code = [
      'vec3 emissive = u_emissiveFactor;',
      'outColor.rgb += emissive;',
    ];
    mainContents = [...mainContents, ...code];
  }

  if (programMaterialNeed.getNormalTexture) {
    const uniform = declareUniform(Type.sampler2D, 'u_normalTexture');
    uniformVariables.push(uniform);
    const varyingTextCoord = `v_texCoord${programMaterialNeed.normalTextureCoord}`;

    const code = [`normal = texture2D(u_normalTexture, ${varyingTextCoord}).rgb * normal;`];
    mainStartContents = [...mainStartContents, ...code];
  }

  if (programMaterialNeed.getMetallicRoughnessTexture) {
    const uniform = declareUniform(Type.sampler2D, 'u_metallicRoughnessTexture');
    uniformVariables.push(uniform);
    const varyingTextCoord = `v_texCoord${programMaterialNeed.metallicRoughnessTextureCoord}`;

    const code = [
      `vec4 rougnessMetallicSample = texture2D(u_metallicRoughnessTexture, ${varyingTextCoord});`,
      'perceptualRoughness = rougnessMetallicSample.g * perceptualRoughness;',
      'metallic = rougnessMetallicSample.b * metallic;',
    ];
    mainStartContents = [...mainStartContents, ...code];
  }
  if (programMaterialNeed.getOcclusionTexture) {
    const uniform = declareUniform(Type.sampler2D, 'u_occlusionTexture');
    uniformVariables.push(uniform);
  }

  if (programGeometryNeed.envMap) {
    const code = [
      'vec3 indirectDiffuse=vec3(0.0);', // contribution from IBL light probe and Ambient Light
      'vec3 indirectSpecular =vec3(0.0);', // contribution from IBL light probe and Area Light
    ];
  }
  return generateShader(constVariables, uniformVariables, attributeVariables, varyingVariables, helperFunctions, mainStartContents, mainContents, mainEndContents);
};

export { generateVertexShader, generateFragmentShader };
