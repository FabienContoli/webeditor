const defaultShaderType = [
    'VERTEX_SHADER',
    'FRAGMENT_SHADER',
  ];

  /**
   * Wrapped logging function.
   * @param {string} msg The message to log.
   */
function error(msg) {
    if (window.console) {
      if (window.console.error) {
        window.console.error(msg);
      } else if (window.console.log) {
        window.console.log(msg);
      }
    }
  }
/**
 * Creates a program, attaches shaders, binds attrib locations, links the
 * program and calls useProgram.
 * @param {WebGLShader[]} shaders The shaders to attach
 * @param {string[]} [opt_attributes] An array of attributes names. Locations will be assigned by index if not passed in
 * @param {number[]} [opt_locations] The locations for the. A parallel array to opt_attributes letting you assign locations.
 * @param {module:webgl-utils.ErrorCallback} opt_errorCallback callback for errors. By default it just prints an error to the console
 *        on error. If you want something else pass an callback. It's passed an error message.
 * @memberOf module:webgl-utils
 */
function createProgram(
gl, shaders, opt_attributes, opt_locations, opt_errorCallback,
) {
const errFn = opt_errorCallback || error;
const program = gl.createProgram();
shaders.forEach((shader) => {
    gl.attachShader(program, shader);
});
if (opt_attributes) {
    opt_attributes.forEach((attrib, ndx) => {
    gl.bindAttribLocation(
        program,
        opt_locations ? opt_locations[ndx] : ndx,
        attrib,
    );
    });
}
gl.linkProgram(program);

// Check the link status
const linked = gl.getProgramParameter(program, gl.LINK_STATUS);
if (!linked) {
    // something went wrong with the link
    const lastError = gl.getProgramInfoLog(program);
    errFn(`Error in program linking:${lastError}`);

    gl.deleteProgram(program);
    return null;
}
return program;
}

  
/**
   * Loads a shader.
   * @param {WebGLRenderingContext} gl The WebGLRenderingContext to use.
   * @param {string} shaderSource The shader source.
   * @param {number} shaderType The type of shader.
   * @param {module:webgl-utils.ErrorCallback} opt_errorCallback callback for errors.
   * @return {WebGLShader} The created shader.
   */
 function loadShader(gl, shaderSource, shaderType, opt_errorCallback) {
    const errFn = opt_errorCallback || error;
    // Create the shader object
    const shader = gl.createShader(shaderType);
  
    // Load the shader source
    gl.shaderSource(shader, shaderSource);
  
    // Compile the shader
    gl.compileShader(shader);
  
    // Check the compile status
    const compiled = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    if (!compiled) {
      // Something went wrong during compilation; get the error
      const lastError = gl.getShaderInfoLog(shader);
      errFn(`*** Error compiling shader '${shader}':${lastError}\n${shaderSource.split('\n').map((l, i) => `${i + 1}: ${l}`).join('\n')}`);
      gl.deleteShader(shader);
      return null;
    }
  
    return shader;
  }

/**
   * Creates a program from 2 sources.
   *
   * @param {WebGLRenderingContext} gl The WebGLRenderingContext
   *        to use.
   * @param {string[]} shaderSources Array of sources for the
   *        shaders. The first is assumed to be the vertex shader,
   *        the second the fragment shader.
   * @param {string[]} [opt_attributes] An array of attributes names. Locations will be assigned by index if not passed in
   * @param {number[]} [opt_locations] The locations for the. A parallel array to opt_attributes letting you assign locations.
   * @param {module:webgl-utils.ErrorCallback} opt_errorCallback callback for errors. By default it just prints an error to the console
   *        on error. If you want something else pass an callback. It's passed an error message.
   * @return {WebGLProgram} The created program.
   * @memberOf module:webgl-utils
   */
 function createProgramFromSources(
    gl, shaderSources, opt_attributes, opt_locations, opt_errorCallback,
  ) {
    const shaders = [];
    for (let ii = 0; ii < shaderSources.length; ++ii) {
      shaders.push(loadShader(
        gl, shaderSources[ii], gl[defaultShaderType[ii]], opt_errorCallback,
      ));
    }
    return createProgram(gl, shaders, opt_attributes, opt_locations, opt_errorCallback);
  }

  
/**
   * Creates a ProgramInfo from 2 sources.
   *
   * A ProgramInfo contains
   *
   *     programInfo = {
   *        program: WebGLProgram,
   *        uniformSetters: object of setters as returned from createUniformSetters,
   *        attribSetters: object of setters as returned from createAttribSetters,
   *     }
   *
   * @param {WebGLRenderingContext} gl The WebGLRenderingContext
   *        to use.
   * @param {string[]} shaderSourcess Array of sources for the
   *        shaders or ids. The first is assumed to be the vertex shader,
   *        the second the fragment shader.
   * @param {string[]} [opt_attributes] An array of attributes names. Locations will be assigned by index if not passed in
   * @param {number[]} [opt_locations] The locations for the. A parallel array to opt_attributes letting you assign locations.
   * @param {module:webgl-utils.ErrorCallback} opt_errorCallback callback for errors. By default it just prints an error to the console
   *        on error. If you want something else pass an callback. It's passed an error message.
   * @return {module:webgl-utils.ProgramInfo} The created program.
   * @memberOf module:webgl-utils
   */
 function createProgramInfo(
    gl, shaderSources, opt_attributes, opt_locations, opt_errorCallback,
  ) {
    shaderSources = shaderSources.map((source) => {
      const script = document.getElementById(source);
      return script ? script.text : source;
    });
    const program = createProgramFromSources(gl, shaderSources, opt_attributes, opt_locations, opt_errorCallback);
    if (!program) {
      return null;
    }
    const uniformSetters = createUniformSetters(gl, program);
    const attribSetters = createAttributeSetters(gl, program);
    return {
      program,
      uniformSetters,
      attribSetters,
    };
  }

  
  
/**
   * Returns the corresponding bind point for a given sampler type
   */
function getBindPointForSamplerType(gl, type) {
    if (type === gl.SAMPLER_2D)   return gl.TEXTURE_2D;        // eslint-disable-line
    if (type === gl.SAMPLER_CUBE) return gl.TEXTURE_CUBE_MAP;  // eslint-disable-line
  return undefined;
}

/**
   * @typedef {Object.<string, function>} Setters
   */

/**
   * Creates setter functions for all uniforms of a shader
   * program.
   *
   * @see {@link module:webgl-utils.setUniforms}
   *
   * @param {WebGLProgram} program the program to create setters for.
   * @returns {Object.<string, function>} an object with a setter by name for each uniform
   * @memberOf module:webgl-utils
   */
function createUniformSetters(gl, program) {
    let textureUnit = 0;
  
    /**
       * Creates a setter for a uniform of the given program with it's
       * location embedded in the setter.
       * @param {WebGLProgram} program
       * @param {WebGLUniformInfo} uniformInfo
       * @returns {function} the created setter.
       */
    function createUniformSetter(program, uniformInfo) {
      const location = gl.getUniformLocation(program, uniformInfo.name);
      const { type } = uniformInfo;
      // Check if this uniform is an array
      const isArray = (uniformInfo.size > 1 && uniformInfo.name.substr(-3) === '[0]');
      if (type === gl.FLOAT && isArray) {
        return function (v) {
          gl.uniform1fv(location, v);
        };
      }
      if (type === gl.FLOAT) {
        return function (v) {
          gl.uniform1f(location, v);
        };
      }
      if (type === gl.FLOAT_VEC2) {
        return function (v) {
          gl.uniform2fv(location, v);
        };
      }
      if (type === gl.FLOAT_VEC3) {
        return function (v) {
          gl.uniform3fv(location, v);
        };
      }
      if (type === gl.FLOAT_VEC4) {
        return function (v) {
          gl.uniform4fv(location, v);
        };
      }
      if (type === gl.INT && isArray) {
        return function (v) {
          gl.uniform1iv(location, v);
        };
      }
      if (type === gl.INT) {
        return function (v) {
          gl.uniform1i(location, v);
        };
      }
      if (type === gl.INT_VEC2) {
        return function (v) {
          gl.uniform2iv(location, v);
        };
      }
      if (type === gl.INT_VEC3) {
        return function (v) {
          gl.uniform3iv(location, v);
        };
      }
      if (type === gl.INT_VEC4) {
        return function (v) {
          gl.uniform4iv(location, v);
        };
      }
      if (type === gl.BOOL) {
        return function (v) {
          gl.uniform1iv(location, v);
        };
      }
      if (type === gl.BOOL_VEC2) {
        return function (v) {
          gl.uniform2iv(location, v);
        };
      }
      if (type === gl.BOOL_VEC3) {
        return function (v) {
          gl.uniform3iv(location, v);
        };
      }
      if (type === gl.BOOL_VEC4) {
        return function (v) {
          gl.uniform4iv(location, v);
        };
      }
      if (type === gl.FLOAT_MAT2) {
        return function (v) {
          gl.uniformMatrix2fv(location, false, v);
        };
      }
      if (type === gl.FLOAT_MAT3) {
        return function (v) {
          gl.uniformMatrix3fv(location, false, v);
        };
      }
      if (type === gl.FLOAT_MAT4) {
        return function (v) {
          gl.uniformMatrix4fv(location, false, v);
        };
      }
      if ((type === gl.SAMPLER_2D || type === gl.SAMPLER_CUBE) && isArray) {
        const units = [];
        for (let ii = 0; ii < info.size; ++ii) {
          units.push(textureUnit++);
        }
        return (function (bindPoint, units) {
          return function (textures) {
            gl.uniform1iv(location, units);
            textures.forEach((texture, index) => {
              gl.activeTexture(gl.TEXTURE0 + units[index]);
              gl.bindTexture(bindPoint, texture);
            });
          };
        }(getBindPointForSamplerType(gl, type), units));
      }
      if (type === gl.SAMPLER_2D || type === gl.SAMPLER_CUBE) {
        return (function (bindPoint, unit) {
          return function (texture) {
            gl.uniform1i(location, unit);
            gl.activeTexture(gl.TEXTURE0 + unit);
            gl.bindTexture(bindPoint, texture);
          };
        }(getBindPointForSamplerType(gl, type), textureUnit++));
      }
      throw (`unknown type: 0x${type.toString(16)}`); // we should never get here.
    }
  
    const uniformSetters = { };
    const numUniforms = gl.getProgramParameter(program, gl.ACTIVE_UNIFORMS);
  
    for (let ii = 0; ii < numUniforms; ++ii) {
      const uniformInfo = gl.getActiveUniform(program, ii);
      if (!uniformInfo) {
        break;
      }
      let { name } = uniformInfo;
      // remove the array suffix.
      if (name.substr(-3) === '[0]') {
        name = name.substr(0, name.length - 3);
      }
      const setter = createUniformSetter(program, uniformInfo);
      uniformSetters[name] = setter;
    }
    return uniformSetters;
  }

/**
   * Creates setter functions for all attributes of a shader
   * program. You can pass this to {@link module:webgl-utils.setBuffersAndAttributes} to set all your buffers and attributes.
   *
   * @see {@link module:webgl-utils.setAttributes} for example
   * @param {WebGLProgram} program the program to create setters for.
   * @return {Object.<string, function>} an object with a setter for each attribute by name.
   * @memberOf module:webgl-utils
   */
 function createAttributeSetters(gl, program) {
    const attribSetters = {
    };
  
    function createAttribSetter(index) {
      return function (b) {
          gl.bindBuffer(gl.ARRAY_BUFFER, b.buffer);
          gl.enableVertexAttribArray(index);
          gl.vertexAttribPointer(
            index, b.itemSize, b.componentType, b.normalized,b.stride || 0, b.offset || 0,
          );
      };
    }
  
    const numAttribs = gl.getProgramParameter(program, gl.ACTIVE_ATTRIBUTES);
    for (let ii = 0; ii < numAttribs; ++ii) {
      const attribInfo = gl.getActiveAttrib(program, ii);
      if (!attribInfo) {
        break;
      }
      const index = gl.getAttribLocation(program, attribInfo.name);
      attribSetters[attribInfo.name] = createAttribSetter(index);
    }
  
    return attribSetters;
  }
  
function createBufferFromTypedArray(gl,array,type,drawType){
    type = type || gl.ARRAY_BUFFER;
    const buffer = gl.createBuffer();
    gl.bindBuffer(type, buffer);
    gl.bufferData(type, array, drawType || gl.STATIC_DRAW);
    return buffer;
}

function setBufferAttributes(gl,attributes){
    let attribsBuffer={}
    Object.keys(attributes).forEach((attribName) => {
        let buffer=createBufferFromTypedArray(gl, attributes[attribName].array)
        let attribute =attributes[attribName]
        attribsBuffer[attribName]={itemSize:attribute.itemSize, componentType: attribute.componentType,normalized:attribute.normalized,buffer} 
    })

    return attribsBuffer
}

function createBuffersFromPrimitive(gl, primitive) {
  const buffers = {
    attributes: setBufferAttributes(gl, primitive.attributes),
  };
  let { indices } = primitive;
  if (indices) {
        let buffer=createBufferFromTypedArray(gl, indices.array, gl.ELEMENT_ARRAY_BUFFER);
        buffers.indices=buffer
  }
  return buffers;
}

function setBuffersAndAttributes(gl, program, buffers) {
    setAttributes(program.attribSetters, buffers.attributes);
    if (buffers.indices) {
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);
    }
}

function setAttributes(setters,attributes){
    Object.keys(attributes).forEach((name) => {
        const setter = setters[name];
        if (setter) {
          setter(attributes[name]);
        }
    });
}

 function setUniforms(setters, ...values) {
    for (const uniforms of values) {
      Object.keys(uniforms).forEach((name) => {
        const setter = setters[name];
        if (setter) {
          setter(uniforms[name]);
        }
      });
    }
  }
  
  function drawPrimitive(gl, primitive) {
    const { indices } = primitive;
    if (indices) {
        // low__farm :invalid enum 4 36 5125
        gl.drawElements(primitive.mode, indices.count, indices.componentType,0);
    } else {
        gl.drawArrays(primitive.mode, 0, primitive.count);
    }
}

export {
    setBuffersAndAttributes,
    createBuffersFromPrimitive,
    createProgramInfo,
    setUniforms,
    drawPrimitive
}