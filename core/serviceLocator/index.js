import AudioApi from "../audio/AudioApi";
import Controllers from "../controllers/Controllers";
import Loop from "../lifecyles/Loop";
import Physics from "../physics/Physics";
import Renderer from "../renderer/Renderer";
import { generateUUID } from "../helpers/uuidv4";
import Logger from "../logger/Logger";

export default class ServiceLocator {

    /**
     * ServiceLocator uuid
     * @type {string}
     * @public
     * @readonly
     */
    uuid;

    /**
     * audioapi Service
     * @type {AudioApi}
     * @private
     */
    audioService;

    /**
     * controllers service
     * @type {Controllers}
     * @private
     */
     controllers;

    /**
     * loop service
     * @type {Loop}
     * @private
     */
    loop;

    /**
     * renderer
     * @type {Renderer}
     * @private
     */
    renderer;

    /**
     * physics
     * @type {Physics}
     * @private
     */
    physics;
    
    /**
     * loader
     * @type {Loader}
     * @private
     */
    loader;

    /**
     * logger
     * @type {Logger}
     * @private
     */
    logger;

    /**
     * engine
     * @type {Logger}
     * @private
     */
    engine
    
    constructor() {
        this.uuid = generateUUID();
    }
    

    /**
     * @function
     * @param audioService {AudioApi}
     */
    setAudioService(audioService){
        this.audioService=audioService
    }
    
    /**
     * @returns {AudioApi}
     */
    getAudioService(){
        return this.audioService
    }

    
    /**
     * @function}
     */
     setEngine(engine){
        this.engine=engine
    }
    
    /**
     * @returns {any}
     */
    getEngine(){
        return this.engine
    }

    
    /**
     * @function
     * @param controllers {Controllers}
     */
    setControllers(controllers){
        this.controllers=controllers
    }

    /**
     * @returns {Controllers}
     */
    getControllers(){
        return this.controllers;
    }

    /**
     * @function
     * @param renderer {Renderer}
     */
    setRenderer(renderer){
        this.renderer=renderer
    }

    /**
     * @returns {Renderer}
     */
    getRenderer(){
        return this.renderer;
    }


    /**
     * @function
     * @param physics {Physics}
     */
    setPhysics(physics){
        this.physics=physics
    }

    /**
     * @returns {Physics}
     */
    getPhysic(){
        return this.physics;
    }
    
    
    /**
     * @function
     * @param loop {Loop}
     */
    setLoop(loop){
        this.loop=loop
    }

    /**
     * @returns {Loop}
     */
    getLoop(){
        return this.loop;
    }

    
    /**
     * @function
     * @param loader {Loader}
     */
    setLoader(loader){
        this.loader=loader
    }

    /**
     * @returns {Loader}
     */
    getLoader(){
        return this.loader;
    }

    /**
     * @function
     * @param logger {Logger}
     */
    setLogger(logger){
        this.logger=logger
    }

    /**
     * @returns {Logger}
     */
    getLogger(){
        return this.logger;
    }

    destroy(){
        if(this.renderer)this.renderer.destroy()
        if(this.loop)this.loop.destroy()
        if(this.physics)this.physics.destroy()
        if(this.controllers)this.controllers.destroy()
        if(this.audioService)this.audioService.destroy()
        if(this.loader)this.loader.destroy()
        if(this.logger)this.logger.destroy()
    }
}