export default class AudioApi {
  constructor(){
    this.audioCtx= new (window.AudioContext || (window.webkitAudioContext))();
  }

  playAudio = function playAudio(audio, loop, volume) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', audio.src);
    xhr.responseType = 'arraybuffer';
    const source = this.audioCtx.createBufferSource();
    xhr.addEventListener('load', () => {
      const playsound = (audioBuffer) => {
        source.buffer = audioBuffer;
        source.loop = loop;
        source.connect(this.audioCtx.destination);
        source.start();
      };
      this.audioCtx.decodeAudioData(xhr.response).then(playsound);
    });
    xhr.send();
    return source;
  };

  destroy = function destroyAudioContext() {
    if (this.audioCtx) {
      this.audioCtx.close();
    }
  };

  update(soundCommands){
    for(let i = 0; i < soundCommands.length; i++) {
      let soundCommand = soundCommands.pop()
      this.playAudio(soundCommand,soundCommand.loop,soundCommand.volume)
    }
  }
}