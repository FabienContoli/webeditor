const getExtension= function(uri){
    return uri.match(/[^\.]+$/)[0].toLowerCase();
}

const getPathFolder= function(uri){
    return uri.substring(0, uri.lastIndexOf('/'))
}

const getFolders=function(uri){
    return getPathFolder(uri).split('/').filter((folderName)=>{
        return folderName!==''
    })
}

const isTextFile=function(extension){
    return extension.match(/(gltf|json|js)$/)
}

export { getExtension,getPathFolder,isTextFile,getFolders }