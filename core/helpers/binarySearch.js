function binarySearch(array, number) {
    let start = 0;
    let end = array.length - 1;
    let index = 0;
    while (start <= end) {
      let index = (end + start) >> 1;
      if (number > array[index] && start !== index) {
        start = index;
      } else if (number < array[index] && end !== index) {
        end = index;
      } else {
        return [index, index + 1];
      }
    }
    return [index, index + 1];
  }
  
  export{binarySearch}
  