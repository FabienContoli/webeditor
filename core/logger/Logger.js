/**
 *   Logger Class
 *  @class
 */
export default class Logger {
    
    /**
     * boolean to deactivate log 
     * @type {boolean}
     * @private
    */
    silent;
    
     /**
     * Logger function 
     * @type {function}
     * @param {string} functionName use as console[functionName]()
     * @param {object} logElement element to log
     * @private
    */
    secureLog;
    
    /**
     * @constructor
     * @param {boolean} silent
     * 
     */
    constructor(silent=false){
        if(silent){
            this.secureLog=this.secureLogSilent
        } else{
            this.secureLog=this.secureLogActivate
        }
    }
    
    secureLogActivate(functionName, logElement) {
        try {
        if (!this.silent) {
            console[functionName](logElement);
        }
        } catch (err) {
        console.error(err);
        }
    }

    secureLogSilent(){

    }

    /**
     * Logger trace function 
     * @function 
     * @param {object|string} logElement element to log
     * @public
    */
    trace(logElement) {
        this.secureLog('trace', logElement);
    }

    
    /**
     * Logger time function 
     * @function 
     * @param {object|string} logElement element to log
     * @public
    */
    time(logElement) {
        this.secureLog('time', logElement);
    }

    /**
     * Logger timeEnd function 
     * @function 
     * @param {object|string} logElement element to log
     * @public
    */
    timeEnd(logElement) {
        this.secureLog('timeEnd', logElement);
    }

    /**
     * Logger debug function 
     * @function 
     * @param {object|string} logElement element to log
     * @public
    */
    debug(logElement) {
        this.secureLog('debug', logElement);
    }

    /**
     * Logger log function 
     * @function 
     * @param {object|string} logElement element to log
     * @public
    */
    log(logElement) {
        this.secureLog('log', logElement);
    }

    /**
     * Logger info function 
     * @function 
     * @param {object|string} logElement element to log
     * @public
    */
    info(logElement) {
        this.secureLog('info', logElement);
    }

    /**
     * Logger warn function 
     * @function 
     * @param {object|string} logElement element to log
     * @public
    */
    warn(logElement) {
        this.secureLog('warn', logElement);
    }

    /**
     * Logger error(logElement) {
     *  
     * @function 
     * @param {object|string} logElement element to log
     * @public
    */
    error(logElement) {
        this.secureLog('error', logElement);
    }

    destroy(){
        
    }
}
  