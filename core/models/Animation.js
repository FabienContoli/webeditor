import { generateUUID } from "../helpers/uuidv4";

export default class Animation {

    /**
     * Material uuid
     * @type {string}
     * @public
     * @readonly
     */
    uuid;

    /**
     * Material name
     * @type {string}
     * @public
     */
    name;

    /**
     * An array of animation channels. An animation channel combines an animation sampler with a target property being animated. Different channels of the same animation **MUST NOT** have the same targets.
     * @type {Array<Channel>}
     * @public
     */
     channels;
    
    /**
     * 	
     * An array of animation samplers. An animation sampler combines timestamps with a sequence of output values and defines an interpolation algorithm.
     * @type {Array<AnimationSampler>}
     * @public
     */
     samplers;
    /**
     * 	
     * The max time from all samplers
     * @type {Number}
     * @public
     */
     maxTime

    /**
     * 
     * @param {Object} object 
     * @param {string} object.name 
     * @param {any} object.channels
     * @param {any} object.samplers
     * @param {any} object.maxTime
     */
    constructor({name,channels,samplers,maxTime}) {

        this.uuid = generateUUID();
        this.name = name;
        this.channels = channels;
        this.samplers = samplers;
        this.maxTime = maxTime;
        
    }
}