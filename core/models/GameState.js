export default class GameState {
    
    constructor({state}) {
        this.sceneIndex=0;
        this.inputs=[];
        this.actions={};
        this.soundCommands=[]
        this.state={}
        this.temp={}
        if(state){
            this.state={...state,...this.state}
        }
    }

    save(){

    }

    load(){

    }
}