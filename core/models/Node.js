import { generateUUID } from "./../helpers/uuidv4";
import Mesh from "./mesh/Mesh";
import Skin from "./mesh/Skin";
import Ref from "./Ref"
import * as m4 from "./../maths/m4"
/**
 *  Represent a Node model
 *  @class
 */
export default class Node {
    

    /**
     * name
     * @type {string}
     * @public
     */
    uuid;
    
    /**
     * ref creation index
     * @type {Ref}
     * @public
     */
    ref;
    
    /**
     * name
     * @type {string}
     * @public
     */
    name;
    
    /**
     * child node
     * @type Array<Node>
     * @public
     */
    children;

    /**
     * parent node
     * @type Node
     * @public
     */
    parent;
    
    /**
     * mesh
     * @type Mesh
     * @public
     */
     mesh;

    /**
    * node local matrix
    * @type Float32Array
    * @public
    */
    matrix;


    /**
     * node world relative matrix
     *
     */
    matrixWorld;
    
    /**
    * matrix Type
    * @type boolean
    * @public 
    */
    isTRS

    /**
    * rotation Matrix
    * @type [number, number, number, number]
    * @public 
    */
    rotation;

    /**
    * translation Matrix
    * @type [number, number, number]
    * @public 
    */
    translation;

    /**
    * scale Matrix
    * @type [number, number, number]
    * @public 
    */
    scale;

    /**
    * scale Matrix
    * @type [number, number, number]
    * @public 
    */
    weights;

    /**
    * skin
    * @type Skin
    * @public 
    */
    skin;

      /**
    * skin
    * @type boolean
    * @public 
    */
    isJoint;
    
      /**
    * skin
    * @type number
    * @public 
    */
    indexJoint;  
    
    /**
    * skin
    * @type boolean
    * @public 
    */
    isSkeleton;

    /**
    * extensions list
    * @type Array<string>
    * @public 
    */
    extensions

    /**
     * @required
     * @param {object} [object]
     * @param {string} [object.name]
     * @param {Array<Node>} [object.children]
     * @param {Node} [object.parent]
     * @param {Array} [object.matrix]
     * @param {object} [object.rotation]
     * @param {object} [object.scale]
     * @param {object} [object.translation]
     * TODO: add element to constructor and use it in the editor 
     */
    constructor({name,children,parent,matrix,rotation,scale,translation}={},ref) {
        this.uuid=generateUUID()

        this.name=name;

        if(ref !== undefined){
            this.ref=ref
        }

        this.parent=parent;

        this.children=children??[]
        

        if (scale || rotation || translation) {
            this.isTRS=true
            if (!scale) {
              this.scale = [1, 1, 1];
            }else{
              this.scale=scale
            }

            if (!rotation) {
              this.rotation = [0, 0, 0, 1];
            }else{
              this.rotation=rotation
            }
    
            if (!translation) {
              this.translation = [0, 0, 0];
            }else {
              this.translation=translation
            }
    
            this.updateMatrixFromTRS()
          } else {
            if (matrix) {
                this.matrix= m4.copy(matrix);
            }else{
                this.matrix = m4.identity();
            }
            this.scale = [1, 1, 1];
            this.rotation = [0, 0, 0, 1];
            this.translation = [0, 0, 0];
            this.updateTRSFromMatrix()
          }
          this.matrixWorld = m4.copy(this.matrix);
    }

    /**
     * @required
     * @param {Node} node
     */
    addChildNode(node){
        this.children.push(node)
    }

     /**
     * @required
     * @param {Mesh} mesh
     */
    addMeshToNode(mesh){
        this.mesh=mesh
    }

    removeChild(node){
        let deleteIndex= this.children.indexOf(node)
        this.children.splice(deleteIndex,1)
    }

    updateMatrixFromTRS(){
      this.matrix = m4.compose(this.translation, this.rotation, this.scale);
    }

    updateTRSFromMatrix(){
      m4.decompose(this.matrix, this.translation, this.rotation, this.scale);
    }

    update(){
      if(!this.skin){
        if (this.parent) {
          m4.multiply(this.parent.matrixWorld, this.matrix, this.matrixWorld);
        } else {
          m4.copy(this.matrix, this.matrixWorld);
        }
      }
      this.updateChildren((childNode)=>childNode.update())
    }

    copyPos(nodeToCopy){
      this.matrix=m4.copy(nodeToCopy.matrix)
      this.rotation=JSON.parse(JSON.stringify(nodeToCopy.rotation))
      this.translation=JSON.parse(JSON.stringify(nodeToCopy.translation))
      this.scale=JSON.parse(JSON.stringify(nodeToCopy.scale))
    }
    
    updateChildren(handler){
      if (this.children) {
        this.children.forEach((childNode) => {
          handler(childNode)
        });
      }
    }
    
    // updateChildren(){
    //   if (this.children) {
    //     this.children.forEach((childNode) => {
    //       if(!childNode ||!childNode.update) debugger
    //       childNode.update()
    //     });
    //   }
    // }

}
