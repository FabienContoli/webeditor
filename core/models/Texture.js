import { generateUUID } from "../helpers/uuidv4";
import { TEXTURE_ENUM } from "../const/GltfConst";

/** 
 *  Class representing a texture. 
 *  @class
 * 
 */
export default class Texture {
    
    /**
     * Material texture magfilter
     * @type {TEXTURE_ENUM_TYPE}
     * @public
     */
    magFilter;

    /**
     * @constructor
     * @param {Blob} image - The image of the texture.
     * @param {Object} sampler - sampler object
     * @param {TEXTURE_ENUM_TYPE} sampler.magFilter
     * @param {TEXTURE_ENUM_TYPE} sampler.minFilter
     * @param {TEXTURE_ENUM_TYPE} sampler.wrapS
     * @param {TEXTURE_ENUM_TYPE} sampler.wrapT
     */
    constructor(image,sampler) {
        this.uuid= generateUUID();
        this.image = image
        this.magFilter=sampler.magFilter||TEXTURE_ENUM.LINEAR;
        this.minFilter=sampler.minFilter||TEXTURE_ENUM.LINEAR_MIPMAP_LINEAR;
        this.wrapS=sampler.wrapS||TEXTURE_ENUM.REPEAT;
        this.wrapT=sampler.wrapT||TEXTURE_ENUM.REPEAT;
    }
}