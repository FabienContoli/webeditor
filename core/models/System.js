import { generateUUID } from "./../helpers/uuidv4";
import Ref from "./Ref";
/**
 *  Represent a System blueprint
 *  @class
 */
export default class System {
    
    /**
     * name
     * @type {number}
     * @public
     */
     name;

    /**
     * uuid
     * @type {string}
     * @public
     */
    uuid;
    
    
     /**
      * script
      * @type {function}
      * @public
      */
      script;
     
     /**
      * @required
      * @param {object} [object]
      * @param {number} [object.name]
      * @param {function} [object.script]
      * TODO: add element to constructor and use it in the editor 
      */
     constructor({name,script}) {
         
        this.uuid=generateUUID()
         if(name)this.name=name
         if(script)this.script=script
     }    
}