export default class Controls {

    /**
     * 
     * @type {boolean} 
     */
     keyboard;

     /**
     * 
     * @type {boolean} 
     */
    mouse;

    /**
     * 
     * @type {boolean} 
     */
    touch;

    /**
     * 
     * @type {boolean} 
    */
    gamepad;

    constructor({keyboard=false,mouse=false,touch=false,gamepad=false}) {
        this.keyboard = keyboard;
        this.mouse = mouse;
        this.touch = touch
        this.gamepad = gamepad;
    }
}