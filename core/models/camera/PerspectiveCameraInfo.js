import {perspective} from "../../maths/m4";

/**
 *  Represent an OrthographicCamera blueprint
 *  @class
 */
export default class PerspectiveCameraInfo {

    /**
   * The floating-point aspect ratio of the field of view.
   * @type {number}
   */
    aspectRatio

    /**
    * The floating-point vertical field of view in radians. This value **SHOULD** be less than π.
    * @type {number}
    */
     yfov

    /**
     * The floating-point distance to the far clipping plane. When defined, `zfar` **MUST** be greater than `znear`. If `zfar` is undefined, client implementations **SHOULD** use infinite projection matrix.
     * @type {number}
     */
    zfar

    /**
    * The floating-point distance to the near clipping plane.
    * @type {number}
    */
    znear

    constructor({aspectRatio,yfov,zfar,znear}={}){
        this.aspectRatio=aspectRatio??1.0;
        this.yfov=yfov??1.0;
        this.zfar=zfar??1000;
        this.znear=znear??0.001;
     }

     
     getProjectionMatrix(gl){
         let aspectRatioRenderer=1.0
      
         if(gl && gl.canvas){
            aspectRatioRenderer= gl.canvas.clientWidth /gl.canvas.clientHeight
         }
         return perspective(this.yfov, this.aspectRatio*aspectRatioRenderer, this.znear, this.zfar);
     }
}