/**
 *  Represent an OrthographicCamera blueprint
 *  @class
 */
export default class OrthographicCameraInfo {

   /**
  * The floating-point horizontal magnification of the view. This value **MUST NOT** be equal to zero. This value **SHOULD NOT** be negative.
  * @type {number}
  * @required
  */
   xmag

   /**
   * The floating-point vertical magnification of the view. This value **MUST NOT** be equal to zero. This value **SHOULD NOT** be negative.
   * @type {number}
   * @required
   */
   ymag

   /**
    * The floating-point distance to the far clipping plane. This value **MUST NOT** be equal to zero. `zfar` **MUST** be greater than `znear`.
    * @type {number}
    * @required
    */
   zfar

   /**
   * The floating-point distance to the near clipping plane.
   * @type {number}
   * @required
   */
   znear

   
   constructor({xmag,ymag,zfar,znear}={}){
      this.xmag=xmag??1.0;
      this.ymag=ymag??1.0;
      this.zfar=zfar??100;
      this.znear=znear??0.01;
   }

   getProjectionMatrix(){
      console.error("not implemented yet")
   }
}