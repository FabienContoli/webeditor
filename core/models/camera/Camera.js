import OrthographicCameraInfo from "./OrthographicCameraInfo";
import PerspectiveCameraInfo from "./PerspectiveCameraInfo";
import {inverse,multiply,lookAt, copy,identity} from "../../maths/m4";
import Node from "../Node";
import { generateUUID } from "../../helpers/uuidv4";
export default class Camera {


    /**
     * name
     * @type {string}
     * @public
     */
        uuid;

    /**
     * Camera name
     * @type {string}
     */
     name;

     /**
      * Camera info 
      * @type {OrthographicCameraInfo|PerspectiveCameraInfo}
      */
     info;
     
     /**
     * Specifies if the camera uses a perspective or orthographic projection.
     * @type {string}
     */
     type;

     /**
     * projection Matrix
     * @type {string}
     */
     projectionMatrix

    /**
     * node
     * @type {Node}
     */
    node

    /**
     * node
     * @type {Node}
     */
    matrixWorld

    constructor(blueprint={}) {
        
        this.uuid=generateUUID()
        this.type = blueprint.type;
            
        if( this.type === "perspective"){
            this.info= new PerspectiveCameraInfo(blueprint);
        }else if( this.type === "orthographic"){
            this.info=new OrthographicCameraInfo(blueprint);
        }else {
            throw new Error(`type :${type} does not exist`)
        }   
    }

    update(gl){
        this.projectionMatrix=this.info.getProjectionMatrix(gl)
        this.worldPosition=[this.node.matrixWorld[12],
        this.node.matrixWorld[13],
        this.node.matrixWorld[14]]
        this.matrixWorld =this.node.matrixWorld
        // this.matrixWorld = lookAt(this.worldPosition, [0, 0, 0], [0, 1, 0]);
        this.viewMatrix=inverse(this.matrixWorld)
        this.viewProjectionMatrix=multiply(this.projectionMatrix, this.viewMatrix)
    }
}   