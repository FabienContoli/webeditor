/**
 *  Represent a Ref model
 *  @class
 */
export default class Ref {
    

    /**
     * name
     * @type {number}
     * @public
     */
    index;
    
    /**
     * ref creation index
     * @type {string}
     * @public
     */
     filePath;
    
    /**
     * @required
     * @param {object} [object]
     * @param {number} [object.index]
     * @param {string} [object.filePath]
     * TODO: add element to constructor and use it in the editor 
     */
    constructor({index,filePath}={}) {
        
        this.index=index;
        if(filePath)this.filePath=filePath
        
    }
}
