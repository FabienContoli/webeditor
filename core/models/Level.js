import Camera from "./camera/Camera";
import Scene from "./Scene";
import Light from "./Light";
import Mesh from "./mesh/Mesh";
import Node from "./Node";
import SystemsManager from "./../manager/SystemsManager";
import Skin from "./Mesh/Skin";
import Material from "./mesh/material/Material";

export default class Level {
    
     /**
     * Level name
     * @type {string}
     * @public
     */
    name;

        
     /**
     * Level animations
     * @type {Map<String,Animation>}
     * @public
     */
      animations;

    /**
     * Scene loaded Object
     * @type {Scene}
     * @public
     */
    scene;

    /**
     * A Dico of Node
     * @type {Map<String,Node>}
     * @public
     */
    nodes;

    /**
     * An array of Light
     * @type {Map<String,Light>}
     * @public
     */
    lights;

    /**
     * An camera index
     * @type {number}
     * @public
     */
    camera;

    /**
     * An array of Camera
     * @type {Map<String,Camera>}
     * @public
     */
    cameras;

    /**
     * An array of Mesh
     * @type {Map<String,Mesh>}
     * @public
     */
    meshes;

    /**
     * An array of skin
     * @type {Map<String,Skin>}
     * @public
     */
    skins;

    /**
     * An array of Material
     * @type {Map<String,Material>}
     * @public
     */
    materials;

    /**
     * An array of Commands
     * @type {Map<String,?>}
     * @public
     */
    commands;
     
    /**
     * An array of Commands
     * @type {SystemsManager}
     * @public
     */
    systems;

     /**
     * An array of Commands
     * @type {Map<String,?>}
     * @public
     */
    scripts;
    

    constructor({name}={}) {
        this.name=name;
        this.scene=new Scene()
        this.camera=0;
        this.commands=[];
        this.cameras=new Map()
        this.lights=new Map()
        this.meshes=new Map()
        this.nodes=new Map()
        this.commands=new Map()
        this.scripts=new Map()
        this.animations=new Map()
        this.skins=new Map()
        this.systems=new SystemsManager()
        
    }

    createChildNode(uuidNodeParent){
        let nodeParent = this.nodes.get(uuidNodeParent)
        let node = new Node({})
        this.nodes.set(node.uuid,node)
        nodeParent.addChildNode(node)
    }

    addMesh(mesh){
        this.meshes.set(mesh.uuid,mesh)
    }

    deleteNode(node){
        this.nodes.delete(node.uuid)
    }

    findNode(nodes,handler){
        let find=false
        for (let index = 0; index < nodes.length; index++) {
            const node = nodes[index];
            if(handler(node)){
                find=node
                break;                
            }

            if(node.children){
                find= this.findNode(node.children,handler)
                if(find){
                    break; 
                } 
            }
        }
        
        return find
    }
    

    findAllNodes(nodes,handler,find=[]){
        for (let index = 0; index < nodes.length; index++) {
            const node = nodes[index];
            if(handler(node)){
                find.push(node)             
            }

            if(node.children){
                this.findAllNodes(node.children,handler,find) 
            }
        }
        return find
    }
}
