import { generateUUID } from "../helpers/uuidv4";

export default class SceneGltf {

    constructor(name,children) {
        this.name=name;
        this.children=[];
        this.matrix= m4.identity();
        this.matrixWorld= m4.identity(); 
        this.updateMatrix= true;
        this.uuid= generateUUID(); 

        if(children && Array.isArray(children)){
            for (let index = 0; index < children.length; index++) {
                const childNode = children[index];
                this.children.push(childNode);
                childNode.parent = scene;
            }
        }
    }
}