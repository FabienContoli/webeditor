import Material from "./material/Material";

import {generateUUID} from "./../../helpers/uuidv4"
/**
 *  Represent a Primitive blueprint
 *  @class
 */
 export default class Primitive {
    

    /**
     * Primitive uuid
     * @type {string}
     * @public
     * @readonly
     */
    uuid;

    /**
     * name
     * @type {string}
     * @public
     */
    name;

    /**
     * render mode number
     * @type number
     * @public
     */
    mode;
    
    /**
     * index of indices
     * @type number
     * @public
     */
    indices;

    /**
     * number of elements to draw
     * @type number
     * @public
     */
    count;

    /**
     * attributes index
     * @type Object
     * @public
     */
     attributes;

    /**
     * material index
     * @type number
     * @public
     */
    material;

    /**
     * targets index
     * @type Array<Target>
     * @public
     */
    targets;

    /**
     * extensions list
     * @type Array<String>
     * @public
     */
    extensions;

    constructor({name,mode=4,indices,attributes,material,targets,extensions}={}){
        this.uuid= generateUUID();
        if(name){
            this.name=name
        }
        this.mode=mode

        this.attributes=(attributes)?attributes:{} 

        if(indices){
            this.indices=indices
            this.count=indices.count
        }else if(attributes && attributes.position){
            this.count=attributes.position.length
        }
   

        if(material){
            this.material=material
        }

        if(targets){ 
            this.targets=targets
        }

        if(extensions){
            console.log(extensions)
        }
    }
}
