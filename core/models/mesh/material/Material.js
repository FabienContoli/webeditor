import NormalTextureInfo from "./NormalTextureInfo"
import OcclusionTextureInfo from "./OcclusionTextureInfo"
import PbrMetallicRoughness from "./PbrMetallicRoughness";
import TextureInfo from "./TextureInfo"
import {generateUUID} from "./../../../helpers/uuidv4"
/**
 *  Represent a Material blueprint
 *  @class
 */
 export default class Material {

    
   /**
     * Material uuid
     * @type {string}
     * @public
     * @readonly
     */
    uuid;

    /**
    * The material name
    * @type {number}
    */
     name;

    /**
    * A set of parameter values that are used to define the metallic-roughness material model from Physically Based Rendering (PBR) methodology. When undefined, all the default values of `pbrMetallicRoughness` **MUST** apply.
    * @type {PbrMetallicRoughness}
    */
     pbrMetallicRoughness;

    /**
    * The tangent space normal texture.
    * @type {NormalTextureInfo}
    */
     normalTexture;

    /**
    *  The occlusion texture. The occlusion values are linearly sampled from the R channel. Higher values indicate areas that receive full indirect lighting and lower values indicate no indirect lighting. If other channels are present (GBA), they **MUST** be ignored for occlusion calculations. When undefined, the material does not have an occlusion texture.
    * @type {OcclusionTextureInfo}
    */
    occlusionTexture;

    /**
    *  The emissive texture. It controls the color and intensity of the light being emitted by the material. This texture contains RGB components encoded with the sRGB transfer function. If a fourth component (A) is present, it **MUST** be ignored. When undefined, the texture **MUST** be sampled as having `1.0` in RGB components.
    * @type {TextureInfo}
    */
    emissiveTexture;
    

    /**
    * The factors for the emissive color of the material. This value defines linear multipliers for the sampled texels of the emissive texture.
    * @type {Array<number>}
    */
    emissiveFactor;

    /**
    * The material's alpha rendering mode enumeration specifying the interpretation of the alpha value of the base color.
    * @default "OPAQUE"
    * @type {string}
    */
    alphaMode;
    

    /**
    * Specifies the cutoff threshold when in `MASK` alpha mode. If the alpha value is greater than or equal to this value then it is rendered as fully opaque, otherwise, it is rendered as fully transparent. A value greater than `1.0` will render the entire material as fully transparent. This value **MUST** be ignored for other alpha modes. When `alphaMode` is not defined, this value **MUST NOT** be defined.
    * @type {number}
    */
    alphaCutoff;

    /**
    *  Specifies whether the material is double sided. When this value is false, back-face culling is enabled. When this value is true, back-face culling is disabled and double-sided lighting is enabled. The back-face **MUST** have its normals reversed before the lighting equation is evaluated.
    * @type {boolean}
    * @default false
    */
    doubleSided;

    constructor({name,alphaCutoff,alphaMode,doubleSided,emissiveFactor,emissiveTexture,extensions,extras,pbrMetallicRoughness,normalTexture,occlusionTexture}={}){
      this.uuid= generateUUID();
      if(name)this.name=name;
      (typeof alphaCutoff==='number')?this.alphaCutoff=alphaCutoff:0.5
      if(alphaMode)this.alphaMode=alphaMode
      this.doubleSided=doubleSided
      if(emissiveTexture)this.emissiveTexture=emissiveTexture
      this.emissiveFactor=(emissiveFactor)?emissiveFactor: [0, 0, 0];
      if(extras)this.extras=extras
      if(extensions)this.extensions=extensions;
      if(pbrMetallicRoughness)this.pbrMetallicRoughness=pbrMetallicRoughness
      if(normalTexture)this.normalTexture=this.normalTexture
      if(occlusionTexture)this.occlusionTexture=this.occlusionTexture
    }

 }
