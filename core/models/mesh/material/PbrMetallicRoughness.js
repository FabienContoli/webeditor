import TextureInfo from "./TextureInfo";

/**
 *  Represent a Material PBR Metallic Roughness blueprint
 *  @class
 */
export default class PbrMetallicRoughness {
    
    /**
    * The factors for the base color of the material. This value defines linear multipliers for the sampled texels of the base color texture.
    * @type {[number,number,number,number]}
    */
    baseColorFactor;

    /**
    * The base color texture. The first three components (RGB) **MUST** be encoded with the sRGB transfer function. They specify the base color of the material. If the fourth component (A) is present, it represents the linear alpha coverage of the material. Otherwise, the alpha coverage is equal to `1.0`. The `material.alphaMode` property specifies how alpha is interpreted. The stored texels **MUST NOT** be premultiplied. When undefined, the texture **MUST** be sampled as having `1.0` in all components.
    * @type {TextureInfo}
    */
    baseColorTexture;


    /**
    * The factor for the metalness of the material. This value defines a linear multiplier for the sampled metalness values of the metallic-roughness texture.
    * @type {number}
    */
    metallicFactor;

    /**
    * The factor for the roughness of the material. This value defines a linear multiplier for the sampled roughness values of the metallic-roughness texture.
    * @type {number}
    */
    roughnessFactor;

    /**
    * The metallic-roughness texture. The metalness values are sampled from the B channel. The roughness values are sampled from the G channel. These values **MUST** be encoded with a linear transfer function. If other channels are present (R or A), they **MUST** be ignored for metallic-roughness calculations. When undefined, the texture **MUST** be sampled as having `1.0` in G and B components.
    * @type {TextureInfo}
    */
    metallicRoughnessTexture;


    constructor({baseColorFactor,baseColorTexture,metallicFactor,roughnessFactor,metallicRoughnessTexture}={}){
       
        this.baseColorFactor=(baseColorFactor)?baseColorFactor:[1.0, 1.0, 1.0, 1.0]
        
        this.metallicFactor=typeof metallicFactor==="number"?metallicFactor:1.0;
        this.roughnessFactor=typeof roughnessFactor==="number"?roughnessFactor:1.0;
        
        if(baseColorTexture)this.baseColorTexture=baseColorTexture
        if(metallicRoughnessTexture)this.metallicRoughnessTexture=metallicRoughnessTexture
    }
     
}