

/**
 *  Represent a Texture blueprint
 *  A texture and its sampler.
 *  @class
 */
 export default class Image {

    
    /**
    * The Image name
    * @type {string}
    */
     name;

    /**
    * The URI (or IRI) of the image.  Relative paths are relative to the current glTF asset.  Instead of referencing an external file, this field **MAY** contain a `data:`-URI. This field **MUST NOT** be defined when `bufferView` is defined.
    * @type {string}
    */
     uri;

    /**
    * The image's media type. This field **MUST** be defined when `bufferView` is defined.
    * @type {string}
    */
     mimeType;

    /**
    * The index of the bufferView that contains the image. This field **MUST NOT** be defined when `uri` is defined.
    * @type {number}
    */
     bufferView

 }
