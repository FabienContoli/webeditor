import { generateUUID } from "./../../helpers/uuidv4";
import * as m4 from "./../../maths/m4"

/**
 *  Represent a Skin model
 *  @class
 */
 export default class Skin {
    
    /**
     * The skin name.
     * @type {string}
     * @public
     */
    name;

   /**
     * The index of the accessor containing the floating-point 4x4 inverse-bind matrices. Its `accessor.count` property **MUST** be greater than or equal to the number of elements of the `joints` array. When undefined, each matrix is a 4x4 identity matrix.
     * @type {Array}
     * @public
     */
   inverseBindMatrices;

   /**
     * The index of the node used as a skeleton root. The node **MUST** be the closest common root of the joints hierarchy or a direct or indirect parent node of the closest common root.
     * @type {Node}
     * @public
     */
   skeleton;

   /**
     *  Indices of skeleton nodes, used as joints in this skin.,
     * @type {Array<Node>}
     * @public
     */
   joints;

   
    /**
     * @required
     * @param {object} [object]
     * @param {string} [object.name]
     * @param {Array} [object.inverseBindMatrices]
     * @param {Node} [object.skeleton]
     * @param {Array<Node>} [object.joints]
     * TODO: add element to constructor and use it in the editor 
     */
     constructor({name,inverseBindMatrices,skeleton,joints,jointMatrices}={}) {
        this.uuid=generateUUID()

        this.name=name;

        this.inverseBindMatrices=inverseBindMatrices

        this.skeleton=skeleton

        this.joints=joints

        this.jointMatrices=jointMatrices?jointMatrices:[]

     }

     update (node) {
      
      let rootNode = (this.skeleton) ? this.skeleton :node;
      
      this.joints.forEach((joint, i) => {
        const jointMatrix = m4.identity();
        m4.multiply(m4.inverse(m4.identity()), joint.matrixWorld, jointMatrix);
        m4.multiply(jointMatrix, this.inverseBindMatrices[i], jointMatrix);
    
        this.jointMatrices[i] = jointMatrix;
      });
      this.boneArray = [];
      this.jointMatrices.forEach((jointMatrix) => {
        this.boneArray.push(...jointMatrix);
      });
    };


}
