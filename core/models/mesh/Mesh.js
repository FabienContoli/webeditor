import Primitive from "./Primitive";

import {generateUUID} from "./../../helpers/uuidv4"
/**
 *  Represent a Mesh model
 *  @class
 */
export default class Mesh {

   /**
    * Mesh uuid
    * @type {string}
    * @public
    * @readonly
    */
      uuid;

   /**
    * name
    * @type {string}
    * @public
    */
   name;

   /**
    * An array of primitives, each defining geometry to be rendered.
    * @type Array<Primitive>
    * @required
    * @public
    */
   primitives;

   /**
    * 	
    * Array of weights to be applied to the morph targets. The number of array elements MUST match the number of morph targets.
    * @type Array<number>
    * @public
    */
   weights;

   constructor({ name, primitives, weights } = {}) {
      this.uuid= generateUUID();
      if (name) {
         this.name = name
      }
      
      this.primitives = primitives?primitives:[]
       
      if (weights) {
         this.weights = weights
      }
   }
}
