import { identity } from "../maths/m4";
import Skin from "./mesh/Skin";
import Node from "./Node"
/**
 *  Represent a Scene model
 *  @class
 */
export default class Scene {
    
    /**
     * name of the scene
     * @type {string}
     * @public
     */
    name;

    /**
     * node indexes
     * @type Array<Node>
     * @public
     */
    nodes;

    
  /**
     * node world relative matrix
     *
     */
   matrixWorld;
    

    /**
     * @required
     * @param {string} [name] 
     * @param {Array<Node>} [nodes] 
     */
    constructor({name,nodes}={}) {
        this.name=name;
        this.nodes=nodes;
        this.matrixWorld=identity()
    }

    update(){
        this.nodes.forEach((node)=>{
            node.update()
        })
        function updateSkin(node){
            if(node.skin){
                node.skin.update(node)
            }
            node.updateChildren(updateSkin)
        }
        this.nodes.forEach((node)=>{
            updateSkin(node)
            node.updateChildren(updateSkin)
        })
    }
}
