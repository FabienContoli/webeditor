import Input from "./Input"

/**
 * @class
 */
 export default class TouchInput extends Input {

    /**
     * @type {string}
     * @readonly
     */
     action

     /**
      * @type {object}
      * @readonly
      */
    value

      
     /**
      * @type {TouchEvent}
      * @readonly
      */
    event

    /**
     * @constructor
     * @param {string} action
     * @param {object} value 
     * @param {TouchEvent} event 
     */
     constructor(action,value,event) {
        
        super("touch");

        this.action = action
        this.value = value
        this.event=event
    }
}