import Input from "./Input"

/**
 * @class
 */
 export default class MouseInput extends Input {

    /**
     * @type {string}
     * @readonly
     */
    action

    /**
     * @type {number}
     * @readonly
     */
    input

    /**
     * @type {Object}
     * @readonly
     */
    value

    /**
     * @type {MouseEvent}
     * @readonly
     */
    event


    /**
     * @constructor
     * @param {string} action 
     * @param {number} input 
     * @param {object} value
     * @param {MouseEvent|WheelEvent} event
     */
     constructor(action, input, value, event) {
        
        super("mouse");

        this.action=action
        this.input=input
        this.value=value
        this.event=event
    }

}