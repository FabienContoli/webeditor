import Input from "./Input"

/**
 * @class
 */
 export default class GamepadInput extends Input {

    /**
     * @type {number}
     * @readonly
     */
    gamepadId

    /**
     * @type {string}
     * @readonly
     */
    action

    /**
     * @type {string}
     * @readonly
    */
    type

    /**
     * @type {number}
     * @readonly
    */
    index


    /**
    * @type {object}
    * @readonly
    */
    value


    /**
     * @constructor
     * @param {number} gamepadId 
     * @param {string} type  
     * @param {number} index 
     * @param {string} action 
     * @param {object} value
     */
    constructor(gamepadId,type,index,action,value) {
        
        super("gamepad");

        this.gamepadId=gamepadId
        this.type=type
        this.index=index
        this.action=action
        this.value=value
    }

}