/**
 * @class
 */
export default class Input {

    /**
     * @type {string}
      * @readonly
     */
     controller

    /**
     * @constructor
     * @param {("keyboard"|"gamepad"|"mouse"|"touch")} type  "keyboard"|"gamepad"|"mouse"|"touch"
     */
    constructor(type) {
        this.controller=type
    }

}