import Input from "./Input"

/**
 * @class
 */
 export default class KeyboardInput extends Input {

    /**
     * @type {string}
      * @readonly
     */
    action

    /**
     * @type {string}
      * @readonly
     */
    input
     
    /**
     * @type {KeyboardEvent}
    * @readonly
     */
    event

    /**
     * @constructor
     * @param {string} action 
     * @param {string} input 
     * @param {KeyboardEvent} event
     */
    constructor(action,input,event) {
        
        super("keyboard");

        this.action=action
        this.input=input
        this.event=event
    }

}