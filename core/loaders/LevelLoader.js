import * as GLTFConst from "./../const/GltfConst"
import * as m4 from "./../maths/m4"
import { generateUUID } from './../helpers/uuidv4';
import { subtractVec2 } from './../maths/vec2';
import { crossVectors3, normalizeVec3, subVectors3, sumVectors3 } from './../maths/vec3';
import Level from "./../models/Level";
import Scene from "./../models/Scene";
import Node from "./../models/Node";
import Camera from "./../models/camera/Camera";
import System from "./../models/System";
import Material from "./../models/mesh/material/Material";
import { getDefaultMaterial } from "./../placeholder/material"
import { getPathFolder } from "../helpers/file";
import Mesh from "../models/mesh/Mesh";
import Primitive from "../models/mesh/Primitive";
import Skin from "../models/mesh/Skin";
import Animation from "../models/Animation";

export default class LevelLoader {
    constructor(fileLoader,{filePath,sharedLevelLoader,cache,refCache}={}) {
      console.log("levelLoader");
      this.asset = null;
      this.filePath = filePath?filePath:"";
      this.subLoaderCache=sharedLevelLoader?sharedLevelLoader:{};
      this.refCache = refCache?refCache:{};
      this.nodeCache ={};
      this.cache = cache?cache:{};
      this.fileLoader= fileLoader
      this.levelBlueprint=null
      this.level=new Level();
      this.loadRef = this.memoize(this.refCache,this.loadRef);
      this.loadUri = this.memoize(this.cache,this.loadUri);
      this.loadMesh = this.memoize(this.cache,this.loadMesh);
      this.loadMaterial = this.memoize(this.cache,this.loadMaterial);
      this.loadImage = this.memoize(this.cache,this.loadImage);
      this.loadTexture = this.memoize(this.cache,this.loadTexture);
      this.loadBufferView = this.memoize(this.cache,this.loadBufferView);
      this.loadBuffer = this.memoize(this.cache,this.loadBuffer);
      this.loadAccessors = this.memoize(this.cache,this.loadAccessors);
      // this.loadChannel = this.memoize(this.cache,this.loadChannel);
      // this.loadAnimationSampler = this.memoize(this.cache,this.loadAnimationSampler);
      this.loadSampler = this.memoize(this.cache,this.loadSampler);
      this.loadSkin = this.memoize(this.cache,this.loadSkin);
      this.loadNode = this.memoize(this.nodeCache,this.loadNode); 
      this.externalAnimations =[]
    }

    loadArg(arg) {
        return typeof arg !== 'string' ? JSON.stringify(arg) : arg;
      }
    
      
    // memoizeRef(cache,method) {
    //   return (...args) => new Promise((resolve) => {
    //       const strArgs = this.loadArg(args[0]);
    //       let key = `${method.name}_${strArgs}`;

    //       let cacheValue;
    //       if (this.refCache[key]) {
    //         console.log(key);
    //         cacheValue = this.refCache[key];
    //       } else {
    //         cacheValue = method.apply(this, args);
    //         this.refCache[key] = cacheValue;
    //       }
    //       Promise.resolve(cacheValue).then((value) => {
    //       resolve(value);
    //       });
    //   });
    // }

    memoize(cache,method) {
      return (...args) => new Promise((resolve) => {
          const strArgs = this.loadArg(args[0]);
          let key = `${method.name}_${strArgs}`;

          if (args[1]) {
          const strArgs2 = this.loadArg(args[2]);
          key = `${key}_${strArgs2}`;
          }

          let cacheValue;
          if (cache[key]) {
            console.log(key);
            cacheValue = cache[key];
          } else {
          cacheValue = method.apply(this, args);
          cache[key] = cacheValue;
          }
          Promise.resolve(cacheValue).then((value) => {
          resolve(value);
          });
      });
    }
   
    async loadRef(ref){
        return await this.fileLoader.loadRef(ref) 
    }

   async loadUri(uri, { byteLength, needBlob = false }) {
      const dataUriRegex = /^data:(.*?)(;base64)?,(.*)$/;
      const dataUriMatch = uri.match(dataUriRegex);
      if (dataUriMatch) {
        const mimeType = dataUriMatch[1];
        let data = dataUriMatch[3];
        data = decodeURIComponent(data);

        if (dataUriMatch[2]) data = atob(data);

        if (needBlob) {
          const sliceSize = 512;
          const byteArrays = [];
          for (let offset = 0; offset < data.length; offset += sliceSize) {
            const slice = data.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i += 1) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
          }

          return new Blob(byteArrays, { type: mimeType });
        } else {
          const view = new Uint8Array(data.length);

          for (let i = 0; i < data.length; i += 1) {
            view[i] = data.charCodeAt(i);
          }
          return view.buffer;
        }
      } else {
        if (uri.startsWith('./')){
          uri = uri.substring(2)
        }

        let file =await this.fileLoader.loadRef({uri:this.filePath+"/"+uri})
        return file
        // import(`/sources/game/import/gltf/${decodeURI(this.asset.scope)}/${decodeURI(uri)}`).then(
        //   (file) => {
        //     if (uri.match(/(.*).(bin)$/)) {
        //       callBinary(file.default).then((response) => {
        //         resolve(response);
        //       });
        //     } else if (uri.match(/(.*).(png|jpeg|jpg|gif)$/)) {
        //       callImage(file.default).then((response) => {
        //         resolve(response);
        //       });
        //     } else {
        //       callUri(file.default).then((response) => {
        //         resolve(response);
        //       });
        //     }
        //   },
        // );
      }
  }

  loadBuffer(index) {
    return new Promise((resolve) => {
      const bufferDef = this.levelBlueprint.buffers[index];

      // If GLB file
  		if (bufferDef.uri === undefined && index === 0) {
  			resolve(gltf.glbBody);
  		} else {
        this.loadUri(bufferDef.uri, { byteLength: bufferDef.byteLength }).then((buffer) => {
          resolve(buffer);
        });
      }
    });
  }

  loadBufferView(index) {
    return new Promise((resolve) => {
      const bufferViewRef = this.levelBlueprint.bufferViews[index];
      this.loadBuffer(bufferViewRef.buffer).then((buffer) => {
        const byteLength = bufferViewRef.byteLength || 0;
        const byteOffset = bufferViewRef.byteOffset || 0;
        const bufferView = buffer.slice(byteOffset, byteOffset + byteLength);
        resolve(bufferView);
      });
    });
  }

  loadMatrix4(matrix) {
    return m4.copy(matrix);
  }

  loadVec2(matrix) {
    return matrix;
  }

  loadVec3(matrix) {
    return matrix;
  }

  loadVec4(matrix) {
    return matrix;
  }

  loadSampler(samplerIndex) {
    return this.levelBlueprint.samplers[samplerIndex];
  }

  loadChannel(animIndex, channelIndex) {
    return new Promise((resolve) => {
      const channelRef = this.levelBlueprint.animations[animIndex].channels[channelIndex];
      const promises = [];
      const channel = { target: {} };
      promises.push(this.loadAnimationSampler(animIndex, channelRef.sampler).then((sampler) => {
        channel.sampler = sampler;
      }));
      promises.push(this.loadNode(channelRef.target.node).then((node) => {
        channel.target.node = node;
      }));
      channel.target.path = channelRef.target.path;

      Promise.all(promises).then(() => {
        resolve(channel);
      });
    });
  }

  loadAnimationSampler(animIndex, samplerIndex) {
    return new Promise((resolve) => {
      const samplerRef = this.levelBlueprint.animations[animIndex].samplers[samplerIndex];
      const promises = [];
      const sampler = {};

      promises.push(this.loadAccessors(samplerRef.input).then((accessor) => {
        sampler.input = accessor;
      }));

      promises.push(this.loadAccessors(samplerRef.output).then((accessor) => {
        sampler.output = accessor;
      }));

      sampler.interpolation = samplerRef.interpolation ? samplerRef.interpolation : 'LINEAR';

      Promise.all(promises).then(() => {
        sampler.valueSize = sampler.output.count / sampler.input.count;
        resolve(sampler);
      });
    });
  }

  loadCamera(index) {
    return this.levelBlueprint.cameras[index];
  }

  loadSkin(index) {
    return new Promise((resolve) => {
      const skinRef = this.levelBlueprint.skins[index];
      const skin = { joints: [], jointMatrices: [] };
      const promises = [];

      skin.inverseBindMatrices = [];
      skinRef.joints.forEach((joint, i) => {
        promises.push(this.loadNode(joint).then((node) => {
          node.isJoint = true;
          node.indexJoint = joint;
          skin.jointMatrices[i] = m4.identity();
          skin.joints[i] = node;
        }));
      });

      if (skinRef.skeleton) {
        promises.push(this.loadNode(skinRef.skeleton).then((node) => {
          node.isSkeleton = true;
          skin.skeleton = node;
        }));
      }

      if (skinRef.inverseBindMatrices) {
        promises.push(this.loadAccessors(skinRef.inverseBindMatrices).then((accessor) => {
          skin.inverseBindMatrices = [];
          for (let i = 0; i < accessor.count; i += 1) {
            skin.inverseBindMatrices[i] = accessor.array.slice(accessor.offset + i * accessor.itemSize, accessor.offset + i * accessor.itemSize + accessor.itemSize);
          }
        }));
      } else {
        for (let i = 0; i < skinRef.joints.length; i += 1) {
          skin.inverseBindMatrices[i]= m4.identity();
        }
      }

      if (skinRef.name) {
        skin.name = skinRef.name;
      }
      if (skinRef.extensions) {
        skin.extensions = skinRef.extensions;
      }
      if (skinRef.extras) {
        skin.extras = skinRef.extras;
      }

      Promise.all(promises).then((values) => {
        resolve(new Skin(skin));
      });
    });
  }

  loadImage(index) {
    return new Promise((resolve) => {
      const imageRef = this.levelBlueprint.images[index];
      let image = {};
      const promises = [];

      if (imageRef.uri) {
        promises.push(new Promise((resolveImg) => {
          this.loadUri(imageRef.uri, { needBlob: true }).then((img) => {
            createImageBitmap(img, /* firefox don't support argument :
            , {
              premultiplyAlpha: 'none',
              colorSpaceConversion: 'none',
            } */).then((bitmap) => {
              image = bitmap;
              resolveImg(bitmap);
            }).catch((err) => {
              console.log(err);
            });
            /* image = img;
            resolveImg(img); */
          /*  createImageBitmap(img).then((bitmapImg) => {
              image = bitmapImg;
              resolveImg(image);
            }); */
          });
        }));
      } else {
        promises.push(this.loadBufferView(index).then((bufferView) => {
          const blob = new Blob([bufferView], { type: imageRef.mimeType });
          // const sourceURI = URL.createObjectURL(blob);
          image = blob;
        }));
      }

      Promise.all(promises).then((values) => {
        resolve(image);
      });
    });
  }

  loadTexture(index) {
    return new Promise((resolve) => {
      const textureRef = this.levelBlueprint.textures[index];
      const texture = { uuid: generateUUID() };
      const promises = [];
      promises.push(this.loadImage(textureRef.source).then((image) => {
        texture.image = image;
      }));

      let samplerPromise = null;
      if (typeof textureRef.sampler === 'number') {
        samplerPromise = this.loadSampler(textureRef.sampler);
      } else {
        samplerPromise = new Promise((resolve) => { resolve({}); });
      }
      promises.push(samplerPromise.then((sampler) => {
        texture.magFilter = sampler.magFilter || 9729; // linearfilter
        texture.minFilter = sampler.minFilter || 9987; // LinearMipmapLinearFilter
        texture.wrapS = sampler.wrapS || 10497;// RepeatWrapping
        texture.wrapT = sampler.wrapT || 10497;// RepeatWrapping
      }));

      // material.baseColorTexture
      Promise.all(promises).then((values) => {
        resolve(texture);
      });
    });
    // search for textures
  }

  // https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#metallic-roughness-material
  loadPbrMetallicRoughness(metallicRoughness) {
    return new Promise((resolve) => {
      const promises = [];
      const materialParams = {};
  			materialParams.baseColorFactor = [1.0, 1.0, 1.0, 1.0];
  			if (Array.isArray(metallicRoughness.baseColorFactor)) {
        materialParams.baseColorFactor = metallicRoughness.baseColorFactor;
  			}

  			if (metallicRoughness.baseColorTexture !== undefined) {
        promises.push(this.loadTexture(metallicRoughness.baseColorTexture.index).then((texture) => {
          materialParams.baseColorTexture = { texture, texCoord: 0 };
          if (typeof metallicRoughness.baseColorTexture.texCoord === 'number') {
            materialParams.baseColorTexture.texCoord = metallicRoughness.baseColorTexture.texCoord;
          }
        }));
  			}

      materialParams.metallicFactor = metallicRoughness.metallicFactor !== undefined ? metallicRoughness.metallicFactor : 1.0;
      materialParams.roughnessFactor = metallicRoughness.roughnessFactor !== undefined ? metallicRoughness.roughnessFactor : 1.0;

  			if (metallicRoughness.metallicRoughnessTexture !== undefined) {
        promises.push(this.loadTexture(metallicRoughness.metallicRoughnessTexture.index).then((texture) => {
          // replace by async loadAccessors

          materialParams.metallicRoughnessTexture = { texture, texCoord: 0 };
          if (typeof metallicRoughness.metallicRoughnessTexture.texCoord === 'number') {
            materialParams.metallicRoughnessTexture.texCoord = metallicRoughness.metallicRoughnessTexture.texCoord;
          }
        }));
  			}

      Promise.all(promises).then(() => {
        resolve(materialParams);
      });
    });
  }

  async loadDefaultMaterial() {
    return await getDefaultMaterial()
    // return new Promise((resolve) => {
    //   resolve({
    //     name: 'defaultMaterial',
    //     emissiveFactor: [0, 0, 0],
    //     alphaMode: 'OPAQUE',
    //     alphaCutoff: 0.5,
    //     doubleSided: false,
    //     metallicRoughness: {
    // 			baseColorFactor: [1.0, 1.0, 1.0, 1.0],
    // 	    metalness: 1.0,
    //       roughness: 1.0,
    //     },
    //   });
  }

  loadMaterial(index) {
    return new Promise((resolve) => {
      const material = this.levelBlueprint.materials[index];
      const promises = [];
      const computeMaterial = {};
      computeMaterial.name = material.name;

      promises.push(this.loadPbrMetallicRoughness(material.pbrMetallicRoughness || {}).then((materialParams) => {
        computeMaterial.pbrMetallicRoughness = materialParams;
      }));

      if (material.normalTexture) {
        promises.push(this.loadTexture(material.normalTexture.index).then((texture) => {
          const { normalTexture } = material;
          computeMaterial.normalTexture = {
            texture,
            strength: typeof normalTexture.scale === 'number' ? normalTexture.scale : 1,
            texCoord: 0,
          };

          if (typeof normalTexture.texCoord === 'number') {
            computeMaterial.normalTexture.texCoord = normalTexture.texCoord;
          }

          if (normalTexture.extensions) {
            console.error('material extensions not handle');
            computeMaterial.normalTexture.extensions = normalTexture.extensions;
          }
          if (normalTexture.extras) {
            console.error('material extras not handle');
            computeMaterial.normalTexture.extras = normalTexture.extras;
          }
        }));
      }

      if (material.occlusionTexture) {
        const { occlusionTexture } = material;
        promises.push(this.loadTexture(material.occlusionTexture.index).then((texture) => {
          computeMaterial.occlusionTexture = {
            texture,
            strength: typeof occlusionTexture.strength === 'number' ? occlusionTexture.strength : 1,
            texCoord: 0,
          };

          if (typeof occlusionTexture.texCoord === 'number') {
            computeMaterial.occlusionTexture.texCoord = occlusionTexture.texCoord;
          }

          if (occlusionTexture.extensions) {
            console.error('material extensions not handle');
            computeMaterial.occlusionTexture.extensions = occlusionTexture.extensions;
          }
          if (occlusionTexture.extras) {
            console.error('material extras not handle');
            computeMaterial.occlusionTexture.extras = occlusionTexture.extras;
          }
        }));
      }
      if (material.emissiveTexture) {
        promises.push(this.loadTexture(material.emissiveTexture.index).then((texture) => {
          computeMaterial.emissiveTexture = { texture, texCoord: 0 };
          if (typeof material.emissiveTexture.texCoord === 'number') {
            computeMaterial.emissiveTexture.texCoord = material.emissiveTexture.texCoord;
          }
        }));
      }
      if (material.extensions) {
        console.error('material extensions not handle');
        computeMaterial.extensions = material.extensions;
      }
      if (material.extras) {
        console.error('material extras not handle');
        computeMaterial.extras = material.extras;
      }

      computeMaterial.emissiveFactor = material.emissiveFactor ? material.emissiveFactor : [0, 0, 0];

      computeMaterial.alphaMode = material.alphaMode ? material.alphaMode : 'OPAQUE';
      if (computeMaterial.alphaMode === 'MASK') {
        computeMaterial.alphaCutoff = material.alphaCutoff ? material.alphaCutoff : 0.5;
      } else {
        computeMaterial.alphaCutoff = 0;
      }

      computeMaterial.doubleSided = typeof material.doubleSided === 'boolean' ? material.doubleSided : false;
      
      Promise.all(promises).then(() => {
        let materialInstance =new Material(computeMaterial)
        console.log("uuid",materialInstance.uuid);
        resolve(materialInstance);
      });
    });
  // search for textures
  }

  checkForUnknownTextureProperty(material) {
    const entries = Object.entries(material);
    for (let i = 0; i < entries.length; i += 1) {
      const [key, value] = entries[i];
      switch (key) {
        case 'name':
        case 'pbrMetallicRoughness':
        case 'normalTexture':
        case 'occlusionTexture':
        case 'emissiveTexture':
        case 'emissiveFactor':
          break;
        default:
          console.error(`material key ${key} not known`);
      }
    }
  }

  async loadAttributes(attributes) {
    const promises = [];
    const entries = Object.entries(attributes);
    const computeAttribute = {};
    for (let i = 0; i < entries.length; i += 1) {
      const [key, value] = entries[i];
      promises.push(this.loadAccessors(value).then((bufferAttribute) => {
        computeAttribute[GLTFConst.ATTRIBUTES[key]] = bufferAttribute;
      }));
    }
    await Promise.all(promises)
    return computeAttribute
  }

  // https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#accessors
  async loadAccessors(index) {
      const accessor = this.levelBlueprint.accessors[index];
      const bufferViewsPromises = [];

      if (accessor.bufferView !== undefined) {
        bufferViewsPromises.push(this.loadBufferView(accessor.bufferView));
      }

      if (accessor.sparse !== undefined) {
        bufferViewsPromises.push(this.loadBufferView(accessor.sparse.indices.bufferView));
        bufferViewsPromises.push(this.loadBufferView(accessor.sparse.values.bufferView));
      }

      const itemSize = GLTFConst.WEBGL_TYPE_SIZES[accessor.type];
      const TypedArray = GLTFConst.COMPONENT_TYPES[accessor.componentType];

      const elementBytes = TypedArray.BYTES_PER_ELEMENT;
      const itemBytes = elementBytes * itemSize;

      const bufferViews=await Promise.all(bufferViewsPromises)

      const bufferView = bufferViews[0];

      const byteOffset = accessor.byteOffset || 0;
      const byteStride = accessor.bufferView !== undefined ? this.levelBlueprint.bufferViews[accessor.bufferView].byteStride : undefined;

      const normalized = accessor.normalized === true;
      let array;
      let bufferAttribute = {};

      /*
      Implementation Note: JavaScript client implementations should convert JSON-parsed floating-point doubles to single precision, when componentType is 5126 (FLOAT). This could be done with Math.fround function.
      */
      if (byteStride && byteStride !== itemBytes) {
        // Each "slice" of the buffer, as defined by 'count' elements of 'byteStride' bytes, gets its own InterleavedBuffer
        // This makes sure that IBA.count reflects accessor.count properly
        const ibSlice = Math.floor(byteOffset / byteStride);
        // length
        array = new TypedArray(bufferView, ibSlice * byteStride, accessor.count * byteStride / elementBytes);
        bufferAttribute = {
          min: accessor.min,
          max: accessor.max,
          array,
          componentType: accessor.componentType,
          count: accessor.count,
          itemSize,
          normalized,
          offset: (byteOffset % byteStride),
          stride: byteStride,
          // offset: (byteOffset % byteStride) / elementBytes,
          // stride: byteStride / elementBytes,
        };
      } else {
        if (bufferView === null) {
          array = new TypedArray(accessor.count * itemSize);
        } else {
          array = new TypedArray(bufferView, byteOffset, accessor.count * itemSize);
        }
        bufferAttribute = {
          min: accessor.min,
          max: accessor.max,
          array,
          componentType: accessor.componentType,
          count: accessor.count,
          itemSize,
          offset: 0,
          normalized,
        };
      }

      if (accessor.sparse !== undefined) {
        const bufferViewIndices = bufferViews[1];
        const bufferViewValues = bufferViews[2];

        const TypedArrayIndices = GLTFConst.COMPONENT_TYPES[accessor.sparse.indices.componentType];
        const arraySparseIndices = new TypedArrayIndices(bufferViewIndices, accessor.sparse.indices.byteOffset, accessor.sparse.count);

        let arraySparseValues;
        if (bufferViewValues === null) {
          arraySparseValues = new TypedArray(accessor.sparse.count * itemSize);
        } else {
          arraySparseValues = new TypedArray(bufferViewValues, accessor.sparse.values.byteOffset, accessor.sparse.count * itemSize);
        }

        for (let i = 0; i < accessor.sparse.count; i += 1) {
          const indexBuffer = arraySparseIndices[i] * itemSize;
          const offsetValues = i * itemSize;
          for (let c = 0; c < itemSize; c += 1) {
            bufferAttribute.array[indexBuffer + c] = arraySparseValues[offsetValues + c];
          }
        }
      }
      
      // missing return of other attributes
      return bufferAttribute;
  }

  loadTargets(targetsRef) {
    return new Promise((resolve) => {
      const promises = [];
      const targets = [];
      targetsRef.forEach((targetRef, i) => {
        promises.push(this.loadAttributes(targetRef).then((attributes) => {
          targets[i] = attributes;
        }));
      });
      Promise.all(promises).then(() => {
        resolve(targets);
      });
    });
  }

  async loadPrimitive(primitiveRef) {
      const primitive = {};
      const promises = [];
      
      if(primitiveRef.attributes){
        promises.push(this.loadAttributes(primitiveRef.attributes).then((attributes) => {
          primitive.attributes = attributes;
        }));
      }

        if(typeof primitiveRef.indices==="number"){
        promises.push(this.loadAccessors(primitiveRef.indices).then((indices) => {
          primitive.indices = indices;
        }));
      }

      if(primitiveRef.targets){
        promises.push(this.loadTargets(primitiveRef.targets).then((targets) => {
          primitive.targets = targets;
        }));
      }
    
      primitive.mode = typeof primitiveRef.mode === 'number' ? primitiveRef.mode : 4;

      if (typeof primitiveRef.material === 'number') {
        promises.push(this.loadMaterial(primitiveRef.material).then((material) => {
          primitive.material = material;
        }));
      } else {
        promises.push(this.loadDefaultMaterial().then((material) => {
          primitive.material = material;
        }));
      }
      await Promise.all(promises)
      return new Primitive(primitive);
      
  }

  makeIndexIterator(indices) {
    let i = 0;
    const { offset, stride } = indices;
    const itemSize = 3;
    let totalOffset = 1;
    if (offset) totalOffset += offset;
    if (stride) totalOffset += stride;
    return () => {
      i += (totalOffset) * itemSize;
      return indices[i];
    };
  }

  makeUnindexedIterator(posAttributes) {
    const { offset, stride } = posAttributes;
    const itemSize = 3;
    let totalOffset = 1;
    if (offset) totalOffset += offset;
    if (stride) totalOffset += stride;
    let i = 0;
    return () => i += (totalOffset) * itemSize;
  }

  computeTangent(position, uv, indices) {
    const getNextIndex = indices ? this.makeIndexIterator(indices) : this.makeUnindexedIterator(position);
    const count = indices ? indices.count : position.count;

    const positionArray = position.array;
    const uvArray = uv.array;

    const tangents = [];
    for (let i = 0; i < count; i += 1) {
      const n1 = getNextIndex();
      const n2 = getNextIndex();
      const n3 = getNextIndex();

      const p1 = positionArray.slice(n1 * 3, n1 * 3 + 3);
      const p2 = positionArray.slice(n2 * 3, n2 * 3 + 3);
      const p3 = positionArray.slice(n3 * 3, n3 * 3 + 3);

      const uv1 = uvArray.slice(n1 * 2, n1 * 2 + 2);
      const uv2 = uvArray.slice(n2 * 2, n2 * 2 + 2);
      const uv3 = uvArray.slice(n3 * 2, n3 * 2 + 2);

      const dp12 = m4.subtractVectors(p2, p1);
      const dp13 = m4.subtractVectors(p3, p1);

      const duv12 = subtractVec2(uv2, uv1);
      const duv13 = subtractVec2(uv3, uv1);

      const f = 1.0 / (duv12[0] * duv13[1] - duv13[0] * duv12[1]);
      const tangent = Number.isFinite(f)
        ? m4.normalize(m4.scaleVector(m4.subtractVectors(
          m4.scaleVector(dp12, duv13[1]),
          m4.scaleVector(dp13, duv12[1]),
        ), f))
        : [1, 0, 0];

      tangents.push(...tangent, ...tangent, ...tangent);
      return {
        itemSize: 3, array: tangents, offset: 0, normalized: false, count: count * 3,
      };
    }

    return tangents;
  }

  computeNormal(posAttributes, indices) {
    const array = [];
    const pos = posAttributes.array;
    const count = indices ? indices.count : posAttributes.count / 3;
    const getNextIndex = indices ? this.makeIndexIterator(indices) : this.makeUnindexedIterator(posAttributes);
    for (let i = 0; i < count; i += 1) {
      let vec3Normal = [0, 0, 0];
      const offsetA = getNextIndex();
      const offsetB = getNextIndex();
      const offsetC = getNextIndex();

      const vecA = [pos[offsetA], pos[offsetA + 1], pos[offsetA + 2]];
      const vecB = [pos[offsetB], pos[offsetB + 1], pos[offsetB + 2]];
      const vecC = [pos[offsetC], pos[offsetC + 1], pos[offsetC + 2]];

      vec3Normal = sumVectors3(vec3Normal, crossVectors3(subVectors3(vecA, vecB), subVectors3(vecC, vecB)));
      array.push(...normalizeVec3(vec3Normal));
      array.push(...normalizeVec3(vec3Normal));
      array.push(...normalizeVec3(vec3Normal));
    }
    return {
      itemSize: 3, array, offset: 0, normalized: false, count,
    };
  }

  async loadMesh(index) {
      const promises = [];
      
      const meshBlueprint = this.levelBlueprint.meshes[index];
      const mesh = {primitives:[]};
      meshBlueprint.primitives?.forEach(primitiveBlueprint => {
        promises.push(this.loadPrimitive(primitiveBlueprint).then((primitive) => {
          if (primitive.attributes && !primitive.attributes.normal) {
            primitive.flatNormal = true;
            primitive.attributes.normal = this.computeNormal(primitive.attributes.position, mesh.indices);
          }
          if (primitive.attributes && !primitive.attributes.tangent && primitive.attributes.texCoord0 && !mesh.flatNormal) {
            //    mesh.attributes.tangent = this.computeTangent(mesh.attributes.position, mesh.attributes.texCoord0, mesh.indices);
          }
          mesh.primitives.push(primitive);
        }));
      });

      mesh.weights = meshBlueprint.weights;  
      mesh.name = meshBlueprint.name;
          
      await Promise.all(promises)
      return new Mesh(mesh)      
  }
  
  setMesh(node,mesh){
    node.mesh=mesh
    node.weights=mesh.weights
    mesh.weights=undefined
    this.level.meshes.set(node.uuid,node);
  }

  async loadSubLevel(ref){
    let json =await this.loadRef(ref)
    json =JSON.parse(JSON.stringify(json))
    let fileFolder= getPathFolder(ref.uri)
    let loader;
    if(!this.subLoaderCache[ref.uri]){
      loader =new LevelLoader(this.fileLoader,{filePath:fileFolder,sharedLevelLoader:this.subLoaderCache,refCache:this.refCache})
      this.subLoaderCache[ref.uri]=loader
      
      let levelInstance=await loader.load(json)
       this.externalAnimations.push(...levelInstance.animations.values())  
      return levelInstance.scene.nodes
    }else{
      let subLevelLoader=this.subLoaderCache[ref.uri]
      loader =new LevelLoader(this.fileLoader,{filePath:fileFolder,sharedLevelLoader:this.subLoaderCache,refCache:this.refCache,cache:subLevelLoader.cache})
   
      let levelInstance=await loader.load(json)
    
      return levelInstance.scene.nodes
    }

  }

  async loadNode(index) {
      const promises = [];
      const nodeBlueprint = this.levelBlueprint.nodes[index];

      const node=new Node(nodeBlueprint,index);
      node.children =[]
      nodeBlueprint.children?.forEach((nodeRef) => {
        if(typeof nodeRef === "number"){
          promises.push(this.loadNode(nodeRef).then((childNode) => {
            childNode.parent = node;
            node.children.push(childNode);
          }));
        }else {
          promises.push(this.loadSubLevel(nodeRef).then((childNodes) => {
            childNodes.forEach((childNode)=>{
              node.children.push(childNode);
              childNode.parent = node;
            })
          }))
        }
      });
      
      if(typeof nodeBlueprint.mesh==="number"){
        promises.push(this.loadMesh(nodeBlueprint.mesh).then((mesh) => {
            this.setMesh(node,mesh)
        }));
      }
    

      if(typeof nodeBlueprint.camera==="number"){
        node.camera = new Camera(this.loadCamera(nodeBlueprint.camera));
        node.camera.node = node
        this.level.cameras.set(node.camera.uuid,node.camera);
        if(nodeBlueprint.camera===this.levelBlueprint.camera){
          this.level.camera=node.camera
        }
      }
         
      if(typeof nodeBlueprint.skin==="number"){
        promises.push(this.loadSkin(nodeBlueprint.skin).then((skin) => {
          if(skin.skeleton){
            skin.skeleton.parent=node
          }
          node.skin = skin;
        
          this.level.skins.set(node.uuid,node);
        }));
      }
    
      await Promise.all(promises)
      return node;
  }

  async loadScene(index) {
      const sceneBlueprint = this.levelBlueprint.scenes[index];
      const promises=[]
      const scene =new Scene({name:sceneBlueprint.name,nodes:[]})
      if(sceneBlueprint.nodes){
        sceneBlueprint.nodes.forEach((nodeIndex) => {
          promises.push(this.loadNode(nodeIndex).then((childNode) => {
            scene.nodes.push(childNode);
            childNode.parent = scene;
          }));
        });
      }
    
    await Promise.all(promises)
        
    return scene
  }

  loadAnimation(animIndex) {
    return new Promise((resolve) => {
      const animationRef = this.levelBlueprint.animations[animIndex];
      const anim = { channels: [], samplers: [], maxTime: 0 };
      const entries = Object.entries(animationRef);
      const promises = [];
      for (let e = 0; e < entries.length; e += 1) {
        const [key, value] = entries[e];

        switch (key) {
          case 'name':
            anim.name = value;
            break;
          case 'channels':
            for (let i = 0; i < value.length; i += 1) {
              promises.push(this.loadChannel(animIndex, i).then((channel) => {
                anim.channels[i]=channel;
              }));
            }
            break;

          case 'samplers':
            for (let i = 0; i < value.length; i += 1) {
              promises.push(this.loadAnimationSampler(animIndex, i).then((sampler) => {
                anim.samplers[i]=sampler;
                if (anim.maxTime < sampler.input.max[0])anim.maxTime = sampler.input.max[0];
              }));
            }
            break;
          default:
            console.error(`loadAnimations key ${key} not handled`);
        }
      }



      Promise.all(promises).then(() => {
        resolve(new Animation(anim));
      });
    });
  }

  async loadAnimations() {
      const gltfAnimations = this.levelBlueprint.animations;
      if (gltfAnimations) {
        const animationPromises = [];
        for (let i = 0; i < gltfAnimations.length; i += 1) {
          animationPromises.push(this.loadAnimation(i).then((value) => {
            this.level.animations.set(value.uuid,value)
          }));
        }
        await Promise.all(animationPromises)
        return;
      } else {
        return null
      }
  }

  
  async loadSystem(systemBlueprint){
    let script = await this.loadRef(systemBlueprint.ref)
    let system=new System({name:systemBlueprint.name,script})
    this.level.systems.set(system);
    return 
  }

  async loadSystems(){
    let promises=[]
    if(this.levelBlueprint.systems){
      this.levelBlueprint.systems.forEach((system)=>{
        promises.push(this.loadSystem(system));
      })
    }
    return await Promise.all(promises) 
  }
  
  async loadScripts(){
    let promises=[]
    this.levelBlueprint.scripts.forEach((script)=>{
      let ref=script.ref
      promises.push(this.loadRef(ref).then((value)=>{
        if(!value)console.warn("script:"+script.name+" not found")
        else this.level.scripts.set(script.name,value);
      }))
    })
    return await Promise.all(promises);
  }

    /*

https://github.com/KhronosGroup/glTF/blob/master/extensions/README.md

KHR_draco_mesh_compression
KHR_lights_punctual
KHR_materials_clearcoat
KHR_materials_ior
KHR_materials_pbrSpecularGlossiness
KHR_materials_sheen
KHR_materials_specular
KHR_materials_transmission
KHR_materials_unlit
KHR_materials_variants
KHR_materials_volume
KHR_mesh_quantization
KHR_texture_basisu
KHR_texture_transform
KHR_xmp

*/

      printGraph (node) {

        console.group(' <' + node.name + '> ');
        node.children.forEach((child) => this.printGraph(child));
        console.groupEnd();
    
      }

    async load(levelBlueprint,{filePath, sceneIndexOverride}={}) {
        this.level=new Level()
        this.levelBlueprint=levelBlueprint
        const promises = [];
        
        if (levelBlueprint.extensionsUsed) {
            console.table(levelBlueprint.extensionsUsed);
        }

        if (levelBlueprint.extensionsRequired) {
            console.table(levelBlueprint.extensionsRequired);
        }

        let sceneIndex=0
        if(sceneIndexOverride){
            sceneIndex = sceneIndexOverride
        }else if (typeof levelBlueprint.scene === 'number') {
            sceneIndex = levelBlueprint.scene;
        }
     
        promises.push(this.loadScene(sceneIndex).then((value) => {
            this.level.scene = value;
            if(this.level.camera.size>0){
              if(typeof levelBlueprint.camera==='number'){
                this.level.camera=this.level.cameras[levelBlueprint.camera]
              } else{
                this.level.camera=this.level.cameras[0]
              }
            }
            this.level.scene.nodes.forEach((node)=>this.printGraph(node))
        }));

        promises.push(this.loadAnimations())

        if(this.levelBlueprint.commands){
          this.levelBlueprint.commands.forEach((command)=>{
            let input = command.input;
            this.level.commands.set(input.controller+input.action+input.input??'',command.name);
          })
        }

        promises.push(this.loadSystems())

        if(this.levelBlueprint.scripts){
          promises.push(this.loadScripts())
        }

        await Promise.all(promises)

        this.externalAnimations.forEach(externalAnimation=>{
          this.level.animations.set(externalAnimation.uuid,externalAnimation)
        })
        
        return this.level; 
    }
}