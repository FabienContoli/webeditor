const callUri = function callUri(uri) {
    return new Promise((resolve, reject) => {
      if (typeof uri === 'string') {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', uri, true);
        // Envoie les informations du header adaptées avec la requête
        xhr.onreadystatechange = function () { // Appelle une fonction au changement d'état.
          if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            const responseObj = xhr.response;
            // Requête finie, traitement ici.
            resolve(responseObj);
          } else if (this.readyState === XMLHttpRequest.DONE) {
            reject();
          }
        };
        xhr.send();
      } else reject();
    });
  };
  
  const callBinary = function callBinary(uri) {
    return new Promise((resolve, reject) => {
      if (typeof uri === 'string') {
        const xhr = new XMLHttpRequest();
        xhr.responseType = 'arraybuffer';
        xhr.open('GET', uri, true);
        // Envoie les informations du header adaptées avec la requête
        xhr.onreadystatechange = function () { // Appelle une fonction au changement d'état.
          if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            const responseObj = xhr.response;
            // Requête finie, traitement ici.
            resolve(responseObj);
          } else if (this.readyState === XMLHttpRequest.DONE) {
            reject();
          }
        };
        xhr.send();
      } else reject();
    });
  };
  
  const callImage = function callImage(uri) {
    return new Promise((resolve, reject) => {
      if (typeof uri === 'string') {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', uri, true);
  
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.responseType = 'arraybuffer';
        // Envoie les informations du header adaptées avec la requête
        xhr.onreadystatechange = function () { // Appelle une fonction au changement d'état.
          if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            const responseObj = xhr.response;
            // Requête finie, traitement ici.
            const dataView = new DataView(responseObj);
            const blob = new Blob([dataView], { type: 'image/png' });
            resolve(blob);
          } else if (this.readyState === XMLHttpRequest.DONE) {
            reject();
          }
        };
        xhr.send();
      } else reject();
    });
  };
  
  export { callUri, callBinary, callImage };
  