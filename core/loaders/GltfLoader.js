// https://www.khronos.org/files/gltf20-reference-guide.pdf
import * as GLTFConst from 'core/const/GLTFConst';
import * as m4 from '../maths/m4';
import { callUri, callBinary, callImage } from 'core/loaders/fileLoader';
import { generateUUID } from '../helpers/uuidv4';
import { subtractVec2 } from '../maths/vec2';
import { crossVectors3, normalizeVec3, subVectors3, sumVectors3 } from '../maths/vec3';

class GLTFLoader {
  constructor() {
    this.asset = null;
    this.cache = {};
    this.cameraNodes = {};
    this.meshNodes = {};
    this.skinnedMeshNodes = {};
    this.loadUri = this.memoize(this.loadUri);
    this.loadMesh = this.memoize(this.loadMesh);
    this.loadMaterial = this.memoize(this.loadMaterial);
    this.loadDefaultMaterial = this.memoize(this.loadDefaultMaterial);
    this.loadImage = this.memoize(this.loadImage);
    this.loadSkin = this.memoize(this.loadSkin);
    this.loadTexture = this.memoize(this.loadTexture);
    this.loadBufferView = this.memoize(this.loadBufferView);
    this.loadBuffer = this.memoize(this.loadBuffer);
    this.loadAccessors = this.memoize(this.loadAccessors);
    this.loadChannel = this.memoize(this.loadChannel);
    this.loadAnimationSampler = this.memoize(this.loadAnimationSampler);
    this.loadSampler = this.memoize(this.loadSampler);
    this.loadNode = this.memoize(this.loadNode);
  }

  loadArg(arg) {
    return typeof arg !== 'string' ? JSON.stringify(arg) : arg;
  }

  memoize(method) {
    return (...args) => new Promise((resolve) => {
      const strArgs = this.loadArg(args[1]);
      let key = `${method.name}_${strArgs}`;

      if (args[2]) {
        const strArgs2 = this.loadArg(args[2]);
        key = `${key}_${strArgs2}`;
      }

      let cacheValue;
      if (this.cache.hasOwnProperty(key)) {
        cacheValue = this.cache[key];
      } else {
        cacheValue = method.apply(this, args);
        this.cache[key] = cacheValue;
      }
      Promise.resolve(cacheValue).then((value) => {
        resolve(value);
      });
    });
  }

  loadUri(gltf, uri, { byteLength, needBlob = false }) {
    return new Promise((resolve) => {
      const dataUriRegex = /^data:(.*?)(;base64)?,(.*)$/;
      const dataUriMatch = uri.match(dataUriRegex);
      if (dataUriMatch) {
        const mimeType = dataUriMatch[1];
        let data = dataUriMatch[3];
        data = decodeURIComponent(data);

        if (dataUriMatch[2]) data = atob(data);

        if (needBlob) {
          const sliceSize = 512;
          const byteArrays = [];
          for (let offset = 0; offset < data.length; offset += sliceSize) {
            const slice = data.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i += 1) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
          }

          resolve(new Blob(byteArrays, { type: mimeType }));
        } else {
          const view = new Uint8Array(data.length);

          for (let i = 0; i < data.length; i += 1) {
            view[i] = data.charCodeAt(i);
          }
          resolve(view.buffer);
        }
      } else {
        if (uri.startsWith('./'))uri = uri.substring(2);
        import(`/sources/game/import/gltf/${decodeURI(this.asset.scope)}/${decodeURI(uri)}`).then(
          (file) => {
            if (uri.match(/(.*).(bin)$/)) {
              callBinary(file.default).then((response) => {
                resolve(response);
              });
            } else if (uri.match(/(.*).(png|jpeg|jpg|gif)$/)) {
              callImage(file.default).then((response) => {
                resolve(response);
              });
            } else {
              callUri(file.default).then((response) => {
                resolve(response);
              });
            }
          },
        );
      }
    });
  }

  loadBuffer(gltf, index) {
    return new Promise((resolve) => {
      const bufferDef = gltf.buffers[index];

      // If GLB file
  		if (bufferDef.uri === undefined && index === 0) {
  			resolve(gltf.glbBody);
  		} else {
        this.loadUri(gltf, bufferDef.uri, { byteLength: bufferDef.byteLength }).then((buffer) => {
          resolve(buffer);
        });
      }
    });
  }

  loadBufferView(gltf, index) {
    return new Promise((resolve) => {
      const bufferViewRef = gltf.bufferViews[index];
      this.loadBuffer(gltf, bufferViewRef.buffer).then((buffer) => {
        const byteLength = bufferViewRef.byteLength || 0;
        const byteOffset = bufferViewRef.byteOffset || 0;
        const bufferView = buffer.slice(byteOffset, byteOffset + byteLength);
        resolve(bufferView);
      });
    });
  }

  loadMatrix4(matrix) {
    return m4.copy(matrix);
  }

  loadVec2(matrix) {
    return matrix;
  }

  loadVec3(matrix) {
    return matrix;
  }

  loadVec4(matrix) {
    return matrix;
  }

  loadSampler(gltf, samplerIndex) {
    return gltf.samplers[samplerIndex];
  }

  loadChannel(gltf, animIndex, channelIndex) {
    return new Promise((resolve) => {
      const channelRef = gltf.animations[animIndex].channels[channelIndex];
      const promises = [];
      const channel = { target: {} };
      promises.push(this.loadAnimationSampler(gltf, animIndex, channelRef.sampler).then((sampler) => {
        channel.sampler = sampler;
      }));
      promises.push(this.loadNode(gltf, channelRef.target.node).then((node) => {
        channel.target.node = node;
      }));
      channel.target.path = channelRef.target.path;

      Promise.all(promises).then(() => {
        resolve(channel);
      });
    });
  }

  loadAnimationSampler(gltf, animIndex, samplerIndex) {
    return new Promise((resolve) => {
      const samplerRef = gltf.animations[animIndex].samplers[samplerIndex];
      const promises = [];
      const sampler = {};

      promises.push(this.loadAccessors(gltf, samplerRef.input).then((accessor) => {
        sampler.input = accessor;
      }));

      promises.push(this.loadAccessors(gltf, samplerRef.output).then((accessor) => {
        sampler.output = accessor;
      }));

      sampler.interpolation = samplerRef.interpolation ? samplerRef.interpolation : 'LINEAR';

      Promise.all(promises).then(() => {
        sampler.valueSize = sampler.output.count / sampler.input.count;
        resolve(sampler);
      });
    });
  }

  loadCamera(gltf, index) {
    return gltf.cameras[index];
  }

  loadSkin(gltf, index) {
    return new Promise((resolve) => {
      const skinRef = gltf.skins[index];
      const skin = { joints: [], jointMatrices: [] };
      const promises = [];

      skin.inverseBindMatrices = [];
      skinRef.joints.forEach((joint, i) => {
        promises.push(this.loadNode(gltf, joint).then((node) => {
          node.isJoint = true;
          node.indexJoint = joint;
          skin.jointMatrices[i] = m4.identity();
          skin.joints[i] = node;
        }));
      });

      if (skinRef.skeleton) {
        promises.push(this.loadNode(gltf, skinRef.skeleton).then((node) => {
          node.isSkeleton = true;
          skin.skeleton = node;
        }));
      }

      if (skinRef.inverseBindMatrices) {
        promises.push(this.loadAccessors(gltf, skinRef.inverseBindMatrices).then((accessor) => {
          skin.inverseBindMatrices = [];
          for (let i = 0; i < accessor.count; i += 1) {
            skin.inverseBindMatrices.push(accessor.array.slice(accessor.offset + i * accessor.itemSize, accessor.offset + i * accessor.itemSize + accessor.itemSize));
          }
        }));
      } else {
        for (let i = 0; i < skinRef.joints.length; i += 1) {
          skin.inverseBindMatrices.push(m4.identity());
        }
      }

      if (skinRef.name) {
        skin.name = skinRef.name;
      }
      if (skinRef.extensions) {
        skin.extensions = skinRef.extensions;
      }
      if (skinRef.extras) {
        skin.extras = skinRef.extras;
      }
      Promise.all(promises).then((values) => {
        resolve(skin);
      });
    });
  }

  loadImage(gltf, index) {
    return new Promise((resolve) => {
      const imageRef = gltf.images[index];
      let image = {};
      const promises = [];

      if (imageRef.uri) {
        promises.push(new Promise((resolveImg) => {
          this.loadUri(gltf, imageRef.uri, { needBlob: true }).then((img) => {
            createImageBitmap(img, /* firefox don't support argument :
            , {
              premultiplyAlpha: 'none',
              colorSpaceConversion: 'none',
            } */).then((bitmap) => {
              image = bitmap;
              resolveImg(bitmap);
            }).catch((err) => {
              console.log(err);
            });
            /* image = img;
            resolveImg(img); */
          /*  createImageBitmap(img).then((bitmapImg) => {
              image = bitmapImg;
              resolveImg(image);
            }); */
          });
        }));
      } else {
        promises.push(this.loadBufferView(gltf, index).then((bufferView) => {
          const blob = new Blob([bufferView], { type: imageRef.mimeType });
          // const sourceURI = URL.createObjectURL(blob);
          image = blob;
        }));
      }

      Promise.all(promises).then((values) => {
        resolve(image);
      });
    });
  }

  loadTexture(gltf, index) {
    return new Promise((resolve) => {
      const textureRef = gltf.textures[index];
      const texture = { uuid: generateUUID() };
      const promises = [];
      promises.push(this.loadImage(gltf, textureRef.source).then((image) => {
        texture.image = image;
      }));

      let samplerPromise = null;
      if (typeof textureRef.sampler === 'number') {
        samplerPromise = this.loadSampler(gltf, textureRef.sampler);
      } else {
        samplerPromise = new Promise((resolve) => { resolve({}); });
      }
      promises.push(samplerPromise.then((sampler) => {
        texture.magFilter = sampler.magFilter || 9729; // linearfilter
        texture.minFilter = sampler.minFilter || 9987; // LinearMipmapLinearFilter
        texture.wrapS = sampler.wrapS || 10497;// RepeatWrapping
        texture.wrapT = sampler.wrapT || 10497;// RepeatWrapping
      }));

      // material.baseColorTexture
      Promise.all(promises).then((values) => {
        resolve(texture);
      });
    });
    // search for textures
  }

  // https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#metallic-roughness-material
  loadPbrMetallicRoughness(gltf, metallicRoughness) {
    return new Promise((resolve) => {
      const promises = [];
      const materialParams = {};
  			materialParams.baseColorFactor = [1.0, 1.0, 1.0, 1.0];
  			if (Array.isArray(metallicRoughness.baseColorFactor)) {
        materialParams.baseColorFactor = metallicRoughness.baseColorFactor;
  			}

  			if (metallicRoughness.baseColorTexture !== undefined) {
        promises.push(this.loadTexture(gltf, metallicRoughness.baseColorTexture.index).then((texture) => {
          materialParams.baseColorTexture = { texture, texCoord: 0 };
          if (typeof metallicRoughness.baseColorTexture.texCoord === 'number') {
            materialParams.baseColorTexture.texCoord = metallicRoughness.baseColorTexture.texCoord;
          }
        }));
  			}

      materialParams.metalness = metallicRoughness.metallicFactor !== undefined ? metallicRoughness.metallicFactor : 1.0;
      materialParams.roughness = metallicRoughness.roughnessFactor !== undefined ? metallicRoughness.roughnessFactor : 1.0;

  			if (metallicRoughness.metallicRoughnessTexture !== undefined) {
        promises.push(this.loadTexture(gltf, metallicRoughness.metallicRoughnessTexture.index).then((texture) => {
          // replace by async loadAccessors

          materialParams.metallicRoughnessTexture = { texture, texCoord: 0 };
          if (typeof metallicRoughness.metallicRoughnessTexture.texCoord === 'number') {
            materialParams.metallicRoughnessTexture.texCoord = metallicRoughness.metallicRoughnessTexture.texCoord;
          }
        }));
  			}

      Promise.all(promises).then(() => {
        resolve(materialParams);
      });
    });
  }

  loadDefaultMaterial() {
    return new Promise((resolve) => {
      resolve({
        name: 'defaultMaterial',
        emissiveFactor: [0, 0, 0],
        alphaMode: 'OPAQUE',
        alphaCutoff: 0.5,
        doubleSided: false,
        metallicRoughness: {
    			baseColorFactor: [1.0, 1.0, 1.0, 1.0],
    	    metalness: 1.0,
          roughness: 1.0,
        },
      });
    });
  }

  loadMaterial(gltf, index) {
    return new Promise((resolve) => {
      const material = gltf.materials[index];
      const promises = [];
      const computeMaterial = {};
      computeMaterial.name = material.name;

      promises.push(this.loadPbrMetallicRoughness(gltf, material.pbrMetallicRoughness || {}).then((materialParams) => {
        computeMaterial.metallicRoughness = materialParams;
      }));

      if (material.normalTexture) {
        promises.push(this.loadTexture(gltf, material.normalTexture.index).then((texture) => {
          const { normalTexture } = material;
          computeMaterial.normalTextureInfo = {
            texture,
            strength: typeof normalTexture.scale === 'number' ? normalTexture.scale : 1,
            texCoord: 0,
          };

          if (typeof normalTexture.texCoord === 'number') {
            computeMaterial.normalTextureInfo.texCoord = normalTexture.texCoord;
          }

          if (normalTexture.extensions) {
            console.error('material extensions not handle');
            computeMaterial.normalTextureInfo.extensions = normalTexture.extensions;
          }
          if (normalTexture.extras) {
            console.error('material extras not handle');
            computeMaterial.normalTextureInfo.extras = normalTexture.extras;
          }
        }));
      }

      if (material.occlusionTexture) {
        const { occlusionTexture } = material;
        promises.push(this.loadTexture(gltf, material.occlusionTexture.index).then((texture) => {
          computeMaterial.occlusionTextureInfo = {
            texture,
            strength: typeof occlusionTexture.strength === 'number' ? occlusionTexture.strength : 1,
            texCoord: 0,
          };

          if (typeof occlusionTexture.texCoord === 'number') {
            computeMaterial.occlusionTextureInfo.texCoord = occlusionTexture.texCoord;
          }

          if (occlusionTexture.extensions) {
            console.error('material extensions not handle');
            computeMaterial.occlusionTextureInfo.extensions = occlusionTexture.extensions;
          }
          if (occlusionTexture.extras) {
            console.error('material extras not handle');
            computeMaterial.occlusionTextureInfo.extras = occlusionTexture.extras;
          }
        }));
      }
      if (material.emissiveTexture) {
        promises.push(this.loadTexture(gltf, material.emissiveTexture.index).then((texture) => {
          computeMaterial.emissiveTexture = { texture, texCoord: 0 };
          if (typeof material.emissiveTexture.texCoord === 'number') {
            computeMaterial.emissiveTexture.texCoord = material.emissiveTexture.texCoord;
          }
        }));
      }
      if (material.extensions) {
        console.error('material extensions not handle');
        computeMaterial.extensions = material.extensions;
      }
      if (material.extras) {
        console.error('material extras not handle');
        computeMaterial.extras = material.extras;
      }

      computeMaterial.emissiveFactor = material.emissiveFactor ? material.emissiveFactor : [0, 0, 0];

      computeMaterial.alphaMode = material.alphaMode ? material.alphaMode : 'OPAQUE';
      if (computeMaterial.alphaMode === 'MASK') {
        computeMaterial.alphaCutoff = material.alphaCutoff ? material.alphaCutoff : 0.5;
      } else {
        computeMaterial.alphaCutoff = 0;
      }

      computeMaterial.doubleSided = typeof material.doubleSided === 'boolean' ? material.doubleSided : false;
      Promise.all(promises).then(() => {
        resolve(computeMaterial);
      });
    });
  // search for textures
  }

  checkForUnknownTextureProperty(gltf, material) {
    const entries = Object.entries(material);
    for (let i = 0; i < entries.length; i += 1) {
      const [key, value] = entries[i];
      switch (key) {
        case 'name':
        case 'pbrMetallicRoughness':
        case 'normalTexture':
        case 'occlusionTexture':
        case 'emissiveTexture':
        case 'emissiveFactor':
          break;
        default:
          console.error(`material key ${key} not known`);
      }
    }
  }

  loadAttributes(gltf, attributes) {
    return new Promise((resolve) => {
      const promises = [];
      const entries = Object.entries(attributes);
      const computeAttribute = {};
      for (let i = 0; i < entries.length; i += 1) {
        const [key, value] = entries[i];
        promises.push(this.loadAccessors(gltf, value).then((bufferAttribute) => {
          computeAttribute[GLTFConst.ATTRIBUTES[key]] = bufferAttribute;
        }));
      }
      Promise.all(promises).then(() => {
        resolve(computeAttribute);
      });
    });
  }

  // https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#accessors
  loadAccessors(gltf, index) {
    return new Promise((resolve) => {
      const accessor = gltf.accessors[index];
      const bufferViewsPromises = [];

      if (accessor.bufferView !== undefined) {
        bufferViewsPromises.push(this.loadBufferView(gltf, accessor.bufferView));
      }

      if (accessor.sparse !== undefined) {
        bufferViewsPromises.push(this.loadBufferView(gltf, accessor.sparse.indices.bufferView));
        bufferViewsPromises.push(this.loadBufferView(gltf, accessor.sparse.values.bufferView));
      }

      const itemSize = GLTFConst.WEBGL_TYPE_SIZES[accessor.type];
      const TypedArray = GLTFConst.COMPONENT_TYPES[accessor.componentType];

      const elementBytes = TypedArray.BYTES_PER_ELEMENT;
      const itemBytes = elementBytes * itemSize;

      Promise.all(bufferViewsPromises).then((bufferViews) => {
        const bufferView = bufferViews[0];

        const byteOffset = accessor.byteOffset || 0;
        const byteStride = accessor.bufferView !== undefined ? gltf.bufferViews[accessor.bufferView].byteStride : undefined;

        const normalized = accessor.normalized === true;
        let array;
        let bufferAttribute = {};

        /*
        Implementation Note: JavaScript client implementations should convert JSON-parsed floating-point doubles to single precision, when componentType is 5126 (FLOAT). This could be done with Math.fround function.
        */
        if (byteStride && byteStride !== itemBytes) {
          // Each "slice" of the buffer, as defined by 'count' elements of 'byteStride' bytes, gets its own InterleavedBuffer
          // This makes sure that IBA.count reflects accessor.count properly
          const ibSlice = Math.floor(byteOffset / byteStride);
          // length
          array = new TypedArray(bufferView, ibSlice * byteStride, accessor.count * byteStride / elementBytes);
          bufferAttribute = {
            min: accessor.min,
            max: accessor.max,
            array,
            componentType: accessor.componentType,
            count: accessor.count,
            itemSize,
            normalized,
            offset: (byteOffset % byteStride),
            stride: byteStride,
            // offset: (byteOffset % byteStride) / elementBytes,
            // stride: byteStride / elementBytes,
          };
        } else {
          if (bufferView === null) {
            array = new TypedArray(accessor.count * itemSize);
          } else {
            array = new TypedArray(bufferView, byteOffset, accessor.count * itemSize);
          }
          bufferAttribute = {
            min: accessor.min,
            max: accessor.max,
            array,
            componentType: accessor.componentType,
            count: accessor.count,
            itemSize,
            offset: 0,
            normalized,
          };
        }

        if (accessor.sparse !== undefined) {
          const bufferViewIndices = bufferViews[1];
          const bufferViewValues = bufferViews[2];

          const TypedArrayIndices = GLTFConst.COMPONENT_TYPES[accessor.sparse.indices.componentType];
          const arraySparseIndices = new TypedArrayIndices(bufferViewIndices, accessor.sparse.indices.byteOffset, accessor.sparse.count);

          let arraySparseValues;
          if (bufferViewValues === null) {
            arraySparseValues = new TypedArray(accessor.sparse.count * itemSize);
          } else {
            arraySparseValues = new TypedArray(bufferViewValues, accessor.sparse.values.byteOffset, accessor.sparse.count * itemSize);
          }

          for (let i = 0; i < accessor.sparse.count; i += 1) {
            const indexBuffer = arraySparseIndices[i] * itemSize;
            const offsetValues = i * itemSize;
            for (let c = 0; c < itemSize; c += 1) {
              bufferAttribute.array[indexBuffer + c] = arraySparseValues[offsetValues + c];
            }
          }
        }

        // missing return of other attributes
        resolve(bufferAttribute);
      });
    });
  }

  loadTargets(gltf, targetsRef) {
    return new Promise((resolve) => {
      const promises = [];
      const targets = [];
      targetsRef.forEach((targetRef, i) => {
        promises.push(this.loadAttributes(gltf, targetRef).then((attributes) => {
          targets[i] = attributes;
        }));
      });
      Promise.all(promises).then(() => {
        resolve(targets);
      });
    });
  }

  loadPrimitive(gltf, primitiveRef) {
    return new Promise((resolve) => {
      const primitive = {};
      const promises = [];
      const entries = Object.entries(primitiveRef);
      for (let i = 0; i < entries.length; i += 1) {
        const [key, value] = entries[i];
        switch (key) {
          case 'attributes':
            promises.push(this.loadAttributes(gltf, value).then((attributes) => {
              primitive.attributes = attributes;
            }));
            break;
          case 'indices':
            promises.push(this.loadAccessors(gltf, value).then((indices) => {
              primitive.indices = indices;
            }));
            break;
          case 'targets':
            promises.push(this.loadTargets(gltf, value).then((targets) => {
              primitive.targets = targets;
            }));
            break;
          case 'extensions':
            console.error('extensions key not handled');
            break;
          case 'mode':
          case 'material':
            break;
          default:
            console.error(`loadPrimitive key ${key} not handled`);
        }
      }

      primitive.mode = typeof primitiveRef.mode === 'number' ? primitiveRef.mode : 4;

      if (typeof primitiveRef.material === 'number') {
        promises.push(this.loadMaterial(gltf, primitiveRef.material).then((material) => {
          primitive.material = material;
        }));
      } else {
        promises.push(this.loadDefaultMaterial().then((material) => {
          primitive.material = material;
        }));
      }
      Promise.all(promises).then(() => {
        resolve(primitive);
      });
    });
  }

  makeIndexIterator(indices) {
    let i = 0;
    const { offset, stride } = indices;
    const itemSize = 3;
    let totalOffset = 1;
    if (offset) totalOffset += offset;
    if (stride) totalOffset += stride;
    return () => {
      i += (totalOffset) * itemSize;
      return indices[i];
    };
  }

  makeUnindexedIterator(posAttributes) {
    const { offset, stride } = posAttributes;
    const itemSize = 3;
    let totalOffset = 1;
    if (offset) totalOffset += offset;
    if (stride) totalOffset += stride;
    let i = 0;
    return () => i += (totalOffset) * itemSize;
  }

  computeTangent(position, uv, indices) {
    const getNextIndex = indices ? this.makeIndexIterator(indices) : this.makeUnindexedIterator(position);
    const count = indices ? indices.count : position.count;

    const positionArray = position.array;
    const uvArray = uv.array;

    const tangents = [];
    for (let i = 0; i < count; i += 1) {
      const n1 = getNextIndex();
      const n2 = getNextIndex();
      const n3 = getNextIndex();

      const p1 = positionArray.slice(n1 * 3, n1 * 3 + 3);
      const p2 = positionArray.slice(n2 * 3, n2 * 3 + 3);
      const p3 = positionArray.slice(n3 * 3, n3 * 3 + 3);

      const uv1 = uvArray.slice(n1 * 2, n1 * 2 + 2);
      const uv2 = uvArray.slice(n2 * 2, n2 * 2 + 2);
      const uv3 = uvArray.slice(n3 * 2, n3 * 2 + 2);

      const dp12 = m4.subtractVectors(p2, p1);
      const dp13 = m4.subtractVectors(p3, p1);

      const duv12 = subtractVec2(uv2, uv1);
      const duv13 = subtractVec2(uv3, uv1);

      const f = 1.0 / (duv12[0] * duv13[1] - duv13[0] * duv12[1]);
      const tangent = Number.isFinite(f)
        ? m4.normalize(m4.scaleVector(m4.subtractVectors(
          m4.scaleVector(dp12, duv13[1]),
          m4.scaleVector(dp13, duv12[1]),
        ), f))
        : [1, 0, 0];

      tangents.push(...tangent, ...tangent, ...tangent);
      return {
        itemSize: 3, array: tangents, offset: 0, normalized: false, count: count * 3,
      };
    }

    return tangents;
  }

  computeNormal(posAttributes, indices) {
    const array = [];
    const pos = posAttributes.array;
    const count = indices ? indices.count : posAttributes.count / 3;
    const getNextIndex = indices ? this.makeIndexIterator(indices) : this.makeUnindexedIterator(posAttributes);
    for (let i = 0; i < count; i += 1) {
      let vec3Normal = [0, 0, 0];
      const offsetA = getNextIndex();
      const offsetB = getNextIndex();
      const offsetC = getNextIndex();

      const vecA = [pos[offsetA], pos[offsetA + 1], pos[offsetA + 2]];
      const vecB = [pos[offsetB], pos[offsetB + 1], pos[offsetB + 2]];
      const vecC = [pos[offsetC], pos[offsetC + 1], pos[offsetC + 2]];

      vec3Normal = sumVectors3(vec3Normal, crossVectors3(subVectors3(vecA, vecB), subVectors3(vecC, vecB)));
      array.push(...normalizeVec3(vec3Normal));
      array.push(...normalizeVec3(vec3Normal));
      array.push(...normalizeVec3(vec3Normal));
    }
    return {
      itemSize: 3, array, offset: 0, normalized: false, count,
    };
  }

  loadMesh(gltf, index) {
    return new Promise((resolve) => {
      const promises = [];
      const gltfMesh = gltf.meshes[index];
      const geometries = {};

      const entries = Object.entries(gltfMesh);
      for (let i = 0; i < entries.length; i += 1) {
        const [key, value] = entries[i];
        switch (key) {
          case 'primitives':
            if (!geometries.meshes)geometries.meshes = [];
            value.forEach((primitive, i) => {
              promises.push(this.loadPrimitive(gltf, primitive).then((mesh) => {
                if (mesh.attributes && !mesh.attributes.normal) {
                  mesh.flatNormal = true;
                  mesh.attributes.normal = this.computeNormal(mesh.attributes.position, mesh.indices);
                }
                if (mesh.attributes && !mesh.attributes.tangent && mesh.attributes.uv && !mesh.flatNormal) {
                  //    mesh.attributes.tangent = this.computeTangent(mesh.attributes.position, mesh.attributes.uv, mesh.indices);
                }
                geometries.meshes.push(mesh);
              }));
            });
            break;
          case 'weights':
            geometries.weights = value;
            break;
          case 'name':
            geometries.name = value;
            break;
          default:
            console.error(`loadMesh key ${key} not handled`);
        }
      }
      Promise.all(promises).then(() => {
        resolve(geometries);
      });
    });
  }

  createMesh(mesh) {
    return {
      uuid: generateUUID(),
      type: 'mesh',
      matrix: m4.identity(),
      matrixWorld: m4.identity(),
      mesh,
    };
  }

  loadNode(gltf, index) {
    return new Promise((resolve) => {
      const promises = [];
      const node = { type: 'group', updateMatrix: true, uuid: generateUUID() };
      const gltfNode = gltf.nodes[index];

      const entries = Object.entries(gltfNode);

      for (let i = 0; i < entries.length; i += 1) {
        const [key, value] = entries[i];
        switch (key) {
          case 'name':
            node.name = value;
            break;
          case 'children':
            if (!node.children)node.children = [];
            value.forEach((nodeIndex) => {
              promises.push(this.loadNode(gltf, nodeIndex).then((childNode) => {
                node.children.push(childNode);
                childNode.parent = node;
              }));
            });
            break;
          case 'matrix':
            node.matrix = this.loadMatrix4(value);
            break;
          case 'mesh':
            promises.push(this.loadMesh(gltf, value).then((geometries) => {
              if (geometries.meshes.length === 1) {
                node.mesh = geometries.meshes[0];
                node.weights = geometries.weights;
                node.name = geometries.name;
                node.type = 'mesh';
                this.meshNodes[node.uuid] = node;
              } else {
                node.type = 'group';
                if (geometries.meshes && geometries.meshes.length > 0 && !node.children) {
                  node.children = [];
                }
                geometries.meshes.forEach((mesh) => {
                  const childNode = this.createMesh(mesh);
                  childNode.weights = geometries.weights;
                  node.weights = geometries.weights;
                  node.children.push(childNode);
                  childNode.parent = node;
                  this.meshNodes[childNode.uuid] = childNode;
                });
              }
            }));

            break;
          case 'camera':
            node.camera = this.loadCamera(gltf, value);
            this.cameraNodes[node.uuid] = node;
            break;
          case 'rotation':
            node.isTRS = true;
            node.rotation = this.loadVec4(value);
            break;
          case 'translation':
            node.isTRS = true;
            node.translation = this.loadVec3(value);
            break;
          case 'scale':
            node.isTRS = true;
            node.scale = this.loadVec3(value);
            break;
          case 'weights':
            node.weights = this.loadVec2(value);
            break;
          case 'skin':
            promises.push(this.loadSkin(gltf, value).then((skin) => {
              node.skin = skin;
              this.skinnedMeshNodes[node.uuid] = node;
            }));
            break;
          case 'extensions':
            console.error('extensions key not handled');
            break;
          default:
            console.error(`loadNode key ${key} not handled`);
        }
      }

      if (node.isTRS) {
        if (!node.scale) {
          node.scale = [1, 1, 1];
        }
        if (!node.rotation) {
          node.rotation = [0, 0, 0, 1];
        }

        if (!node.translation) {
          node.translation = [0, 0, 0];
        }

        node.matrix = m4.compose(node.translation, node.rotation, node.scale);
      } else {
        if (!node.matrix && !node.isTRS) {
          node.matrix = m4.identity();
        }
        node.scale = [1, 1, 1];
        node.rotation = [0, 0, 0, 1];
        node.translation = [0, 0, 0];
        m4.decompose(node.matrix, node.translation, node.rotation, node.scale);
      }
      node.matrixWorld = m4.copy(node.matrix);
      Promise.all(promises).then(() => {
        resolve(node);
      });
    });
  }

  loadScene(gltf, index) {
    return new Promise((resolve) => {
      const gltfScene = gltf.scenes[index];
      const scene = {
        type: 'scene', children: [], matrix: m4.identity(), matrixWorld: m4.identity(), updateMatrix: true,
      };
      const entries = Object.entries(gltfScene);
      const promises = [];
      for (let i = 0; i < entries.length; i += 1) {
        const [key, value] = entries[i];

        switch (key) {
          case 'name':
            scene.name = value;
            break;
          case 'nodes':
            value.forEach((nodeIndex) => {
              promises.push(this.loadNode(gltf, nodeIndex).then((childNode) => {
                scene.children.push(childNode);
                childNode.parent = scene;
              }));
            });

            break;
          default:
            console.error(`loadScene key ${key} not handled`);
        }
      }
      Promise.all(promises).then(() => {
        resolve(scene);
      });
    });
  }

  loadAnimation(gltf, animIndex) {
    return new Promise((resolve) => {
      const animationRef = gltf.animations[animIndex];
      const anim = { channels: [], samplers: [], maxTime: 0 };
      const entries = Object.entries(animationRef);
      const promises = [];
      for (let e = 0; e < entries.length; e += 1) {
        const [key, value] = entries[e];

        switch (key) {
          case 'name':
            anim.name = value;
            break;
          case 'channels':
            for (let i = 0; i < value.length; i += 1) {
              promises.push(this.loadChannel(gltf, animIndex, i).then((channel) => {
                anim.channels.push(channel);
              }));
            }
            break;

          case 'samplers':
            for (let i = 0; i < value.length; i += 1) {
              promises.push(this.loadAnimationSampler(gltf, animIndex, i).then((sampler) => {
                anim.samplers.push(sampler);
                if (anim.maxTime < sampler.input.max[0])anim.maxTime = sampler.input.max[0];
              }));
            }
            break;
          default:
            console.error(`loadAnimations key ${key} not handled`);
        }
      }
      Promise.all(promises).then(() => {
        resolve(anim);
      });
    });
  }

  loadAnimations(gltf) {
    return new Promise((resolve) => {
      const gltfAnimations = gltf.animations;
      const animations = [];
      if (gltfAnimations) {
        const animationPromises = [];
        for (let i = 0; i < gltfAnimations.length; i += 1) {
          animationPromises.push(this.loadAnimation(gltf, i).then((value) => {
            animations.push(value);
          }));
        }
        Promise.all(animationPromises).then(() => {
          resolve(animations);
        });
      } else {
        resolve(null);
      }
    });
  }
  /*

https://github.com/KhronosGroup/glTF/blob/master/extensions/README.md

KHR_draco_mesh_compression
KHR_lights_punctual
KHR_materials_clearcoat
KHR_materials_ior
KHR_materials_pbrSpecularGlossiness
KHR_materials_sheen
KHR_materials_specular
KHR_materials_transmission
KHR_materials_unlit
KHR_materials_variants
KHR_materials_volume
KHR_mesh_quantization
KHR_texture_basisu
KHR_texture_transform
KHR_xmp

*/

  load(asset, gltf) {
    return new Promise((resolve) => {
      const gltfImport = {};
      const promises = [];
      this.asset = asset;

      if (gltf.extensionsUsed) {
        console.table(gltf.extensionsUsed);
      }

      if (gltf.extensionsRequired) {
        console.table(gltf.extensionsRequired);
      }

      let sceneIndex = 0;
      if (typeof gltf.scene === 'number') {
        sceneIndex = gltf.scene;
      }

      promises.push(this.loadScene(gltf, sceneIndex).then((value) => {
        gltfImport.scene = value;
        gltfImport.cameraNodes = this.cameraNodes;
        gltfImport.skinnedMeshNodes = this.skinnedMeshNodes;
        gltfImport.meshNodes = this.meshNodes;
      }));

      promises.push(this.loadAnimations(gltf).then((value) => {
        if (value) {
          gltfImport.animations = value;
        }
      }));

      Promise.all(promises).then((values) => {
        resolve(gltfImport);
      });
    });
  }
}

export { GLTFLoader };
