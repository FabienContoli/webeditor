import Camera from "./../models/camera/Camera";
import Node from "./../models/Node";
import FileManager from "./FileManager"
import LevelLoader from "./LevelLoader";

export default class GameLoader {

    /** 
     * @type {FileManager}
     */
     fileManager;

    constructor({root}){
        this.fileManager= new FileManager()
        this.root=root;
        this.gameDefinition=null
        this.levelLoader=new LevelLoader(this.fileManager)
    }
    
    load =function(gameState){
        return new Promise(async (resolve)=>{     
            await this.fileManager.downloadRootFile(this.root)
            this.gameDefinition= await this.fileManager.downloadGameFile();
            this.levelDefinition= await this.fileManager.downloadLevelFile(this.getDefaultLevelName());
            let level=await this.levelLoader.load(JSON.parse(JSON.stringify(this.levelDefinition)))
                
            if(typeof level.camera ==="number"){
                if(!level.cameras[level.camera]){
                    const defaultCamera=new Camera({name:"defaultCamera",type:"perspective"})
                    const node= new Node();
                    node.camera=defaultCamera
                    defaultCamera.node=node
                    level.cameras.set(defaultCamera.uuid,defaultCamera)
                    level.camera=defaultCamera
                    level.scene.nodes.push(node)
                }
            }

            resolve({game:this.gameDefinition,level,state:gameState})
        })
    }

    getDefaultLevelName(){
        return typeof this.gameDefinition.level==='number'||this.gameDefinition.levels[0]
    }

}