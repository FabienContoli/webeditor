
import * as commonScript from "./../common/scripts"
import LevelLoader from './LevelLoader';
import * as commonSystems from "./../common/systems"

export default class FileManager{

    files;
    
    constructor(){
        this.files={}
    }

    setFileContent(id,content){
        this.files[id].content=content;
    }
    
    async downloadRootFile(name){
        let response = await this._fetchFile({id:name,name:name+".json"})
        this.files = response.reduce((pre,cur)=>{
            pre[cur.id]=cur
            return pre;
        },{}) 
    }

    async downloadGameFile(){
        let file= this.getFileByPath("/game.json")
        await this.downloadFile(file)
        return JSON.parse(JSON.stringify(file.content))
    }

    async downloadLevelFile(levelName){
        let file= this.getFileByPath("/levels/"+levelName+".json")
        await  this.downloadFile(file)
        return JSON.parse(JSON.stringify(file.content))
    }

    async downloadAllFiles(){
        this.files.values((file)=>{
            let fileContent =this.downloadFile(file)
        })
    }
    
    async downloadFile(file) {
        if(file.content){
            return file.content
        }
        const response = await this._fetchFile(file)
        this.setFileContent(file.id,response)
        return file
    }

    async _fetchFile(file){
        let extension=  file.name.match(/[^\.]+$/)[0].toLowerCase();
        let path ="./gameFiles/"+file.id+"."+extension
        let response = await fetch(path,{headers: { 'Content-Type': file.mimeType}})
        if(extension==="gltf"|| extension==="json"){
            response=await response.json();
        }else if(extension==="bin"){
            response=await response.arrayBuffer()

        }else if(extension==="jpg"||extension==="png"){
            let arrayBuffer = await response.arrayBuffer()
            const dataView = new DataView(arrayBuffer);
            response = new Blob([dataView], { type: 'image/png' });
        }
        return response;
        
    }

    getFileByPath(path){
        return Object.values(this.files).find((file) => file.path === path)
    }

    async loadRef(ref){
        if(ref.type==="common"){
            if(commonSystems[ref.uri]){
                return commonSystems[ref.uri]
            }
            return commonScript[ref.uri]
        }else{
            const file =this.getFileByPath(ref.uri)
            if(!file){
            console.error("No file for the ref",ref.uri)
            }
            
            let extension=  ref.uri.match(/[^\.]+$/)[0].toLowerCase();
            let fileFolder= ref.uri.substring(0, ref.uri.lastIndexOf('/'))
            
            if(!file.content){
                let fileDownloaded =await this.downloadFile(file)
                file.content =await fileDownloaded.content
            }

            if(extension==="gltf"){
                let json = JSON.parse(JSON.stringify(file.content));
                let levelInstance=await new LevelLoader(this).load(json,{filePath:fileFolder})
                return levelInstance.scene.nodes
            }

            return file.content 
        }
    }
}