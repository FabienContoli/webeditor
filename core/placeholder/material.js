import Material from "./../models/mesh/material/Material";
let material = null;

const getDefaultMaterial=function(){
    if(material)return material;
    material =new Material();
    return material;
}

export {getDefaultMaterial};

