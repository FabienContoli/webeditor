import Primitive from "./Primitive";

/**
 *  Represent a Mesh blueprint
 *  @class
 */
export default class Mesh {

   /**
    * name
    * @type {string}
    * @public
    */
   name;

   /**
    * An array of primitives, each defining geometry to be rendered.
    * @type Array<Primitive>
    * @required
    * @public
    */
   primitives;

   /**
    * 	
    * Array of weights to be applied to the morph targets. The number of array elements MUST match the number of morph targets.
    * @type Array<number>
    * @public
    */
   weights;

   constructor({ name, primitives, weights } = {}) {
      if (name) {
         this.name = name
      }
      if (Array.isArray(primitives)) {
         this.primitives = []
         primitives.forEach(primitive => {
            this.primitives.push(new Primitive(primitive));
         });
      }
      if (weights) {
         this.weights = weights
      }
   }
}
