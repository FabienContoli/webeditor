/**
 *  Represent a Buffer blueprint
 *  @class
 */
 export default class Buffer {
    
    /**
     * name
     * @type {string}
     * @public
     */
    name;

    /**
     * The URI (or IRI) of the buffer.  Relative paths are relative to the current glTF asset.  Instead of referencing an external file, this field **MAY** contain a `data:`-URI.
     * @type {string}
     * @public
     */
     uri;
    
    /**
     * 	
     * The length of the buffer in bytes.
     * @type {number}
     * @public
     */
     byteLength;

}
