import Material from "./material/Material";

/**
 *  Represent a Primitive blueprint
 *  @class
 */
 export default class Primitive {
    
    /**
     * name
     * @type {string}
     * @public
     */
    name;

    /**
     * render mode number
     * @type number
     * @public
     */
    mode;
    
    /**
     * index of indices
     * @type number
     * @public
     */
    indices;

    
    /**
     * attributes index
     * @type Object
     * @public
     */
     attributes;

    /**
     * material index
     * @type number
     * @public
     */
    material;

    /**
     * targets index
     * @type Array<Target>
     * @public
     */
    targets;

    /**
     * extensions list
     * @type Array<String>
     * @public
     */
    extensions;

    constructor({name,mode=4,indices,attributes,material,targets,extensions}={}){
        if(name){
            this.name=name
        }
        this.mode=mode

        if(typeof indices==="number"){
            this.indices=indices
         }
         if(typeof material==="number"){
            this.material=new Material()
         }

        this.attributes={}
        if(attributes){
            Object.entries(attributes).forEach(([key,value])=>{
                this.attributes[key]=value
            })
        }

        if(targets){
            throw "target not handle"
        }

        if(extensions){
            console.log(extensions)
        }
    }
}
