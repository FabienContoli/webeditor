/**
 *  Represent a Sparse accessor blueprint
 *  @class
 */
 export default class SparseAccessor {

    /**
    * The number of elements referenced by this accessor.
    * @type {number}
    */
    count;

    
    /**
    * "An object pointing to a buffer view containing the indices of deviating accessor values. The number of indices is equal to `count`. Indices **MUST** strictly increase.
    * @type {number}
    */
     indices;

     /**
    * "An object pointing to a buffer view containing the indices of deviating accessor values. The number of indices is equal to `count`. Indices **MUST** strictly increase.
    * @type {number}
    */
     values
 }
