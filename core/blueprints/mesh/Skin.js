/**
 *  Represent a Skin blueprint
 *  @class
 */
 export default class Skin {
    
    /**
     * The skin name.
     * @type {string}
     * @public
     */
    name;

   /**
     * The index of the accessor containing the floating-point 4x4 inverse-bind matrices. Its `accessor.count` property **MUST** be greater than or equal to the number of elements of the `joints` array. When undefined, each matrix is a 4x4 identity matrix.
     * @type {number}
     * @public
     */
   inverseBindMatrices;

   /**
     * The index of the node used as a skeleton root. The node **MUST** be the closest common root of the joints hierarchy or a direct or indirect parent node of the closest common root.
     * @type {number}
     * @public
     */
   skeleton;

   /**
     *  Indices of skeleton nodes, used as joints in this skin.,
     * @type {Array<number>}
     * @public
     */
   joints;

}
