/**
 *  Represent a Texture blueprint
 *  A texture and its sampler.
 *  @class
 */
 export default class Texture {

    
    /**
    * The texture name
    * @type {number}
    */
     name;

    /**
    * The index of the sampler used by this texture. When undefined, a sampler with repeat wrapping and auto filtering **SHOULD** be used.
    * @type {number}
    */
    sampler;

    
    /**
    * The index of the image used by this texture. When undefined, an extension or other mechanism **SHOULD** supply an alternate texture source, otherwise behavior is undefined.
    * @type {number}
    */
     source;

 }
