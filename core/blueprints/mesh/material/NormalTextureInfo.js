import TextureInfo from "./TextureInfo";

/**
 *  Represent a Texture blueprint
 *  A texture and its sampler.
 *  @class
 */
 export default class NormalTextureInfo extends TextureInfo{

    /**
    * The scalar parameter applied to each normal vector of the texture. This value scales the normal vector in X and Y directions using the formula: `scaledNormal =  normalize((<sampled normal texture value> * 2.0 - 1.0) * vec3(<normal scale>, <normal scale>, 1.0))`.
    * @type {number}
    */
    scale;

 }
