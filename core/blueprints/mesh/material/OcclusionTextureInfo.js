import TextureInfo from "./TextureInfo";
/**
 *  Represent a Occlusion Texture info blueprint
 *  A texture and its sampler.
 *  @class
 */
 export default class OcclusionTextureInfo  extends TextureInfo {

    
    /**
    * A scalar parameter controlling the amount of occlusion applied. A value of `0.0` means no occlusion. A value of `1.0` means full occlusion. This value affects the final occlusion value as: `1.0 + strength * (<sampled occlusion texture value> - 1.0)`.
    * @type {number}
    */
     strength;

 }
