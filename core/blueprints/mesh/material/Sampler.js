

/**
 *  Represent a Sampler blueprint
 *  @class
 */
 export default class Sampler {
    
    /**
    * The Sampler name
    * @type {number}
    */
     name;

    /**
    * Magnification filter.
    * `samplerParameteri()` with pname equal to TEXTURE_MAG_FILTER
    * @type {number}
    */
     magFilter;

    /**
    * Minification filter.
    * `samplerParameteri()` with pname equal to TEXTURE_MIN_FILTER
    * @type {number}
    */
    minFilter;

    /**
    * S (U) wrapping mode.
    * `samplerParameteri()` with pname equal to TEXTURE_WRAP_S
    * @type {number}
    */
    wrapS;

    /**
    * T (V) wrapping mode.
    * `samplerParameteri()` with pname equal to TEXTURE_WRAP_T
    * @type {number}
    */
    wrapT;

 }