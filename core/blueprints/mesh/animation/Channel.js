import AnimationChannelTarget from "./AnimationChannelTarget"
/**
 *  Represent a Channel blueprint
 *  @class
 */
 export default class Channel {
    
    /**
     * The index of a sampler in this animation used to compute the value for the target, e.g., a node's translation, rotation, or scale (TRS).
     * @type {number}
     * @public
     */
     sampler;

    /**
     * The descriptor of the animated property.
     * @type {AnimationChannelTarget}
     * @public
     */
     target;
}
