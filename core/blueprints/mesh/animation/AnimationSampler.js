/**
 *  Represent an Animation Sampler blueprint
 *  @class
 */
 export default class AnimationSampler {
    
    /**
     * The index of an accessor containing keyframe timestamps.
     * @type {number}
     * @public
     */
    input;

    /**
     * Interpolation algorithm.
     * @type {string}
     * @default "LINEAR"
     * @public
     */
    interpolation;
    
    /**
     * The index of an accessor, containing keyframe output values.
     * @type {number}
     * @public
     */
     output;

}
