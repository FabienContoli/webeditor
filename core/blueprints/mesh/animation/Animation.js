import AnimationSampler from "./AnimationSampler"
import Channel from "./Channel"

/**
 *  Represent a Animation blueprint
 *  A keyframe animation.
 *  @class
 */
 export default class Animation {
    
    /**
     * name
     * @type {string}
     * @public
     */
    name;

    /**
     * An array of animation channels. An animation channel combines an animation sampler with a target property being animated. Different channels of the same animation **MUST NOT** have the same targets.
     * @type {Array<Channel>}
     * @public
     */
     channels;
    
    /**
     * 	
     * An array of animation samplers. An animation sampler combines timestamps with a sequence of output values and defines an interpolation algorithm.
     * @type {Array<AnimationSampler>}
     * @public
     */
     samplers;

}
