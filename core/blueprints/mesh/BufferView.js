/**
 *  Represent a BufferView blueprint
 *  @class
 */
 export default class BufferView {
    
    /**
     * name
     * @type {string}
     * @public
     */
    name;

    /**
     * "The index of the buffer."
     * @type {number}
     * @public
     */
     buffer;
    
       
    /**
     * 	
     * The offset into the buffer in bytes.
     * @type {number}
     * @public
     */
     byteOffset;

    /**
     * 	
     * The length of the buffer in bytes.
     * @type {number}
     * @public
     */
     byteLength;

    /**
     * The stride, in bytes, between vertex attributes.  When this is not defined, data is tightly packed. When two or more accessors use the same buffer view, this field **MUST** be defined.
     * vertexAttribPointer()` stride parameter
     * @type {number}
     * @public
     */
    byteStride;


    /**
     * 	
     * The hint representing the intended GPU buffer type to use with this buffer view.
     * used for bindBuffer()
     * @type {number}
     * @public
     */
    target;
    
}
