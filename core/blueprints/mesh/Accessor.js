
/**
 *  Represent a Accessor blueprint
 *  @class
 */
export default class Accessor {

    /**
    * The name of the accessor.
    * @type {number}
    * @public
    */
    name;

    /**
    * The index of the buffer view.
    * @type {number}
    * @public
    */
    bufferView;

    /**
    * The offset relative to the start of the buffer view in bytes.
    * @type {number}
    * @public
    */
    byteOffset;

    /**
     * The datatype of the accessor's components.
     * @type {number}
     * @public
     */
    componentType;

    /**
     * Specifies whether integer data values are normalized before usage.
     * @type {boolean}
     * @default false
     */
    normalized;

    /**
    * The number of elements referenced by this accessor.
    * @type {number}
    */
    count;

    /**
     * Maximum value of each component in this accessor.
     * @type {Array<number>}
     */
    max;

    /**
   * Minimum value of each component in this accessor.
   * @type {Array<number>}
   */
    min;

    /**
   * Minimum value of each component in this accessor.
   * @type {Array<number>}
   */
    sparse

}