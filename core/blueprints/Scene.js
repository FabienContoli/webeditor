
/**
 *  Represent a Scene blueprint
 *  @class
 */
export default class Scene {
    
    /**
     * name of the scene
     * @type {string}
     * @public
     */
    name;

    /**
     * node indexes
     * @type Array<number>
     * @public
     */
    nodes;
    

    /**
     * @required
     * @param {string} [name] 
     * @param {Array<number>} [nodes] 
     */
    constructor({name,nodes}={}) {
        this.name=name;
        this.nodes=nodes;
    }
}
