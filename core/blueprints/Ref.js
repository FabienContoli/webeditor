/**
 *  Represent a Ref blueprint
 *  @class
 */
export default class Ref {
    

    /**
     * index
     * @type {number}
     * @public
     */
    index;
    
    /** 
     * type common or distant file
     * @type {string}
     */
    type;

    /**
     * ref creation index
     * @type {string}
     * @public
     */
     uri;
    
    /**
     * @required
     * @param {object} [object]
     * @param {number} [object.index]
     * @param {string} [object.type]
     * @param {string} [object.uri]
     * TODO: add element to constructor and use it in the editor 
     */
    constructor({index,type,uri}={}) {
        
        if(typeof index ==="number")this.index=index
        if(type)this.type=type
        if(uri)this.uri=uri


    }
}
