import Ref from "./Ref";

/**
 *  Represent a Ref blueprint
 *  @class
 */
 export default class Script {
    

    /**
     * name
     * @type {string}
     * @public
     */
    name;
    
    /**
     * ref
     * @type {Ref}
     * @public
     */
     ref;
    
    /**
     * @required
     * @param {object} [object]
     * @param {number} [object.name]
     * @param {Ref} [object.ref]
     * TODO: add element to constructor and use it in the editor 
     */
    constructor({name,ref}) {
        
        if(name)this.name=name
        if(ref)this.ref=new Ref(ref)


    }
}
