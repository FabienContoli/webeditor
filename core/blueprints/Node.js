import * as m4 from "./../maths/m4"
/**
 *  Represent a Node blueprint
 *  @class
 */
export default class Node {
    
    /**
     * name
     * @type {string}
     * @public
     */
    name;
    
    /**
     * child node indexes
     * @type Array<number>
     * @public
     */
    children;
    
    /**
     * mesh index
     * @type number
     * @public
     */
     mesh;

    /**
     * camera index
     * @type number
     * @public
     */
    camera;


    /**
    * node local matrix
    * @type Float32Array
    * @public
    */
    matrix;

    /**
    * rotation Matrix
    * @type [number, number, number, number]
    * @public 
    */
    rotation;

    /**
    * translation Matrix
    * @type [number, number, number]
    * @public 
    */
    translation;

    /**
    * scale Matrix
    * @type [number, number, number]
    * @public 
    */
    scale;

    /**
    * scale Matrix
    * @type [number, number, number]
    * @public 
    */
    weights;

    /**
    * skin index
    * @type number
    * @public 
    */
    skin;

    /**
    * extensions list
    * @type Array<string>
    * @public 
    */
    extensions

    /**
     * @required
     * @param {object} [object]
     * @param {string} [object.name]
     * @param {array} [object.children]
     * @param {array} [object.matrix]
     * @param {number} [object.mesh]
     * @param {number} [object.camera]
     * TODO: add element to constructor and use it in the editor 
     */
    constructor({name,children,matrix,mesh,camera,translation,rotation,scale}={}) {
        this.name=name;

        this.children=children??[]
        
        if(matrix){
            this.matrix=matrix
        }else if(translation||rotation||scale){
            if (scale) {
                this.scale =scale
            }

            if (rotation) {
                this.rotation = rotation 
            }
    
            if (translation) {
                this.translation = translation
            }
        }
        
        this.mesh=mesh

        this.camera=camera
    }

    /**
     * @required
     * @param {Number} nodeIndex
     */
    addChildNode(nodeIndex){
        if(this.children.indexOf(nodeIndex)!==-1)
        this.children.push(nodeIndex)
    }
}
