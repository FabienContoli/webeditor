/**
 *  Represent a Input blueprint
 *  @class
 *  
 */
 export default class Input {
    /**
     * controller type
     * @type {("keyboard"|"gamepad"|"mouse"|"touch")}
     * @public
     */
    controller;

    /**
     * @type {string}
     * @readonly
     */
    action

    /**
     * @type {string}
     * @readonly
     */
    input
 
     /**
      * @type {string}
      * @readonly
     */
     value
 
     /**
     * @type {object}
     * @readonly
     */
     event

    constructor({controller,action,input,value,event}){
        this.controller=controller
        if(action)this.action=action
        if(input)this.input=input
        if(value)this.value=value
        if(event)this.event=event
    }
}