/**
 *  Represent a Asset gltf blueprint
 *  @class
 */
 export default class Asset {

    /**
    * A copyright message suitable for display to credit the content creator.
    * @type {string}
    */
    copyright;

     /**
    * Tool that generated this glTF model.  Useful for debugging.
    * @type {string}
    */
    generator;

     /**
    * The glTF version in the form of `<major>.<minor>` that this asset targets.
    * @type {string}
    */
    version;

     /**
    * The minimum glTF version in the form of `<major>.<minor>` that this asset targets. This property **MUST NOT** be greater than the asset version.
    * @type {string}
    */
    minVersion;

}
