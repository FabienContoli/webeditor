import Accessor from "../mesh/Accessor";
import Buffer from "../mesh/Buffer";
import BufferView from "../mesh/BufferView";
import Animation from "../mesh/animation/Animation";
import Texture from "../mesh/material/Texture";
import Asset from "./Asset";
import Camera from "../camera/Camera";
import Material from "../mesh/material/Material";
import Mesh from "../mesh/Mesh";
import Node from "../Node";
import Sampler from "../mesh/animation/AnimationSampler";
import Scene from "../Scene";
import Skin from "../Mesh/Skin";
import Extension from "./Extension";

/**
 *  Represent a Gltf blueprint
 *  @class
 */
export default class Gltf {
    
    /**
     * gltf name
     * @type {string}
     * @public
     */
    name;

    /**
    *  Names of glTF extensions used in this asset.
    * @type {Array<string>}
    */
    extensionsUsed;

    /**
    * Names of glTF extensions required to properly load this asset.
    * @type {Array<string>}
    */
    extensionsRequired;

    /**
    * An array of accessors.
    * @type {Array<Accessor>}
    */
    accessors;

    /**
    * An array of keyframe animations.
    * @type {Array<Animation>}
    */
    animations;

    /**
    * Metadata about the glTF asset.
    * @type {Array<Asset>}
    */
    asset

    /**
    * An array of buffers.
    * @type {Array<Buffer>}
    */
    buffers;

    /**
    * An array of bufferViews.
    * @type {Array<BufferView>}
    */
    bufferViews;

    /**
    * An array of cameras.  A camera defines a projection matrix.
    * @type {Array<Camera>}
    */
    cameras;

    /**
    * An array of images.  An image defines data used to create a texture.
    * @type {Array<Image>}
    */
    images;

    /**
    * An array of materials.  A material defines the appearance of a primitive.
    * @type {Array<Material>}
    */
    materials;

    /**
    * An array of meshes.  A mesh is a set of primitives to be rendered.
    * @type {Array<Mesh>}
    */
    meshes;

    /**
    * An array of nodes.
    * @type {Array<Node>}
    */
    nodes;

    /**
    * An array of samplers.  A sampler contains properties for texture filtering and wrapping modes.
    * @type {Array<Sampler>}
    */
    samplers;

    /**
    * The index of the default scene.  This property **MUST NOT** be defined, when `scenes` is undefined.
    * @type {number}
    */
    scene;

    /**
    * An array of scenes.
    * @type {Array<Scene>}
    */
    scenes;

    /**
    * An array of skins.  A skin is defined by joints and matrices.
    * @type {Array<Skin>}
    */
    skins;

    /**
    * An array of textures.
    * @type {Array<Texture>}
    */
    textures;

     /**
    * An array of extensions.
    * @type {Array<Extension>}
    */
    extensions;

     /**
    * An array of extras info.
    * @type {Array<string>}
    */
    extras;

    /**
     * @required
     * @param {string} [name] 
     */
    constructor(name) {
        this.name=name;
    }
}
