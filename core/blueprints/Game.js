export default class Game {

    /**
     * Level name
     * @type {string}
     * @public
     */
    name;

    /**
     * Level index
     * @type {number}
     * @public
     */
    level;

    /**
     * Levels names
     * @type Array{string}
     * @public
     */
    levels
    
    constructor(blueprint) {
        this.name = blueprint.name
        if(blueprint.level){
            this.level= blueprint.level
        }
        this.levels=[]
        if(blueprint.levels){
            this.levels = JSON.parse(JSON.stringify(blueprint.levels))
        }
    }

    getLevel(name){
        return this.levels.find(levelName=>levelName===name)
    }

    addLevel(name){
        this.levels.push(name)
    }

    getDefaultLevel(){
        if(typeof this.level ==="number"){
            return this.levels[this.level]
        }
        return this.levels[0]
    }
}
