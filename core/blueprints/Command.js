import Input from "./Input";

/**
 *  Represent a Command blueprint
 *  @class
 *  
 */
 export default class Command {
    /**
     * input state
     * @type {Input}
     * @public
     */
    input;

    /**
     * action triggered name
     * @type {number}
     * @public
     */
    name;

    constructor({input,name}){
        this.input=new Input(input);
        this.name=name;

    }
}