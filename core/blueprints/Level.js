import Camera from "./camera/Camera";
import Scene from "./Scene";
import Light from "./Light";
import Mesh from "./mesh/Mesh";
import Node from "./Node";
import Skin from "./Mesh/Skin";
import Material from "./mesh/material/Material";
import Command from "./Command";
import Script from "./Script";
import Ref from "./Ref";
import System from "./System";

export default class Level {
    
     /**
     * Level name
     * @type {string}
     * @public
     */
    name;

    /**
     * Scene index
     * @type {number}
     * @public
     */
    scene;
    
    /**
     * Scenes
     * @type {Array<Scene>}
     * @public
     */
    scenes;

     /**
     * Gltf reference to files
     * @type {Array<GltfRef>}
     * @public
     */
    gltfs;

    /**
     * An array of Node
     * @type {Array<Node>}
     * @public
     */
    nodes;

    /**
     * An array of Light
     * @type {Array<Light>}
     * @public
     */
    lights;

    /**
     * An camera index
     * @type {number}
     * @public
     */
    camera;

    /**
     * An array of Camera
     * @type {Array<Camera>}
     * @public
     */
    cameras;

    /**
     * An array of Mesh
     * @type {Array<Mesh>}
     * @public
     */
    meshes;

    /**
     * An array of accessors
     * @type {Array<Accesor>}
     * @public
     */
    accessors

    /**
     * An array of Camera
     * @type {Array<Skin>}
     * @public
     */
    skins;

    /**
     * An array of Material
     * @type {Array<Material>}
     * @public
     */
    materials;

    /**
     * An array of Commands
     * @type {Array<Command>}
     * @public
     */
    commands;

     /**
     * An array of Commands
     * @type {Array<System>}
     * @public
     */
    systems;
     /**
     * An array of Scripts
     * @type {Array<Script>}
     * @public
     */
    scripts;
    

    constructor(blueprint) {
        this.name = blueprint.name
        this.scene= blueprint.scene
        this.camera=blueprint.camera
        this.cameras =[];
        blueprint.cameras?.forEach(camera => {
            this.cameras.push(new Camera(camera))
        });
        
        this.scenes =[]
        blueprint.scenes?.forEach(scene => {
            this.scenes.push(new Scene(scene))
        });
        
        this.nodes =[]
        blueprint.nodes?.forEach(node=> {
            this.nodes.push(new Node(node))
        });
        
        this.meshes =[]
        blueprint.meshes?.forEach(mesh=> {
            this.meshes.push(new Mesh(mesh))
        });

        if(blueprint.accessors){
            this.accessors=blueprint.accessors
        }

        if(blueprint.bufferViews){
            this.bufferViews=blueprint.bufferViews
        }
     
        if(blueprint.buffers){
            this.buffers=blueprint.buffers
        }

        this.commands =[]
        blueprint.commands?.forEach(command=> {
            this.commands.push(new Command(command))
        });
        
        this.systems =[]
        blueprint.systems?.forEach(system=> {
            this.systems.push(new System(system))
        });
        
        this.scripts=[]
        blueprint.scripts?.forEach(script=> {
            this.scripts.push(new Script(script))
        });
    }

    addNode(indexNode){
        this.nodes.push(new Node())
        this.nodes[indexNode].children.push(this.nodes.length-1)
    }

    addMeshToNode(indexNode,indexMesh){
        this.nodes[indexNode].mesh=indexMesh
    }
    
    addGltfToNode(indexNode,file){
        let ref= new Ref({uri:file.path})
        this.nodes[indexNode].children.push(ref)
    }

    deleteMesh(indexNode){
        delete this.nodes[indexNode].mesh
    }

    deleteNodeChild(indexNode,indexNodeToDelete){
        let deleteIndex= this.nodes[indexNode].children.indexOf(indexNodeToDelete)
        this.nodes[indexNode].children.splice(deleteIndex,1)
    }

    linkNode(indexNode,indexNodeToLink){
        this.nodes[indexNode].children.push(indexNodeToLink)
    }

    updateNodeTransformation(index,attr,value){
        this.nodes[index][attr]=value
    }
    
}
