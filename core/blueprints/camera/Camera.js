import OrthographicCameraInfo from "./OrthographicCameraInfo"
import PerspectiveCameraInfo from "./PerspectiveCameraInfo"

/**
 *  Represent a Camera blueprint
 *  @class
 */
 export default class Camera {
    
    /**
     * Camera name
     * @type {string}
     */
    name;

    /**
     * An orthographic camera containing properties to create an orthographic projection matrix. This property **MUST NOT** be defined when `perspective` is defined.
     * @type {OrthographicCameraInfo}
     */
    orthographic;

    /**
     * A perspective camera containing properties to create a perspective projection matrix. This property **MUST NOT** be defined when `orthographic` is defined.
     * @type {PerspectiveCameraInfo}
     */
    perspective;
    
    /**
    * Specifies if the camera uses a perspective or orthographic projection.
    * @type {string}
    */
    type;

    constructor({name,orthographic,perspective,type}={}){
       this.name=name;
       if(type==="perspective"){
          this.perspective=new PerspectiveCameraInfo(perspective)
       }else if(type==="orthographic"){
          this.orthographic=new OrthographicCameraInfo(orthographic)
       }
       this.type=type;
    }
 }