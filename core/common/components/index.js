import actionsDef from "./actions/actionsDef.json"
import machinesDef from "./machines/machinesDef.json"
import physicDef from "./physics/physicDef.json"

export {actionsDef,
    machinesDef,
    physicDef}