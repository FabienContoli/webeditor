export default function(serviceLocator,level,state,input){
    let cameraNode = level.camera.node

    if(cameraNode.isTRS){
        cameraNode.translation[2]-=1
        cameraNode.updateMatrixFromTRS()
    }else{
        cameraNode.matrix[14]-=1
    }
}