import {yRotate} from "../../maths/m4"
import {degToRad} from "../../maths/angles"

export default function(serviceLocator,level,state,input){
    let cameraNode = level.camera.node

    if(cameraNode.isTRS){
        cameraNode.updateMatrixFromTRS()
        cameraNode.matrix=yRotate(cameraNode.matrix,degToRad(-2))
        cameraNode.updateTRSFromMatrix()
    }else{
        cameraNode.matrix=yRotate(cameraNode.matrix,degToRad(-2))
        //cameraNode.matrix[12]+=1
    }
}