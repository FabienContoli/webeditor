import { compose } from './../../maths/m4';
import { binarySearch } from './../../helpers/binarySearch';
import { cubicsplineInterpolitation } from './../../maths/cubicSpline';

// quaternion Interpolation
// https://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/slerp/index.htm

const linearInterpolation = function linearInterpolation(target, indexTime0, indexTime1, elapsed, itemSize, times, values) {
  const time0 = times[indexTime0];
  const time1 = times[indexTime1];

  const maxIncrease = time1 - time0;
  const realIncrease = elapsed - time0;

  const increasePercent = (maxIncrease - realIncrease) / maxIncrease;
  const decreasePercent = 1 - increasePercent;

  const offset0 = indexTime0 * itemSize;
  const offset1 = indexTime1 * itemSize;

  if (target.path === 'rotation') {
    const qa = [values[offset0], values[offset0 + 1], values[offset0 + 2], values[offset0 + 3]];
    const qb = [values[offset1], values[offset1 + 1], values[offset1 + 2], values[offset1 + 3]];
    target.node[target.path] = slerp(qa, qb, decreasePercent);
  } else {
    for (let c = 0; c < itemSize; c += 1) {
      let val = (values[offset0 + c] * increasePercent) + (values[offset1 + c] * decreasePercent);
      target.node[target.path][c] =val;
    }
  }
};

const stepInterpolation = function stepInterpolation(target, indexTime0, indexTime1, elapsed, itemSize, times, values) {
  let offset = indexTime0 * itemSize;
  if (elapsed === times[indexTime1]) {
    offset = indexTime1 * itemSize;
  }
  for (let c = 0; c < itemSize; c += 1) {
    target.node[target.path][c] = values[offset + c];
  }
};
// look for faster like nlerp
const slerp = function slerp(qa, qb, t) {
  // quaternion to return
  const qm = [0, 0, 0, 1];
  // Calculate angle between them.
  let cosHalfTheta = qa[0] * qb[0] + qa[1] * qb[1] + qa[2] * qb[2] + qa[3] * qb[3];

  if (cosHalfTheta < 0) {
    qb[3] = -qb[3]; qb[2] = -qb[2]; qb[1] = qb[1]; qb[0] = qb[0];
    cosHalfTheta = -cosHalfTheta;
  }

  // if qa=qb or qa=-qb then theta = 0 and we can return qa
  if (Math.abs(cosHalfTheta) >= 1.0) {
    qm[3] = qa[3]; qm[2] = qa[2]; qm[1] = qa[1]; qm[0] = qa[0];
    return qm;
  }
  // Calculate temporary values.
  const halfTheta = Math.acos(cosHalfTheta);
  const sinHalfTheta = Math.sqrt(1.0 - cosHalfTheta * cosHalfTheta);
  // if theta = 180 degrees then result is not fully defined
  // we could rotate around any axis normal to qa or qb
  if (Math.abs(sinHalfTheta) < 0.001) { // fabs is floating point absolute
    qm[0] = (qa[0] * 0.5 + qb[0] * 0.5);
    qm[1] = (qa[1] * 0.5 + qb[1] * 0.5);
    qm[2] = (qa[2] * 0.5 + qb[2] * 0.5);
    qm[3] = (qa[3] * 0.5 + qb[3] * 0.5);
    return qm;
  }
  const ratioA = Math.sin((1 - t) * halfTheta) / sinHalfTheta;
  const ratioB = Math.sin(t * halfTheta) / sinHalfTheta;
  // calculate Quaternion.
  qm[0] = (qa[0] * ratioA + qb[0] * ratioB);
  qm[1] = (qa[1] * ratioA + qb[1] * ratioB);
  qm[2] = (qa[2] * ratioA + qb[2] * ratioB);
  qm[3] = (qa[3] * ratioA + qb[3] * ratioB);
  return qm;
};

const cache ={}

export default function (serviceLocator,level,gameState){
  
    level.animations.forEach((animation) => {

    if(!cache[animation.uuid]){
      cache[animation.uuid]={
        isRunning:true,
        elapsed:0
      }
    }

    let anim=cache[animation.uuid];
    let newAnimation = false;

    if (anim.isRunning && animation.maxTime > 0) {
      const delta =serviceLocator.loop.time;
      anim.elapsed += delta;
      if (anim.elapsed > animation.maxTime) {
        anim.elapsed %= animation.maxTime;
      }
      const elapsed = anim.elapsed;
      if (!anim.startState) {
        newAnimation = true;
        anim.startState = [];
      }

      const nodeToUpdate = [];
      for (let i = 0; i < animation.channels.length; i += 1) {
        const channel = animation.channels[i];
        const { target } = channel;
        if (newAnimation) {
          anim.startState[i] = JSON.parse(JSON.stringify(target.node[target.path]));
        }

        const { sampler } = channel;
        const {
          interpolation, input,
          output, valueSize,
        } = sampler;

        const times = input.array;
        const values = output.array;

        const { itemSize } = output;
        const size = (target.path === 'weights') ? valueSize : itemSize;

        // if (elapsed < times[0]) {
        //   continue;
        // } else if (elapsed > times[times.length - 1]) {
        //   const offset = (times.length - 1) * itemSize;
        //   for (let c = 0; c < itemSize; c += 1) {
        //     target.node[target.path][c] = values[offset + c];
        //   }
        //   nodeToUpdate.push(target.node);

        //   continue;
        // }

        const indexesTime = binarySearch(times, elapsed);

        const indexedTime0 = indexesTime[0];
        const indexedTime1 = indexesTime[1];
        if (interpolation === 'CUBICSPLINE') {
          cubicsplineInterpolitation(target, indexedTime0, indexedTime1, elapsed, size, times, values);
        } else if (interpolation === 'STEP') {
          stepInterpolation(target, indexedTime0, indexedTime1, elapsed, size, times, values);
        } else {
          linearInterpolation(target, indexedTime0, indexedTime1, elapsed, size, times, values);
        }
        nodeToUpdate.push(target.node);
      }
      nodeToUpdate.forEach((node) => {
        node.matrix = compose(node.translation, node.rotation, node.scale);
      });
    }
  });

  level.scene.update();
}

