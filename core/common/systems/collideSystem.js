import {distance} from "../../maths/m4"

export default function(serviceLocator,level,gameState){
    let state=gameState.temp
    if(!state.enemyPull){
        state.enemyPull = level.findAllNodes(level.scene.nodes,(node)=>{
            if(node.name)
            return node.name.startsWith('enemy')
        })
    }

    if(!state.hero){
        state.hero= level.findNode(level.scene.nodes,(node)=>{
            if(node.name)
            return node.name==='hero'
        })
        state.score=0
        state.updateScore=true
    }

    if(!state.scoreElement){
        let elem= document.querySelector(".game-score")
        state.scoreElement=elem
    }
    if(!state.bestScoreElement){
        let elem= document.querySelector(".best-game-score")
        state.bestScoreElement=elem
    }


    let enemyPull= state.enemyPull
    let heroMatrix=state.hero.matrixWorld
        
    for (let index = 0; index < enemyPull.length; index++) {
        const enemyMatrix = enemyPull[index].matrixWorld;

        let enemyPos=[enemyMatrix[12],
        enemyMatrix[13],
        enemyMatrix[14]]
        
        let heroPos=[heroMatrix[12],
        heroMatrix[13],
        heroMatrix[14]]
        
        let dist = distance(heroPos,enemyPos)
        if(dist<=1.7){
            if(+state.bestScoreElement.textContent<state.score){
                state.bestScoreElement.textContent=state.score
            }
            serviceLocator.getEngine().restart()
           
            break;
        }
        
        if(state.heroNode.matrix[14]<enemyMatrix[14]){
            state.updateScore=true
            state.score+=1
            enemyPull[index].matrix[14]-=90
            enemyPull[index].matrix[12]=(Math.random() * 36) -18
        }
    }
    
    if(level.camera.info.yfov<1.60){
        level.camera.info.yfov+=0.0001
    }

    if(state.updateScore && state.scoreElement){
            state.scoreElement.textContent=state.score
    }

}