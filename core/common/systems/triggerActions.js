export default function(serviceLocator,level,gameState){
    let inputs = gameState.inputs;
    inputs.forEach((input)=>{
        let action =level.commands.get(input.controller+input.action+(input.input??''))
        if(action){    
            let script =level.scripts.get(action)
            if(script){
                script(serviceLocator,level,gameState,input)
            }
        }
    })
}
