export default function(serviceLocator,level,gameState){
    let inputs = gameState.inputs;
    inputs.forEach((input)=>{
        console.log(input.controller+input.action);
        let action =level.commands.get(input.controller+input.action)
        console.log(action);
        if(action){
            if(gameState.actions[action]){
                gameState.actions[action].push({action,input});
            }else{
                gameState.actions[action]=[input]
            }
        }
    })
}