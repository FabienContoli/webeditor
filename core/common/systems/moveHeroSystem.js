import {distance} from "../../maths/m4"

export default function(serviceLocator,level,gameState){

    let state=gameState.temp
    if(!state.heroNode){
        state.heroNode= level.findNode(level.scene.nodes,(node)=>{
            console.log(node.name);
            return node.name==='heroGroup'
        })
    }
  
    if(state.moveRight){
        let cameraNode = level.camera.node.parent

        if(cameraNode.matrix[12]<18){
            if(cameraNode.isTRS){
                cameraNode.translation[0]+=0.3
                cameraNode.updateMatrixFromTRS()
            }else{
                cameraNode.matrix[12]+=0.3
            }
        }
    }

    
    if(state.moveLeft){
        let cameraNode = level.camera.node.parent

        if(cameraNode.matrix[12]>-18){
            if(cameraNode.isTRS){
                cameraNode.translation[0]-=0.3
                cameraNode.updateMatrixFromTRS()
            }else{
                cameraNode.matrix[12]-=0.3
            }
        }
    }
}