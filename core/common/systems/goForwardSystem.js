export default function(serviceLocator,level,gameState){
    let state=gameState.temp
    if(!state.heroNode){
        state.heroNode= level.findNode(level.scene.nodes,(node)=>{
            return node.name==='heroGroup'
        })
        state.speed=0.55
    }

    if(!state.platform){
        state.platform= level.findNode(level.scene.nodes,(node)=>{
            if(node.name)
            return node.name==='platformNode'
        })
    }

    if(state.speed<10){
        state.speed+=0.0001
    }

    let node=state.heroNode
    if(node.isTRS){
        node.translation[2]-=state.speed
        node.updateMatrixFromTRS()
    }else{
        node.matrix[14]-=state.speed
    }

    state.platform.matrix[14]-=state.speed
    

}