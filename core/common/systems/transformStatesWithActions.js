export default function(serviceLocator,level,gameState){
    Object.entries(gameState.actions).forEach(([key,input])=>{
        console.log(key);
        let action =level.scripts.get(key)
        if(action){
            action(level,gameState,input)
        }
    })
}