const subtractVec2=function subtractVec2(a, b) {
  return a.map((v, ndx) => v - b[ndx]);
}

export { subtractVec2 }