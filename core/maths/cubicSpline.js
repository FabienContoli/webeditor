const cubicSplinePoint = function (previousPoint, previousTangent, nextPoint, nextTangent, interpolationValue) {
    const t = interpolationValue;
    const t2 = t * t;
    const t3 = t2 * t;
  
    return (2 * t3 - 3 * t2 + 1) * previousPoint + (t3 - 2 * t2 + t) * previousTangent + (-2 * t3 + 3 * t2) * nextPoint + (t3 - t2) * nextTangent;
  };
  
  const cubicsplineInterpolitation = function cubicsplineInterpolitation(target, indexTime0, indexTime1, elapsed, itemSize, times, values) {
    // https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#appendix-c-spline-interpolation
    const time0 = times[indexTime0];
    const time1 = times[indexTime1];
  
    const offset0 = indexTime0 * itemSize * 3;
    const offset1 = indexTime1 * itemSize * 3;
  
    const deltaTime = (time1 - time0);
    const t = (elapsed - time0) / deltaTime;
  
    for (let i = 0; i < itemSize; i += 1) {
      const itemOffset0 = i + offset0;
      const itemOffset1 = i + offset1;
  
      const previousSplineVertex = values[itemOffset0 + itemSize];
      const previousOutputTangent = values[itemOffset0 + 2 * itemSize];
  
      const nextInTangent = values[itemOffset1];
      const nextSplineVertex = values[itemOffset1 + itemSize];
  
      const previousTangent = deltaTime * previousOutputTangent;
      const nextTangent = deltaTime * nextInTangent;
  
      const point = cubicSplinePoint(previousSplineVertex, previousTangent, nextSplineVertex, nextTangent, t);
      target.node[target.path][i] = point;
    }
  
    // normalize rotation
  };

  
  export { cubicSplinePoint,cubicsplineInterpolitation };
  