import {degToRad,radToDeg} from "./angles"
function toQuaternion(eulerRotation) // yaw (Z), pitch (Y), roll (X)
{   
    
    let roll =degToRad(eulerRotation[0])
    let pitch =degToRad(eulerRotation[1])
    let yaw=degToRad(eulerRotation[2])
    // Abbreviations for the various angular functions
    let cy = Math.cos(yaw * 0.5);
    let sy = Math.sin(yaw * 0.5);
    let cp = Math.cos(pitch * 0.5);
    let sp = Math.sin(pitch * 0.5);
    let cr = Math.cos(roll * 0.5);
    let sr = Math.sin(roll * 0.5);

    let q ={};
    q[3] = cr * cp * cy + sr * sp * sy;
    q[0] = sr * cp * cy - cr * sp * sy;
    q[1] = cr * sp * cy + sr * cp * sy;
    q[2] = cr * cp * sy - sr * sp * cy;

    return q;
}

const copySign = (x, y) => Math.sign(x) === Math.sign(y) ? x : -x;

function toEuler(quat){

    let angles = [0,0,0];
    let q ={x:quat[0],y:quat[1],z:quat[2],w:quat[3]}
    // roll (x-axis rotation)
    let sinr_cosp = 2 * (q.w * q.x + q.y * q.z);
    let cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y);
    angles[0] = radToDeg(Math.atan2(sinr_cosp, cosr_cosp));

    // pitch (y-axis rotation)
    let sinp = 2 * (q.w * q.y - q.z * q.x);
    if (Math.abs(sinp) >= 1)
        angles.pitch = copySign(Math.PI / 2, sinp); // use 90 degrees if out of range
    else
        angles[1] = radToDeg(Math.asin(sinp));

    // yaw (z-axis rotation)
    let siny_cosp = 2 * (q.w * q.z + q.x * q.y);
    let cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z);
    angles[2] = radToDeg(Math.atan2(siny_cosp, cosy_cosp));

    return angles;
}

export {toQuaternion,toEuler}