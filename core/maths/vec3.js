
const multiplyScalarVec3 = function multiplyScalarVec3(scalar, vec3) {
    return [ vec3[0] * scalar, vec3[1] * scalar, vec3[2] * scalar];
}

const divideScalarVec3 = function divideScalarVec3(scalar, vec3) {
    return multiplyScalarVec3(1 / scalar, vec3);
}

const lengthVec3 = function lengthVec3(vec3) {
return Math.sqrt(vec3[0] * vec3[0] + vec3[1] * vec3[1] + vec3[2] * vec3[2]);
}

const normalizeVec3 = function normalizeVec3(vec3) {
// normalize(cross(B - A, C - A));
return divideScalarVec3(lengthVec3(vec3) || 1, vec3);
}

const sumVectors3 = function sumVectors3(a, b) {
return [a[0] + b[0],
    a[1] + b[1],
    a[2] + b[2]];
}

const subVectors3 = function subVectors3(a, b) {
    return [a[0] - b[0],
    a[1] - b[1],
    a[2] - b[2]];
}

const crossVectors3 = function crossVectors3(a, b) {
    const ax = a[0]; const ay = a[1]; const
        az = a[2];
    const bx = b[0]; const by = b[1]; const
        bz = b[2];

    const x = ay * bz - az * by;
    const y = az * bx - ax * bz;
    const z = ax * by - ay * bx;

    return [x, y, z];
}

export {
    multiplyScalarVec3,divideScalarVec3,lengthVec3,normalizeVec3,sumVectors3,subVectors3,crossVectors3 
}