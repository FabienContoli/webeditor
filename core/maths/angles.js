function degToRad(d) {
    return d * Math.PI / 180;
  }
  function radToDeg(d) {
    return d * 180 / Math.PI;
  }
  
  export { degToRad, radToDeg };
  