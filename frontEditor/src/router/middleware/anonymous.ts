import { useUser } from '@/stores/user'
import { useApp } from '@/stores/app'


export default function (to,from) {
     
    const appStore = useApp();
    if(appStore.isInit){
      const user = useUser()
    
      if ( user.isConnected ) {
        return '/profil'
      }
    }
    
    
}