import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomePage.vue'
import SignIn from '@/views/sign-in.vue'
import SignUp from '@/views/sign-up.vue'
import Projects from '@/views/projects.vue'
import ForgotPassword from '@/views/forgot-password.vue'

import TermsOfService from '@/views/terms-of-service.vue'
import Profil from '@/views/profil.vue'
import EditorProject from '@/views/editor/ProjectEdit.vue'
import PreviewProject from '@/views/preview/ProjectPreview.vue'

import anonymousMiddleware from "@/router/middleware/anonymous"
import authMiddleware from "@/router/middleware/auth"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: '/',
      component: HomeView
    },
    {
      path: '/projects',
      name: 'projects',
      component: Projects
    },
    {
      path:'/sign-up',
      name:'sign-up',
      component: SignUp,
      meta:{
        middleware:"anonymous"
      },
      beforeEnter:anonymousMiddleware,
    },
    {
      path:'/sign-in',
      name:'sign-in',
      component: SignIn,
      meta:{
        middleware:"anonymous"
      },
      beforeEnter:anonymousMiddleware
    },
    {
      path:'/forgot-password',
      name:'forgot-password',
      component: ForgotPassword,
      meta:{
        middleware:"anonymous"
      },
      beforeEnter:anonymousMiddleware
    },
    {
      path:'/terms-of-service',
      name:'terms-of-service',
      component: TermsOfService,
    },
    {
      path:'/profil',
      name:'profil',
      component: Profil,
      meta:{
        middleware:"auth"
      },
      beforeEnter:authMiddleware
    },
    {
      path:'/editor/:projectId',
      name:'editor',
      component: EditorProject,
      meta:{
        middleware:"auth"
      },
      beforeEnter:authMiddleware
    },
    {
      path:'/preview/:projectId',
      name:'preview',
      component: PreviewProject
    },

  ]
})

export default router
