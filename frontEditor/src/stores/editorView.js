import { defineStore } from 'pinia'
import { useGameProject} from  "@/stores/gameProject";

export const useEditorView = defineStore('editorView', {
  state: () => ({
    type: "level",
  }),

  actions: {
    selectView:function(type){
        this.type=type
        if(this.type==='preview'){
            useGameProject().startPreview() 
        }
    },
  },
})
