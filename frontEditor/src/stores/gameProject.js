import { defineStore,acceptHMRUpdate } from 'pinia'
import { useProjectDetail } from '@/stores/projectDetail';
import { useEditorView } from '@/stores/editorView';
import GameBlueprint from 'core-web-engine/blueprints/Game';
import LevelBlueprint from 'core-web-engine/blueprints/Level';
import {createDefaultLevel} from 'core-web-engine/builder/blueprintsBuilder';
import { useToast } from "vue-toastification";
import GameState from 'core-web-engine/models/GameState';
import Camera from 'core-web-engine/models/camera/Camera';
import LevelLoader from 'core-web-engine/loaders/LevelLoader';
import Node from 'core-web-engine/models/Node';
import defaultEditorScript from 'core-web-engine/common/blueprints/actions/freeCamera.json';
import defaultEditorCommand from 'core-web-engine/common/blueprints/commands/freeCamera.json';
import {getExtension,isTextFile} from 'core-web-engine/helpers/file'
import { markRaw } from 'vue';


export const useGameProject = defineStore('gameProject', {
  state: () => ({
    pending:true,
    error:false,
    //next line are used as engine parameters
    gameState:null,
    gameLoader:null,
    gameBlueprint:null,
    gameBlueprintFile:null,
    levelInstance:null,
    levelBlueprint:null,
    levelBlueprintFile:null,
    //put that in a gameState
    focus:false,
    selectedLevel:null,
    selectedScene:null,
    selectedNode:null,
    selectedFile:null,
    debugCamera:null
    
  }),
  

  getters: {
    levels:(state)=>{
      return state.gameBlueprint?.levels
    },
    files: () =>{
      const projectDetail = useProjectDetail();
      return projectDetail.files;
    },
    isReady: (state)=>{
      return state.pending ===false && state.error ===false
    },
  },

  actions: {
    async init() {
      const projectDetail =useProjectDetail()
      const file =  projectDetail.getFileByNameAndFolder("","game.json")
      if(file){
        this.gameBlueprintFile=file;
        if(file.content) {
          
          this.gameBlueprint = new GameBlueprint(this.gameBlueprintFile.content)
          this.pending=false
        }else{
            await projectDetail.downloadFile(file)
            this.gameBlueprint = new GameBlueprint(this.gameBlueprintFile.content)
            this.pending=false
            useToast().success("game downloaded");
        }
      }else{
        this.createRootFile()
      }
    },
    async createRootFile(){
      const projectDetail = useProjectDetail();
      const newBlueprint =new GameBlueprint({name:projectDetail.name})

      const file = new File([JSON.stringify(newBlueprint)], "game.json", {
        type: "application/json",
      });
      this.gameBlueprintFile= await projectDetail.createFile("","game.json",file)
      this.gameBlueprint= newBlueprint

      this.pending=false
    },

    async addLevel(name){

      var toast =useToast()
      try{
        
      const projectDetail = useProjectDetail();
        
      
      const file =  projectDetail.getFileByNameAndFolder("","game.json")
      if(file){
        
        if(this.gameBlueprint.getLevel(name)){
          const errorMessage='level already exist'
          toast.error(errorMessage);
          throw errorMessage
        }

        this.gameBlueprint.addLevel(name)
        

        const gameFile = new File([JSON.stringify(this.gameBlueprint)], "game.json", {
          type: "application/json",
        });

        await projectDetail.updateFile(file,gameFile)

        const levelBlueprint =createDefaultLevel(name)
        const filename =name+".json"
        const levelFile = new File([JSON.stringify(levelBlueprint)], filename , {
          type: "application/json",
        });
        await projectDetail.createFile("/levels",filename,levelFile)
      }else{
        toast.error("project not ready")
      }

      }catch(err){
        console.error("addLevel error:",err)
      }
    },

    async selectLevel(levelName,isPreview){
      
      const projectDetail =useProjectDetail()
      const file =  projectDetail.getFileByNameAndFolder("/levels",levelName+".json")

      this.levelBlueprintFile=await projectDetail.downloadFile(file)

      this.levelBlueprint= new LevelBlueprint(this.levelBlueprintFile.content)
      this.levelInstance=null;
      //this.gameLoader=new GameLoader()
      await this.selectScene(this.levelBlueprint.scene,isPreview) 
      
      useToast().success("level downloaded");
    },

    
    async selectScene(sceneName,isPreview){
      this.selectedScene= sceneName
      this.loadScene(isPreview)
    },

    async loadScene(isPreview){
      
        this.gameState= new GameState({state:{focus:this.focus,
          selectedLevel:this.selectedLevel,
          selectedScene:this.selectedScene,
          selectedNode:this.selectedNode}});
          
          const projectDetail =useProjectDetail()
          
          let levelBlueprint=isPreview?this.levelBlueprint:this.overrideScene(this.levelBlueprint)
         // levelBlueprint=this.overrideScene(this.levelBlueprint)
         
          this.levelInstance=markRaw(await new LevelLoader(projectDetail).load(levelBlueprint,{filePath:"",sceneIndexOverride:this.selectedScene}))
        
          if(!isPreview){
            this.debugCamera=new Camera({name:"debugEditor",type:"perspective"})
            this.editorRootNode= new Node({translation:[0,2,0]});
            this.editorRootNode.camera=this.debugCamera
            this.debugCamera.node=this.editorRootNode
            this.levelInstance.cameras.set(this.debugCamera.uuid,this.debugCamera)
            this.levelInstance.camera=this.debugCamera;
            this.levelInstance.scene.nodes.push(this.editorRootNode)
            // this.debugCamera.node=this.levelInstance.scene.nodes[0];
          }
         
          //this.levelInstance=new LevelModel(this.levelBlueprint)//this.gameLoader.load(this.gameState)
    },

    overrideScene(levelBlueprint){
      
      let levelOverideBlueprint = JSON.parse(JSON.stringify(levelBlueprint))
      levelOverideBlueprint.commands=JSON.parse(JSON.stringify(defaultEditorCommand))
      levelOverideBlueprint.scripts=JSON.parse(JSON.stringify(defaultEditorScript))
      levelOverideBlueprint.systems=[]
      return levelOverideBlueprint
    },

    async updateScene(isPreview){
      const projectDetail =useProjectDetail()
      
      let levelBlueprint=isPreview?this.levelBlueprint:this.overrideScene(this.levelBlueprint)
      let updateLevel=markRaw(await new LevelLoader(projectDetail).load(levelBlueprint,{filePath:"",sceneIndexOverride:this.selectedScene}))

      this.levelInstance.scene.nodes=updateLevel.scene.nodes
      
      if(!isPreview){
        this.editorRootNode.camera=this.debugCamera
        this.debugCamera.node=this.editorRootNode
        this.levelInstance.cameras.set(this.debugCamera.uuid,this.debugCamera)
        this.levelInstance.camera=this.debugCamera;
        this.levelInstance.scene.nodes.push(this.editorRootNode) 
      }
    },

    //update all Node from blueprint, weird name function 
    async updateNodeTransformation(ref){
      const projectDetail =useProjectDetail()
      if(typeof ref ==="number"){
        let levelLoader=new LevelLoader(projectDetail)
        levelLoader.levelBlueprint=this.levelBlueprint
        let newNode =await levelLoader.loadNode(ref)
        const handler=(node)=>{
          if(node.ref === ref && !node.isJoint){
            console.log("node.ref:"+node.ref);
            console.log("ref:"+ref);
            node.copyPos(newNode)
          }
        }
        this.recursiveUpdateInstanceThree(handler, this.levelInstance.scene.nodes)
        this.levelInstance.scene.update()
        
      }else {
        console.warn("not handled")
      }
   
    },

    recursiveUpdateInstanceThree(handler,nodes){
      nodes.forEach((childNode)=>{
        handler(childNode)
        this.recursiveUpdateInstanceThree(handler,childNode.children)
      })
    },

    async saveLevel(){
      const projectDetail =useProjectDetail()
      const levelName=this.levelBlueprint.name
     
      const levelFile = new File([JSON.stringify(this.levelBlueprint)], "/levels/"+levelName+".json", {
        type: "application/json",
      });
      await projectDetail.updateFile(this.levelBlueprintFile,levelFile)

    },

    async selectFile(file){
      const projectDetail =useProjectDetail()

      let downloadedFile =await projectDetail.downloadFile(file)
      let extension=  getExtension(downloadedFile.path)
      this.selectedFile=downloadedFile
      if(isTextFile(extension)){
        useEditorView().selectView("textFile")
      } 
      console.log("file selected:",downloadedFile);
    },

    async deleteFile(path){
      const projectDetail =useProjectDetail()

      let file =projectDetail.getFileByPath(path)
      await projectDetail.deleteFile(file)

      useToast().success("File "+file.name+" deleted");
    },

    async deleteFolder(path){
      const projectDetail =useProjectDetail()

      const promises=[]

      let files =projectDetail.getFilesByFolderPath(path)
      files.forEach((file)=>{
        promises.push(projectDetail.deleteFile(file))
      })
      await Promise.all(promises)
      
      useToast().success("Folder deleted");
      
    },
    
    editNode(nodeIndex){
      this.selectedNode=nodeIndex;
      this.focus=true
    },

    async startPreview(){
      this.selectedNode=null;
      this.focus=false;
      let name = this.levelBlueprint?this.levelBlueprint.name:this.gameBlueprint.getDefaultLevel();
      await this.selectLevel(name,true)
      // this.loadScene()
    },
    async export(){
      const projectDetail =useProjectDetail()
      await projectDetail.export()
    }
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useGameProject, import.meta.hot))
}