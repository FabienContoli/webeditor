import { defineStore,acceptHMRUpdate } from 'pinia'
import { fetchData } from '@/helpers/fetch'
import { useToast } from "vue-toastification";
import { useUserProjects } from '@/stores/userProjects';
import { useApp } from '@/stores/app';

export const useUser = defineStore('loggedInUser', {
  state: () => ({
    emailAddress: "",
    fullName: "",
    avatarUrl:"",
    isConnected: false,
    img:null,
    pending:false
  }),

  getters: {
    projects:  () =>{
      const userProjects = useUserProjects();
      return userProjects.projects;
    }
  },

  actions: {
    
    async getProfil() {
      const config = import.meta.env
      let options ={
        method:'get',
        credentials: "include",
        baseURL:config.VITE_BACKEND_URL
      };
      
      
      const userProjects = useUserProjects();
      this.pending=true
      const toast= useToast()
      
      const appStore = useApp();

      try{
        const response= await fetchData('account/profil',options);

        if(response){
          const user =response
          this.isConnected=true
          
          this.avatarUrl=user.avatarUrl
          this.fullName=user.fullName
          this.emailAddress=user.emailAddress
          userProjects.setList(user.projects)
          this.pending=false

          const route =this.router.currentRoute.value

          if(route.meta && route.meta.middleware && route.meta.middleware.indexOf("anonymous")!==-1){
           this.router.push('/');
          }
        }else{
          throw "Error fetching"
        }
      }catch(err){
        

        this.$reset()
        userProjects.$reset()

        const route =this.router.currentRoute.value

        if(route.meta && route.meta.middleware && route.meta.middleware.indexOf("auth")!==-1){
          toast.error("You are not connected")
          this.router.push('/sign-in');
        }
      }

      if(!appStore.isInit){
        appStore.init()
      }

    },

    disconnect(){
      this.router.push('/');
      this.$reset()
      const userProjects = useUserProjects();
      userProjects.$reset()
    },

    async logout() {
      const config = import.meta.env
      
      let options ={
        method:'get',
        credentials: "include",
        baseURL:config.VITE_BACKEND_URL
      };
      
      try{
        await fetchData('account/logout',options);

      }catch(err){
        console.error(err)
      }

      
      this.router.push('/');
      this.$reset()
      const userProjects = useUserProjects();
      userProjects.$reset()
    },


    async login(userParams){

      
      const config = import.meta.env
      
      let options ={
        method:'put',
        body:userParams,
        credentials: "include",
        baseURL:config.VITE_BACKEND_URL
      };
      const toast= useToast()
      try{
        const response = await fetchData('entrance/login',options);
        
        if(response){
          
          const user =response
          this.isConnected=true
          
          this.avatarUrl=user.avatarUrl
          this.fullName=user.fullName
          this.emailAddress=user.emailAddress
          toast.success("Your are logged in", {
            timeout: 2000
          });

        this.router.push('/profil');

        }else{
          throw "error fetching"
        }
      }catch(err){
        this.$reset()
        const userProjects = useUserProjects();
        userProjects.$reset()
        if(err.data && err.data==='Unauthorized'){
          toast.error("Authentification error")
        }else{
          toast.error("Server error: try later")
        }
      }

    },
  },
})

 if (import.meta.hot) {
   import.meta.hot.accept(acceptHMRUpdate(useUser, import.meta.hot))
 }
