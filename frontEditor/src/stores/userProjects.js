import { acceptHMRUpdate, defineStore } from 'pinia'
import {fetchData} from "@/helpers/fetch"
import { useToast } from "vue-toastification";
export const useUserProjects = defineStore('userProjects', {
  state: () => ({
    projects: [],
  }),
  

  actions: {
    /**
     * Add project to list
     * @param {object} projectParam
     * @param {string} projectParam.name
     * @param {string} projectParam.description
     */
     async create({name,description}) {

      const config = import.meta.env
      let options ={
        method:'put',
        body:{name,description},
        credentials: "include",
        baseURL:config.VITE_BACKEND_URL
      };
      const toast=useToast()
      const { data } = await fetchData('project/create',options);
      
       if( data && data.value ){
         
        toast.success("Project created");
        this.projects.push(data.value)
      }else{
        toast.error("Project creation failed")
      }
    },

    setList(projectArray){
      this.$patch({projects:projectArray})
    },
    /**
     * Remove project from list
     * @param {string} name
     */
    async remove(project) {
      
      const config = import.meta.env
      let options ={
        method:'delete',
        body:{projectId:project.id},
        credentials: "include",
        baseURL:config.VITE_BACKEND_URL
      };
      const toast=useToast()
      const { data } = await fetchData('project/delete',options);
      
       if( data && data.value ){
         
        toast.success("Project "+project.name+" deleted");
          
        const i = this.projects.findIndex(element => element.id === project.id);

        if (i > -1) this.projects.splice(i, 1)
      }else{
        toast.error("Delete project failed")
      }
    },

  },
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useUserProjects, import.meta.hot))
}
