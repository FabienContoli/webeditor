
import { defineStore,acceptHMRUpdate } from 'pinia'
import { useToast } from "vue-toastification";
import { useGameProject } from './gameProject';
import { uploadFiles } from '@/helpers/uploadFiles';
import Folder from '@/helpers/folder';
import gameDefaultFoldersTemplate from '@/helpers/template/gameDefaultFolders';
import {fetchData,fetchFile} from "@/helpers/fetch"
import Ref from 'core-web-engine/models/Ref';
import * as commonScript from "core-web-engine/common/scripts"
import * as commonSystems from "core-web-engine/common/systems"
import {getExtension,getPathFolder,getFolders} from 'core-web-engine/helpers/file'

export const useProjectDetail = defineStore('projectDetail', {
    state: () => ({
        files:{},
        id:"",
        name:"",
        owner:"",
        folder:{'root':null}
    }),
    
    getters:{
      getFileById: (state) => {
        return (id) => Object.values(state.files).find((file) => file.id === id)
      },
      getFileByNameAndFolder: (state) => {
        return (folder,name) => Object.values(state.files).find((file) => file.path === folder+'/'+name)
      },
      getFileByPath: (state) => {
        return (path) => Object.values(state.files).find((file) => file.path === path)
      },
      getFilesByFolderPath: (state) => {
        return (path) => Object.values(state.files).filter((file) => file.path.startsWith(path))
      },
      folders:(state)=>{
        return state.folder.root
      }
    },
    actions: {

        async downloadFile(file) {   
          
          const toast=useToast()
          try {
        
          const config = import.meta.env
          let options ={
            method:'get',
            credentials: "include",
            baseURL:config.VITE_BACKEND_URL
          };
          
          const response = await fetchFile(`project/${this.id}/${file.id}`,options,file.mimeType);
          
           if( response ){
            this.files[file.id].content=response
            return this.files[file.id]
           }else{
             throw new Error("repsonse empty")
           }
                
          } catch(err){
            toast.error("Download failed: File "+file.name+" in folder "+ file.folder)
            throw err
          }
        },
        
        /**
         * load Ref file 
         * @param {Ref}
         */
        async loadRef(ref){

          const toast=useToast()
          try {
            if(ref.type==="common"){
              if(commonSystems[ref.uri]){
                return commonSystems[ref.uri]
              }
              return commonScript[ref.uri]
            }else{
              const file =this.getFileByPath(ref.uri)
              if(!file){
                console.error("No file for the ref",ref.uri)
              }
              
              let extension=  getExtension(ref.uri)
              
              let fileDownloaded =await this.downloadFile(file)
              let fileContent =await fileDownloaded.content
              if(extension==="gltf"){
                let json = JSON.parse(await fileContent.text());
                return json
                //TODO: move this in levelLoader
                // let levelInstance=await new LevelLoader(this).load(json,{filePath:fileFolder})
                //return levelInstance.scene.nodes
              }else if(extension==="bin"){
                let bin =await fileContent.arrayBuffer()
                return bin
              }else if(extension==="jpg"||extension==="png"){
                let arrayBuffer = await fileContent.arrayBuffer()
                const dataView = new DataView(arrayBuffer);
                const blob = new Blob([dataView], { type: 'image/png' });
                return (blob);
              }
              
            }
          }catch(err){
              toast.error("Load Ref failed: File "+ref)
              throw err
          }
        },
        

        /**
         * Add file to project
         * @param {object} 
         * @param {string} folder
         * @param {string} name
         * @param {File}  file
         */
         async createFile(folder,name,file) {
           
          const toast=useToast()
          try{
            if(this.id){

              const config = import.meta.env
              
              const url =config.VITE_BACKEND_URL+`project/${this.id}`
            
              const response =await uploadFiles(url,{path:folder+"/"+name,file:file})


              if( response && response.file ){
                let createdFile=response.file
                this.files[createdFile.id]=createdFile;
                this.files[createdFile.id].content=file;
                this.addFilesToThreeFolder(this.folders,[this.files[createdFile.id]])
                
                toast.success("file created");
                return this.files[createdFile.id]
              }else{
                toast.error("File "+name+" in folder "+folder+" publish failed")
                throw new Error("File "+name+" in folder "+folder+" publish failed");
              }
            }
          }catch(err){
            toast.error("File "+name+" in folder "+folder+" publish failed")
            throw err
          }
        },

            /**
         * Add file to project
         * @param {Object} file
         * @param {File} fileContent 
         */
        async updateFile(file, fileContent) {

            const config = import.meta.env
         
            const toast=useToast()

            try{
              const url =config.VITE_BACKEND_URL+`project/${this.id}/${file.id}`;
              const response =await uploadFiles(url,{path:file.path,file:fileContent},'put')
        
              if( response && response.file ){
                  
                  let createdFile=response.file
                  this.files[createdFile.id]=createdFile
                  this.files[createdFile.id].content=fileContent
                  toast.success("file update");
                  return this.files[createdFile.id]
              }else{
                  toast.error("File update failed: "+file.path)
              }
            }catch(err){
              toast.error("File update failed: "+file.path)
              throw err
            }
        },
        
        async retrieveProjectDetail(projectId){
            const config = import.meta.env
            let options ={
                method:'get',
                credentials: "include",
                baseURL:config.VITE_BACKEND_URL
            };
            const toast=useToast()

            try{
            const response = await fetchData(`project/${projectId}`,options)

              if( response  ){
                let project =response
                this.id=project.id
                this.name=project.name
                this.owner=project.owner
                this.setDico(project.files)
                const gameProject = useGameProject();
                await gameProject.init();
                return response
              }else{
                toast.error("Files retrieved failed")
                
            }
            }catch(err){
              toast.error("Files retrieved failed")
              throw err
            }
        },

        setDico(fileArray){
          
          const fileDico = fileArray.reduce(
            (dict, el) => (dict[el.id] = el, dict),
            {});

            const createThreeRecursive =function(foldersObject){
              let folder =new Folder()
              if(foldersObject){  
                const entries=  Object.entries(foldersObject)
                for (let index = 0; index <entries.length; index++) {
                  const entry = entries[index];
                  const key= entry[0]
                  const value= entry[1]
                  folder.folders[key]=createThreeRecursive(value.folders)
                }
              }
              return folder
            }
            let  folderThree= createThreeRecursive(gameDefaultFoldersTemplate)

            this.addFilesToThreeFolder(folderThree,fileArray)
                
          this.$patch({files:fileDico,folder:{'root':folderThree}})
        },

        addFilesToThreeFolder(folderThree,fileArray){
          folderThree=fileArray.reduce(
            (three, el) => {
                var names=el.path.split('/').filter((name)=> name!=='');
           
                var folder =three
                for (let index = 0; index < names.length-1; index++) {
                
                  const name = names[index];
         
                  let subFolder =folder.getFolder(name)
                  if(subFolder){
                    folder=subFolder
                  }else{
                    subFolder=folder.addFolder(name)
                    folder=subFolder
                  }
                }
                folder.addFile(el)
              return three},folderThree);
        },

        /**
         * removeFile from three
         * @param {object} folder
         * @param {Array} foldersName
         */
        removeFileFromThreeFolder(currentFolder,foldersName,fileName){
          if(foldersName.length===0){
            return delete currentFolder.files[fileName]
            
          }else if(foldersName.length>0){
            let subFolderName=foldersName.shift()
            let subFolder=currentFolder.folders[subFolderName]
            if(subFolder){
              this.removeFileFromThreeFolder(subFolder,foldersName,fileName)
              //default folder need a boolean
              // if(Object.keys(subFolder.files).length===0 && Object.keys(subFolder.folders).length===0){
              //   delete currentFolder.folders[subFolderName]
              // }
              return true;
            }
          }
          throw new Error('unknow folder')
        },
      

        /**
         * deleteFile file from list
         * @param {object} file
         */
        async deleteFile(file) {
          
          const config = import.meta.env
          let options ={
            method:'delete',
            credentials: "include",
            baseURL:config.VITE_BACKEND_URL
          };
          const toast=useToast()
          const response = await fetchData(`project/${this.id}/${file.id}`,options);
           if( response.ok ){
              
            delete this.files[file.id];
            this.removeFileFromThreeFolder(this.folders,getFolders(file.path),file.name)

    
          }else{
            toast.error("Delete file failed: "+file.name)
            throw "Delete file failed"
          }
        },
        async export(){
          let promises=[]
          let fileList=Object.values(this.files)
          const liteFilesArray=fileList.map((file)=>{
            return {"id":file.id,"path":file.path,"name":file.name,"mimeType":file.mimeType}
          });
          promises.push(this.save({id:"db_files",name:"db_files.json",content:liteFilesArray,mimeType:"application/json"}))
          
          fileList.forEach((file)=>{
            if(!file.content){
              promises.push(this.downloadAndSave(file))
            }else{
              promises.push(this.save(file))
            } 
          })
          await Promise.all(promises)
          return;
        },
        async downloadAndSave(file){
          await this.downloadFile(file)
          await this.save(file)
          return file;
        },
        async save(file){
          let data= file.content;
          let filename=file.id;
          let extension=  file.name.match(/[^\.]+$/)[0].toLowerCase();
          if(file.mimeType==="application/json"){
            data=JSON.stringify(data)
          }
          const blob = new Blob([data],{"type":file.mimeType});
          const elem = window.document.createElement('a');
          elem.href = window.URL.createObjectURL(blob);
          elem.download = filename+"."+extension;        
          document.body.appendChild(elem);
          elem.click();        
          document.body.removeChild(elem);
        }
      },
    })
    
    if (import.meta.hot) {
      import.meta.hot.accept(acceptHMRUpdate(useProjectDetail, import.meta.hot))
    }
    