import { defineStore } from 'pinia'


export const useApp = defineStore('app', {
  state: () => ({
    isInit: false,
  }),

  actions: {
    
    init() {
        this.isInit=true
    },
  },
})
