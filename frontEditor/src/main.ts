import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { markRaw } from 'vue'

import Toast, { POSITION } from "vue-toastification";
import "vue-toastification/dist/index.css";

import App from './App.vue'
import router from './router'

const app = createApp(App)

// Import the CSS or use your own!

const toastOptions = {
    position: POSITION.BOTTOM_RIGHT
};


app.use(Toast, toastOptions);


const components = import.meta.globEager('./components/Atom/*.vue')

Object.entries(components).forEach(([path, definition]) => {
  // Get name of component, based on filename
  // "./components/Fruits.vue" will become "Fruits"
  const componentName = path.split('/').pop().replace(/\.\w+$/, '')

  // Register component on this Vue instance
  app.component(componentName, definition.default)
})

app.use(router)

const pinia =createPinia()

pinia.use(({ store }) => {
  store.router = markRaw(router)
})

app.use(pinia)

app.mount('#app')
