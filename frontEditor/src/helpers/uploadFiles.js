import { $fetch } from 'ohmyfetch'

export async function uploadFiles(url, files,method) {
    const formData  = new FormData();
    
    for(const name in files) {
      formData.append(name, files[name]);
    }
  
    const response = await $fetch(url, {
      method: method??'POST',
      body: formData,
      credentials: "include",
    });

    return response
  
  }