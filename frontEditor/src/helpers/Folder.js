export default class Folder{

    folders;
    files;

    constructor(){
        this.folders={};
        this.files={}
    }

    getFile(file){
        return this.files[file.name]
    }

    getFileByName(name){
        return this.files[name]
    }

    getFolder(name){
        return this.folders[name]
    }

    addFile(file){
        this.files[file.name]=file
    }

    addFolder(name){
        return this.folders[name]=new Folder()
    }
}