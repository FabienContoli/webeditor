// fetch.js
import { ref, isRef, unref, watchEffect } from 'vue'
import { useUser } from "@/stores/user"

import { $fetch } from "ohmyfetch"

export function useFetch(url,options) {
  const data = ref(null)
  const error = ref(null)

  function doFetch() {
    // reset state before fetching..
    data.value = null
    error.value = null
    // unref() unwraps potential refs
    let urlToFetch=unref(url)
    if(options && options.baseURL){ 
      urlToFetch=options.baseURL+url
    }
    fetch(urlToFetch,options)
      .then((res) => res.json())
      .then((json) => (data.value = json))
      .catch((err) => (error.value = err))
  }

  if (isRef(url)) {
    // setup reactive re-fetch if input URL is a ref
    watchEffect(doFetch)
  } else {
    // otherwise, just fetch once
    // and avoid the overhead of a watcher
    doFetch()
  }

  return { data, error }
}

export async function fetchData(url,options){

  try{
    
   let urlToFetch=unref(url)
   
   if(options){
     if(options.body){
       options.body=JSON.stringify(options.body)
     }
     if(options.baseURL){ 
      urlToFetch=options.baseURL+url
    }
   } 
   

   const response = await fetch(urlToFetch,options);
   
   if(!response.ok){
      const error = new Error(response.statusText);
      error.json = response.json();
      if(response.status===401){
        const user = useUser();
        user.disconnect()
      }
      throw error;
   }
   
   if(response.headers.get("Content-Type").startsWith('application/json;')){
    return response.json()
   }else{
     return response
   }
  

   
  }catch(err){
    throw err;
  }

}


export async function fetchFile(url,options){

  let urlToFetch=unref(url)
  try{
    
   
   if(options){
     if(options.body){
       options.body=JSON.stringify(options.body)
     }
     if(options.baseURL){ 
      urlToFetch=options.baseURL+urlToFetch
    }
   } 
   

   const response = await $fetch(urlToFetch,options);

   return response

  }catch(err){
    throw err;
  }

}
