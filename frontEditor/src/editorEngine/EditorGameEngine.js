import Loop from "core-web-engine/lifecyles/Loop.js";
import AudioApi from "core-web-engine/audio/AudioApi";
import Renderer from "core-web-engine/renderer/Renderer";
import Controllers from "core-web-engine/controllers/Controllers";
import Physics from "core-web-engine/physics/Physics";
import Logger from "core-web-engine/logger/Logger";
import ServiceLocator from "core-web-engine/serviceLocator"
export default class EditorEngine{
    
    constructor({container = document.body,level,gameState}){

        // this.gameLoader = new GameLoader();

        // //Store game 
        // this.gameState= new GameState();
    
        // this.gameState.load();
        
        /*
        *   lights
        *   systemes
        *   commands
        *   graph
        *   meshes
        *   skins
        *   cameras
        */
        // let scene=this.gameLoader.load(this.gameState);

        let logger = new Logger()
        let controllers =new Controllers(null,container,logger);
        let audioApi = new AudioApi()
        let renderer =new Renderer(container);
        let loop = new Loop({logger});
        let physics =new Physics();
        this.serviceLocator=new ServiceLocator()
        this.serviceLocator.setAudioService(audioApi)
        this.serviceLocator.setControllers(controllers)
        this.serviceLocator.setRenderer(renderer)
        this.serviceLocator.setLoop(loop)
        this.serviceLocator.setPhysics(physics) 
        this.serviceLocator.setLogger(logger) 
        level.scene.update();
        this.serviceLocator.getLoop().setUpdate(() => {
            gameState.inputs=this.serviceLocator.getControllers().getInputs();
            level.systems.update(this.serviceLocator,level,gameState);
            level.scene.update();
            level.camera.update(this.serviceLocator.getRenderer().gl);
            gameState.actions={}
            gameState.inputs.length=0
            this.serviceLocator.getAudioService().update(gameState.soundCommands);
            gameState.inputs.length = 0;
        });
    
        this.serviceLocator.getLoop().setRender(() => {
            // level.renderers.update(this.serviceLocator,level,gameState);
            this.serviceLocator.getRenderer().render(level);
        });
    
        this.serviceLocator.getLoop().play();

        window['gameEngine']=this;
    }

    destroy(){
        console.log("destroy");
        this.serviceLocator.destroy()
        delete this.serviceLocator;
    }
    //setInitialState();




}