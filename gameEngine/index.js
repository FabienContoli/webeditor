import GameLoader from "core-web-engine/loaders/GameLoader";

import Loop from "core-web-engine/lifecyles/Loop.js";
import AudioApi from "core-web-engine/audio/AudioApi";
import Renderer from "core-web-engine/renderer/Renderer";
import Controllers from "core-web-engine/controllers/Controllers";
import Physics from "core-web-engine/physics/Physics";
import Logger from "core-web-engine/logger/Logger";
import ServiceLocator from "core-web-engine/serviceLocator"

export default class gameEngine{
    
    container;

    level;

    gameState;

    gameStateSave;
    constructor({container = document.body,level,gameState,loadLevel}){

        //this.gameLoader = new GameLoader();

        //Store game 
        //this.gameState= new GameState();
        //this.gameState.load();
        this.container=container;
        this.level=level;
        this.gameState=gameState;
        this.loadLevel=loadLevel
        this.start()
      
    }

    start(){
  
        let logger = new Logger()
        let controllers =new Controllers(null,this.container,logger);
        let audioApi = new AudioApi()
        let renderer =new Renderer(this.container);
        let loop = new Loop({logger});
        let physics =new Physics();
        this.serviceLocator=new ServiceLocator()

        this.serviceLocator.setEngine(this)
        this.serviceLocator.setAudioService(audioApi)
        this.serviceLocator.setControllers(controllers)
        this.serviceLocator.setRenderer(renderer)
        this.serviceLocator.setLoop(loop)
        this.serviceLocator.setPhysics(physics) 
        this.serviceLocator.setLogger(logger) 

        this.serviceLocator.getLoop().setUpdate(() => {
            this.gameState.inputs=this.serviceLocator.getControllers().getInputs();
            this.level.systems.update(this.serviceLocator,this.level,this.gameState);
            this.level.scene.update();
            this.level.camera.update(this.serviceLocator.getRenderer().gl);
            this.gameState.actions={}
            this.gameState.inputs.length=0
            this.serviceLocator.getAudioService().update(this.gameState.soundCommands);
            this.gameState.inputs.length = 0;
        });
    
        this.serviceLocator.getLoop().setRender(() => {
            this.serviceLocator.getRenderer().render(this.level);
        });
    
        this.serviceLocator.getLoop().play();

        // window['gameEngine']=this;
    }

    stop(){
        
        this.serviceLocator.getLoop().setUpdate(() => {
            console.log("destroy update");
            this.serviceLocator.getLoop().setUpdate(() => {
                console.log("update empty");
            });
            
            this.destroy()
            this.loadLevel()
        });
    
        this.serviceLocator.getLoop().setRender(() => {
            console.log("render empty");
        });
    
    }

    destroy(){
        console.log("destroy3");
        this.serviceLocator.destroy()
        delete this.serviceLocator;
        delete this.engine;
        this.gameState.temp={}
    }

    restart(){
        this.stop()
    }
  

}